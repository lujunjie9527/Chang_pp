package com.changjiedata.chanpp.web.manager;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.changjiedata.chanpp.web.WebViewActivity;


public class MyChromeClient extends WebChromeClient {
    private Activity mContext;
    private ReceivedTitleListener listener;
    private ProgressChangedListener onProgressChangedListener;
    private ValueCallback<Uri[]> mUploadMessageForAndroid5;
    private ValueCallback<Uri> mUploadMessage;

    public MyChromeClient(Activity context,
                          ReceivedTitleListener listener,
                          ProgressChangedListener onProgressChangedListener) {
        this.mContext = context;
        this.listener = listener;
        this.onProgressChangedListener = onProgressChangedListener;
    }


    public void setmUploadMessageForAndroid5(ValueCallback<Uri[]> mUploadMessageForAndroid5) {
        this.mUploadMessageForAndroid5 = mUploadMessageForAndroid5;
    }
    public ValueCallback<Uri[]> getmUploadMessageForAndroid5() {
        return mUploadMessageForAndroid5;
    }

    public ValueCallback<Uri> getmUploadMessage() {
        return mUploadMessage;
    }
    public void setmUploadMessage(ValueCallback<Uri> mUploadMessage) {
        this.mUploadMessage = mUploadMessage;
    }

    @Override
    public void onReceivedTitle(WebView view, String title) {
        super.onReceivedTitle(view, title);
        listener.onReceivedTitle(view,title);
    }

    @Override
    public void onProgressChanged(final WebView view, int newProgress) {
        super.onProgressChanged(view, newProgress);
        onProgressChangedListener.onProgressChanged(view,newProgress);
    }

    //扩展浏览器上传文件
    //3.0++版本
    public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
        openFileChooserImpl(uploadMsg);
    }

    //3.0--版本
    public void openFileChooser(ValueCallback<Uri> uploadMsg) {
        openFileChooserImpl(uploadMsg);
    }

    public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
        openFileChooserImpl(uploadMsg);
    }

    // Android 5.0以上
    @Override
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        onenFileChooseImpleForAndroid(filePathCallback);
        return true;
    }

    private void openFileChooserImpl(ValueCallback<Uri> uploadMsg) {
        mUploadMessage = uploadMsg;
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("image/*");
        mContext.startActivityForResult(Intent.createChooser(i, "File Chooser"), WebViewActivity.FILE_CHOOSER_RESULT_CODE);
    }

    private void onenFileChooseImpleForAndroid(ValueCallback<Uri[]> filePathCallback) {
        mUploadMessageForAndroid5 = filePathCallback;
        Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
        contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
        contentSelectionIntent.setType("image/*");

        Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
        chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
        chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
        mContext.startActivityForResult(chooserIntent, WebViewActivity.FILE_CHOOSER_RESULT_CODE_FOR_ANDROID_5);
    }




    public interface ReceivedTitleListener {
        void onReceivedTitle(WebView view, String title);
    }
    public interface ProgressChangedListener {
        void onProgressChanged(final WebView view, int newProgress);
    }
}