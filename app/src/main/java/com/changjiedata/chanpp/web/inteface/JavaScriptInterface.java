package com.changjiedata.chanpp.web.inteface;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.changjiedata.chanpp.utils.LogUtil;
import com.changjiedata.chanpp.web.WebViewActivity;


public class JavaScriptInterface {

    private Handler handler;

    public JavaScriptInterface(Handler handler) {
        this.handler = handler;
    }

    @JavascriptInterface
    public void back() {
        Message message = new Message();
        message.what = WebViewActivity.BACK;
        handler.sendMessage(message);
    }



    @JavascriptInterface
    public void goLogin(){
        LogUtil.e("frank","H5吊起登录");
        Message message = new Message();
        message.what = WebViewActivity.LOGIN;
        handler.sendMessage(message);
    }


    @JavascriptInterface
    public void goToCapture(){ //跳转实名认证方法
        Log.e("mango", "goToCapture: ");
        Message message = new Message();
        message.what = WebViewActivity.GOTOCAPTURE;
        handler.sendMessage(message);
    }






}