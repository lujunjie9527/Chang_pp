package com.changjiedata.chanpp.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class DialogUntil {

    public static void showDefaultDialog(Context context, String title, String message, final DialogImpl listener) {
        showDefaultDialog(context, title, message, "确定", "取消", listener);
    }

    public static void showDefaultDialog(Context context, String title, String message, String ok, String cancel, final DialogImpl listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onOk();
                    }
                })
                .setNegativeButton(cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onCancel();
                    }
                }).show();
    }

public static class DialogImpl {
    public void onOk() {
    }

    public void onCancel() {
    }
}

}
