package com.changjiedata.chanpp.net.interfaces;

import com.google.protobuf.InvalidProtocolBufferException;

public interface OnRequestCallBack {
        void onOk(byte[] bytes)throws InvalidProtocolBufferException;
        void onError(int errorCode, String errorMessage);

    }