package com.changjiedata.chanpp.net;


/**
 * H5部分 注意 ！！！！！此处不可定义变量值！！！！
 */

public interface WebApis {
    String serverHead = "";    //域名


    // 常见问题
    String commonProblem = serverHead + "dist/commonProblem.html";

    // 关于合利宝 url 传一个图片地址
    String webImage = serverHead + "dist/webImage.html?url=";

    /*-----------------------  商城 ----------------------------*/

    String shopping_details = serverHead + "dist/index.html#/shopping_details?id=";




    /*----------------------- 咨询 ----------------------------*/

    // 资讯详情
    String news = serverHead + "dist/index.html#/news/index?id=";


    /*----------------------- 首页 ----------------------------*/

    //     首页---qian_dao
    String signin = serverHead + "dist/index.html#/signIn";

    // 提现进度
    String progress = serverHead + "dist/index.html#/order/progress";


    // 订单详情
    String orderDetail = serverHead + "dist/index.html#/order/orderDetail";

    // 担保通知详情
    String operation = serverHead + "dist/index.html#/order/operation?id=";

    //   用户服务协议
    String Agreement = serverHead + "dist/index.html#/Agreement";

    // 关于我们
    String about = serverHead + "dist/index.html#/AboutUs";

    // 积分规则页面
    String Integral_rule = serverHead + "dist/index.html#/personal/Integral_rule";

    //排行榜规则页面
    String rankingExplain = serverHead + "dist/index.html#/rankingExplain";


    String shopping = serverHead + "dist/index.html#/shopping";

    String register = serverHead + "dist/index.html#/register";

    String Business_application = serverHead + "dist/index.html#/personal/Business_application";

    String checklxh = "http://www.lianhanghao.com";
}
