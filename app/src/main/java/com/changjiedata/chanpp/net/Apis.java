package com.changjiedata.chanpp.net;


public interface Apis {


    //域名
//    String serverHead =  BuildConfig.SERVERHEAD+"index.php/";
    String serverHead = "index.php/";  //index.php/

    // 查看是否已提交结算卡修改(code:1-已提交;0-未提交)
    String check_change = serverHead + "mobile/Merchantservice/check_change";

    // 修改详情
    String change_detail = serverHead + "mobile/Merchantservice/change_detail";

    // 获取修改列表
    String change_list = serverHead + "mobile/Merchantservice/change_list";


    //服务商制卡
    String add_shop_ios = serverHead + "mobile/Merchantservice/add_shop_ios";

    //商户入网第一步
    String merchant_reg = serverHead + "mobile/Merchantservice/merchant_reg";

    //商户入网第二步:
    String merchant_open = serverHead + "mobile/Merchantservice/merchant_open";

    //添加商铺:
    String add_shop = serverHead + "mobile/Merchantservice/add_shop";

    //申请标签:
    String apply_shop = serverHead + "mobile/Merchantservice/apply_shop";

    //写卡成功:
    String card_write = serverHead + "mobile/Merchantservice/card_write";

    //商铺列表:
    String shop_list = serverHead + "mobile/Merchantservice/shop_list";

    //获取MCC信息:
    String mcc_list = serverHead + "mobile/Merchantservice/mcc_list";

    //获取支持银行:
    String support_list = serverHead + "mobile/Merchantservice/support_list";

    //// 获取地区列表：/:
    String area_list = serverHead + "mobile/Merchantservice/area_list";

    // 退出登录接口
    String logout = serverHead + "mobile/logout/index";

    // 搜索mcc列表接口
    String getmcc = serverHead + "mobile/Merchantservice/getmcc";

    // 修改mcc接口
    String modify_mcc = serverHead + "mobile/Merchantservice/modify_mcc";


    // 获取支持列表接口
    String get_support = serverHead + "mobile/bank/get_support";

    //  人脸识别接口
    String face_check = serverHead + "mobile/Merchantservice/face_check";

    // 绑定信用卡发送短信
    String credit_send = serverHead + "mobile/Merchantservice/credit_send";

    // 绑定信用卡
    String credit_card = serverHead + "mobile/Merchantservice/credit_card";

    // 机具绑定接口
    String bind = serverHead + "mobile/Merchantservice/bind";

    //  获取代理信息接口
    String get_member_info = serverHead + "mobile/Member/get_member_info";

    //  商户审核接口
    String audit = serverHead + "mobile/Merchantservice/audit";

    //  发送验证码接口
    String send_code = serverHead + "mobile/Merchantservice/send_code";
    // 获取交易列表
    String get_trading_list = serverHead + "mobile/Statistics/get_trading_list";

    // 获取卡列表接口
    String get_bank = serverHead + "mobile/bank/get_bank";

    // 交易详情
    String get_trading_detail = serverHead + "mobile/Statistics/get_trading_detail";

    // 解绑信用卡接口
    String unbind_card = serverHead + "mobile/bank/unbind_card";

    // 我的设备接口
    String get_sn = serverHead + "mobile/Merchantservice/get_sn";

    // 帮助列表
    String information_index = serverHead + "mobile/information/index";

    // //统计接口
    String get_trend = serverHead + "mobile/Statistics/get_trend";

    String get_statistics = serverHead + "mobile/Statistics/get_statistics";

    // // C端收款-创建订单信息
    String created_order = serverHead + "mobile/member/created_order";


    /*-----------------------公共接口----------------------------*/
    //图片上传
    String uploadImage = serverHead + "mobile/member/img_upload";

    //微信支付
    String wechat_pay = serverHead + "mobile/Memberpayment/pay_new";
    //支付宝支付
    String ali_pay = serverHead + "mobile/Memberpayment/pay_new";

    /*-----------------------登录注册----------------------------*/
    //手机注册
    String register = serverHead + "mobile/connect/sms_register";
    //获取手机验证码
    String getMobileCode = serverHead + "mobile/connect/get_sms_captcha";
    //校验手机验证码
    String checkMobileCode = serverHead + "mobile/connect/check_sms_captcha";
    //登录
    String login = serverHead + "mobile/login/index";
    //忘记密码
    String forgetPassword = serverHead + "mobile/connect/find_password";
    // 修改密码
    String changePassword = serverHead + "mobile/connect/find_password";


    /*-----------------------用户信息----------------------------*/
    //个人信息
    String memberDataInfo = serverHead + "mobile/member/get_member_info";
    //修改个人信息
    String editMemberDataInfo = serverHead + "mobile/member/edit_member_info";
    //用户反馈
    String feedback_add = serverHead + "mobile/memberfeedback/feedback_add";
    //会员等级列表
    String grade_vip = serverHead + "?";
    //更换手机 绑定手机
    String update_phone = serverHead + "?act=connect&op=changePhone";

    // 获取用户信息及保存

    /*-----------------------首页链接----------------------------*/

    // 获取banner数据
    String get_banner = serverHead + "mobile/index/get_show_list";

    // 发布晒单的地址
    String add_dynamic = serverHead + "?act=fans_drying_list&op=releaseASheet";


    /*-----------------------分类链接----------------------------*/


    /*--------------------------商城----------------------------*/
    // 商城轮播图
    String get_shop_banner = serverHead + "mobile/index/goodsIndex";

    // 获取商城数据
    String get_shop_recycle_list = serverHead + "mobile/Index/goodsList";

    /*--------------------------咨询----------------------------*/
    String get_media = serverHead + "mobile/information/index";

    String information_like = serverHead + "mobile/information/information_like";
    //  获取银行卡信息
    String get_tixian = serverHead + "";

    // 申请提现
    String post_tixian = serverHead + "mobile/recharge/pd_cash_add";

    // 实名认证
    String member_approve = serverHead + "mobile/member/member_approve";

    // 今日收益
    String today_earnings = serverHead + "mobile/recharge/today_earnings";

    // 我的二维码
    String get_memberinviter = serverHead + "mobile/Memberinviter/get_my_qrcode_inviter";


    String update_paypwd = serverHead + "mobile/member/update_paypwd";

    // 支付密码验证
    String check_pd_pwd2 = serverHead + "mobile/memberbuy/check_pd_pwd2";

    // 添加银行卡
    String bank_card_add = serverHead + "mobile/member_bank/bank_card_add";

    // 银行卡列表
    String member_bank = serverHead + "mobile/member_bank/index";

    // 银行卡解除绑定
    String del_bank_card = serverHead + "mobile/member_bank/del_bank_card";
    // 获取地区列表
    String get_areas_list = serverHead + "mobile/area/area_list";
    // 支付密码检查
    String check_pd_pwd = serverHead + "mobile/Memberbuy/check_pd_pwd";

    // 调整商户费率（暂时不用）
    String updateSubmitAdjustmentRate = serverHead + "mobile/Mystock/updateSubmitAdjustmentRate";

    // 资讯分享
    String information_share = serverHead + "mobile/information/information_share";

    //  收入消息列表
    String pd_cash_list = serverHead + "mobile/recharge/earnings_detail_list";


    // 提现列表
    String pd_cash_list_log = serverHead + "mobile/member/pd_cash_list";

    // 奖励消息列表
    String reward_log = serverHead + "mobile/member/reward_log";

    // 订单列表
    String order_list = serverHead + "mobile/member/order_list";

    // 系统消息列表
    String system_log = serverHead + "mobile/member/system_log";
    // 获取客服信息
    String get_kfu_info = serverHead + "mobile/index/get_kfu_info";

    // 余额支付接口
    String balancePayment = serverHead + "mobile/memberpayment/balancePayment";

    // 获取协议
    String get_agreement = serverHead + "mobile/index/get_agreement";

    // 获取版本号
    String versionCode = serverHead + "mobile/index/get_app_version_info";

    // 提现详细
    String pd_cash_info = serverHead + "mobile/member/pd_cash_info";

    // 获取域名
    String get_site_domain = serverHead + "mobile/index/get_site_domain";

    // 签到页信息
    String signin_page = serverHead + "mobile/membersignin/signin_page";

    // qian_dao
    String signin = serverHead + "mobile/membersignin/signin";

    // 抽奖
    String draw = serverHead + "mobile/membersignin/draw";

    //积分列表
    String pointslog = serverHead + "mobile/memberpoints/pointslog";

    // 我的商户列表
    String listOfMyBusiness = serverHead + "mobile/Mystock/listOfMyBusiness";

    // 我的伙伴
    String my_partners_list = serverHead + "mobile/member/my_partners_list";

    // 排行榜
    String earnings_ranking = serverHead + "mobile/recharge/earnings_ranking";

    // 我的收益
    String my_earnings = serverHead + "mobile/recharge/my_earnings";

    // 收益明细
    String earnings_detail_info = serverHead + "mobile/recharge/earnings_detail_info";

    // 伙伴的机具
    String myMachine = serverHead + "mobile/Mystock/myMachine";

    // 我的库存列表
    String myMachineToolsList = serverHead + "mobile/Mystock/myMachineToolsList";

    // 调拨记录
    String allocationLogList = serverHead + "mobile/Mystock/allocationLogList";

    // 调拨记录详情
    String allocationLogInfo = serverHead + "mobile/Mystock/allocationLogInfo";

    // 确认收款
    String allocationConfirm = serverHead + "mobile/Mystock/allocationConfirm";

    // 分润规则
    String myProductRateInfo = serverHead + "mobile/Mystock/myProductRateInfo";

    // 调拨机具所属sn号列表
    String showProductSNCode = serverHead + "mobile/Mystock/showProductSNCode";

    String initiateATransfer = serverHead + "mobile/Mystock/initiateATransferNew";

    // sn号搜索
    String adjustmentRate = serverHead + "mobile/Mystock/adjustmentRate";

    // 查询我拥有的机具费率
    String showMyAllocationRateInfo = serverHead + "mobile/mystock/showMyAllocationRateInfo";

    // 调整我的费率
    String adjustMyRates = serverHead + "mobile/mystock/adjustMyRates";

    // 调整我的激活奖励
    String modifyActivateRewards = serverHead + "mobile/Mystock/modifyActivateRewards";

    // 申请自备机入网
    String applicationForSelf = serverHead + "mobile/Mystock/applicationForSelf";

    // 费改通知
    String examineAllocationInfo = serverHead + "mobile/Mystock/examineAllocationInfo";

    // 费改通知详情
    String examineAllocationDetail = serverHead + "mobile/Mystock/examineAllocationDetail";

    // 上级同意审核
    String parentExamine = serverHead + "mobile/Mystock/parentExamine";

    // 获取可以调拨的人
    String get_transfer_mb_list = serverHead + "mobile/Mystock/get_transfer_mb_list";

    // 获取调拨申请详情
    String get_sn_transfer_detail = serverHead + "mobile/Mystock/get_sn_transfer_detail";

    // 审核调拨
    String edit_sn_transfer = serverHead + "mobile/Mystock/edit_sn_transfer";

    // 获取调拨申请列表
    String get_sn_transfer_list = serverHead + "mobile/Mystock/get_sn_transfer_list";

    // 获取提现手续费
    String setting_info = serverHead + "mobile/index/get_pd_cash_setting_info";

    // 统计接口
    String my_stats = serverHead + "mobile/Statistics/my_stats";

    // 趋势接口
    String my_trend = serverHead + "mobile/Statistics/my_trend";

    // 结算卡修改
    String balance_change = serverHead + "mobile/Statistics/balance_change";

    // 获取商户入网二维码
    String get_platbg_list = serverHead + "mobile/index/get_platbg_list";

    // 获取资讯背景图
    String get_inforbg_list = serverHead + "mobile/index/get_inforbg_list";

    // 获取厂商型号
    String get_manufacturer_model = serverHead + "mobile/Mystock/get_manufacturer_model";

    // 总激活量
    String get_total_activation = serverHead + "mobile/Statistics/get_total_activation";

    // 月激活量
    String get_month_activation = serverHead + "mobile/Statistics/get_month_activation";

    // 月交易量
    String get_total_trading_month = serverHead + "mobile/Statistics/get_total_trading_month";

    // 月商户交易量
    String get_merchant_trading_month = serverHead + "mobile/Statistics/get_merchant_trading_month";

    // 月渠道交易量
    String get_channel_trading_month = serverHead + "mobile/Statistics/get_channel_trading_month";

    //  申请明细（费改通知）
    String examineAllocationList = serverHead + "mobile/Mystock/examineAllocationList";

    // 钱客通A类商户入网
    String get_platbga_list = serverHead + "mobile/index/get_platbga_list";
    // 申请扶持金
    String Supportfund_add = serverHead + "mobile/Supportfund/add";

    // 搜索sn号
    String searchSNCodeGoods = serverHead + "mobile/mystock/searchSNCodeGoods";

    // 收益（曲线图）
    String get_total_earnings = serverHead + "mobile/Statistics/get_total_earnings";

    // 收益构成（饼状图）
    String get_form_earnings = serverHead + "mobile/Statistics/get_form_earnings";

    // 获取交易趋势接口（曲线图）
    String get_trading_tend = serverHead + "mobile/Statistics/get_trading_tend";

    //获取月交易（饼状图）
    String get_trading_month = serverHead + "mobile/Statistics/get_trading_month";


    // 获取激活趋势接口（曲线图）
    String get_activation_tend = serverHead + "mobile/Statistics/get_activation_tend";

    // 获取激活构成（饼状图）
    String get_activation_month = serverHead + "mobile/Statistics/get_activation_month";

    // 获取新增趋势接口（曲线图）
    String get_increase = serverHead + "mobile/Statistics/get_increase";

    // 获取台均（饼状图）
    String get_sn_average = serverHead + "mobile/Statistics/get_sn_average";

    // 获取台均（饼状图）
    String get_average_list = serverHead + "mobile/Statistics/get_average_list";

    // 邀请码验证
    String check_inviter = serverHead + "mobile/Connect/check_inviter";

    // 提现金额查询接口
    String check_cash = serverHead + "mobile/recharge/check_cash";

    // 商铺详情：
    String shop_detail = serverHead + "mobile/Merchantservice/shop_detail";

    // 支行列
    String channel_list = serverHead + "mobile/Merchantservice/channel_list";

    // 更换结算卡
    String change_bank = serverHead + "mobile/Merchantservice/change_bank";

    // 获取MCC信息
    String get_mcc = serverHead + "mobile/Merchantservice/get_mcc";

//    // 退出登录接口
//    String logout = serverHead + "mobile/logout/index";
}
