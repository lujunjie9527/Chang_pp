package com.changjiedata.chanpp.net;

import android.app.Application;
import android.text.TextUtils;

import com.changjiedata.chanpp.BuildConfig;
import com.changjiedata.chanpp.base.MainApplication;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.net.interfaces.OnUpLoadCallBack;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.proto.PBPublicOuterClass;
import com.changjiedata.chanpp.utils.ValidateUtils;
import com.lzy.okgo.OkGo;


import java.io.File;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by intexh on 2016/6/4.
 */
public enum NetworkManager {
    INSTANCE;

    public void init(Application application) {
        //必须调用初始化
        OkGo.getInstance().init(application);
//        OkHttpClient.Builder builder = new OkHttpClient.Builder();
//        //全局的读取超时时间
//        builder.readTimeout(15000, TimeUnit.MILLISECONDS);
//        //全局的写入超时时间
//        builder.writeTimeout(15000, TimeUnit.MILLISECONDS);
//        //全局的连接超时时间
//        builder.connectTimeout(15000, TimeUnit.MILLISECONDS);
    }

    //请求 post
    public void post(String api, Map<String, String> params, OnRequestCallBack callBack) {

        if (TextUtils.isEmpty(MainApplication.getServiceHost())) {
            RequestModel.INSTANCE.post("", BuildConfig.SERVERHEAD + api, params, callBack);
        } else {
            RequestModel.INSTANCE.post("", MainApplication.getServiceHost() + api, params, callBack);
        }
    }

    //请求 get
    public void get(String api, Map<String, String> params, OnRequestCallBack callBack) {
        if (TextUtils.isEmpty(MainApplication.getServiceHost())) {
            RequestModel.INSTANCE.get("", BuildConfig.SERVERHEAD + api, params, callBack);
        } else {
            RequestModel.INSTANCE.get("", MainApplication.getServiceHost() + api, params, callBack);
        }
    }

    //请求文件上传
    public void upLoadFile(String api, String key, File file, OnUpLoadCallBack callBack) {
        RequestModel.INSTANCE.upLoadFile(api, key, file, callBack);


    }


    //请求 post
    public void post(String api, byte[] data, int page, OnRequestCallBack callBack) {
        Map<String, String> params = new HashMap<>();
        if (ValidateUtils.isValidate(data)) {
            params.put("data", byteArrayToString(data));
        }
        // 公共方法
        PBPublicOuterClass.PBPublic.Builder builder = PBPublicOuterClass.PBPublic.newBuilder();
        if (UserHelper.isLogin()) builder.setKey(UserHelper.getCurrentToken());
        builder.setClient("android");
        if (page != 0) {
            builder.setPage(page);
            builder.setPagesize(10);
        }
        PBPublicOuterClass.PBPublic public_data = builder.build();
        params.put("dtp", byteArrayToString(public_data.toByteArray()));
        if (TextUtils.isEmpty(MainApplication.getServiceHost())) {
            RequestModel.INSTANCE.post("", BuildConfig.SERVERHEAD + api, params, callBack);
        } else {
            RequestModel.INSTANCE.post("", MainApplication.getServiceHost() + api, params, callBack);
        }
    }

    //请求 post
    public void post(String api, byte[] data, OnRequestCallBack callBack) {
        post(api, data, 0, callBack);  // 默认为 0 的时候不给 Page pagesize等公共参数
    }

    //请求 post
    public void post(String api, OnRequestCallBack callBack) {
        post(api, null, 0, callBack);  // 默认为 0 的时候不给 Page pagesize等公共参数
    }

    public void post(String api,int page, OnRequestCallBack callBack) {
        post(api, null, page, callBack);  // 默认为 0 的时候不给 Page pagesize等公共参数
    }

    private   String byteArrayToString(byte[] bytes) {
        StringBuilder stringBuffer = new StringBuilder();
        for (byte aByte : bytes) {
            stringBuffer.append((int) aByte).append(",");
        }
        stringBuffer.deleteCharAt(stringBuffer.length() - 1);  //
        return stringBuffer.toString();
    }

}
