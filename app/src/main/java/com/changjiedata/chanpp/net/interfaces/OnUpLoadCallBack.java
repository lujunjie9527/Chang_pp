package com.changjiedata.chanpp.net.interfaces;

import com.google.protobuf.InvalidProtocolBufferException;

public interface OnUpLoadCallBack {
        void onOk(String response) throws InvalidProtocolBufferException;
        void onError(int code, String errorMessage);
        void upProgress(int progress);

    }