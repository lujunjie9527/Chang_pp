package com.changjiedata.chanpp.module.shanghunet;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.helper.UserBean;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.module.login.LoginActivity;
import com.changjiedata.chanpp.module.main.CreateSelectCardActivity;
import com.changjiedata.chanpp.module.main.LocationSelectActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Connect;
import com.changjiedata.chanpp.proto.MemberOuterClass;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.changjiedata.chanpp.utils.DialogUntil;
import com.changjiedata.chanpp.utils.GsonUtils;
import com.changjiedata.chanpp.utils.ValidateUtils;
import com.changjiedata.chanpp.view.RecycleViewBankDialog;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kale.ui.view.dialog.EasyDialog;

public class NetOneActivity extends BaseActivity {
    @BindView(R.id.shanghu_name_et)
    EditText shanghuNameEt;
    @BindView(R.id.shanghu_subname_et)
    EditText shanghuSubnameEt;
    @BindView(R.id.shanghu_id_et)
    EditText shanghuIdEt;
    @BindView(R.id.jiesuan_number_et)
    EditText jiesuanNumberEt;
    @BindView(R.id.etUserName)
    EditText etUserName;
    @BindView(R.id.jiesuan_bank_tv)
    TextView jiesuanBankTv;
    @BindView(R.id.tvCheckLXH)
    TextView tvCheckLXH;
    @BindView(R.id.jiesuan_area_tv)
    TextView jiesuanAreaTv;
    @BindView(R.id.subbank_name_tv)
    TextView subbankNametv;
    @BindView(R.id.phone_edt)
    EditText phoneEdt;
    @BindView(R.id.mobile_code_et)
    EditText mobileCodeEt;
    @BindView(R.id.get_code_tv)
    TextView getCodeTv;
    @BindView(R.id.lian_xin_number_et)
    EditText lianXinNumberEt;
    @BindView(R.id.next_btn)
    Button nextBtn;
    private String mPhone;
    private String city;
    private List<Merchantservice.basics> banks = new ArrayList<>();
    private String province="";
    private String county;
    private String sub_name;
    private String channel;
    private String bank_code;
    private String bank_name="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_hu_net_one);
        ButterKnife.bind(this);
        get_support();
    }


    @OnClick({R.id.tvExit, R.id.jiesuan_bank_tv, R.id.jiesuan_area_tv, R.id.get_code_tv, R.id.next_btn,R.id.subbank_name_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvExit:
                showTip();
                break;
            case R.id.jiesuan_bank_tv:
                showBank();
                break;
            case R.id.jiesuan_area_tv:
                Intent intent = new Intent(mContext, LocationSelectActivity.class);
                intent.putExtra("type", "1");
                startActivityForResult(intent, 100);
                break;
            case R.id.get_code_tv:
                getCode();
                break;
            case R.id.next_btn:
                submit();
                break;
            case R.id.subbank_name_tv:
                selectBank();
                break;
        }
    }

    private void selectBank() {
        if (bank_name.isEmpty())
        {
            showToast("请先选择银行名称");
            return;
        }
        if (province.isEmpty())
        {
            showToast("请先选择地区");
            return;
        }
        Intent intent = new Intent(mContext, CreateSelectCardActivity.class);
        intent.putExtra("provice",province);
        intent.putExtra("city",city);
        intent.putExtra("county",county);
        intent.putExtra("bankName",bank_name);
        startActivityForResult(intent, 200); // 获取银行
    }

    /**
     * 获取个人信息
     */

    private void get_member_info() {
        NetworkManager.INSTANCE.post(Apis.memberDataInfo, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                hideProgress();
                MemberOuterClass.Member member = MemberOuterClass.Member.parseFrom(bytes);
                UserBean userBean = new UserBean();
                userBean.setStep(member.getStep());
                userBean.setMember_id(member.getMemberId());
                UserHelper.syncCurrentUserInfo(userBean);   //同步个人信息
                Intent intent = new Intent(mContext, NetTwoActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onError ( int errorCode, String errorMessage){
                hideProgress();
                showToast(errorMessage);

            }
        });
    }


    public void submit() {
        String name = shanghuNameEt.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            showToast("请填写商户全称");
            return;
        }
        String subname = shanghuSubnameEt.getText().toString().trim();
        if (TextUtils.isEmpty(subname)) {
            showToast("请填写商户简称");
            return;
        }
        String qrcode = shanghuIdEt.getText().toString().trim();
        if (TextUtils.isEmpty(qrcode)) {
            showToast("请输入身份证号码");
            return;
        }
        if (qrcode.length() < 18) {
            showToast("请输入18位身份证号码");
            return;
        }
        String card = jiesuanNumberEt.getText().toString().trim();
        if (TextUtils.isEmpty(card)) {
            showToast("请填写您本人的结算卡卡号");
            return;
        }
        String userName = etUserName.getText().toString().trim();
        if (TextUtils.isEmpty(userName)) {
            showToast("请填写持卡人姓名");
            return;
        }
        String bank = jiesuanBankTv.getText().toString().trim();
        if (TextUtils.isEmpty(bank)) {
            showToast("请填写结算卡所属银行名称");
            return;
        }
        String subbank = subbankNametv.getText().toString().trim();
        if (TextUtils.isEmpty(subbank)) {
            showToast("请填写结算卡所属支行名称");
            return;
        }
        String mobile = phoneEdt.getText().toString().trim();
        if (TextUtils.isEmpty(mobile)) {
            showToast("请输入手机号码");
            return;
        }
        if (mobile.length() != 11) {
            showToast("请输入11位手机号码");
            return;
        }
        String captcha = mobileCodeEt.getText().toString().trim();
        if (TextUtils.isEmpty(captcha)) {
            showToast("请输入手机验证码");
            return;
        }
        showProgress();
        Merchantservice.merchant.Builder builder = Merchantservice.merchant.newBuilder();
        builder.setMerName(name);// 商户名称
        builder.setMerAbbr(subname);// 商户简称
        builder.setIdCard(qrcode);// 身份证号
        builder.setBankCard(card);//  银行卡号
        builder.setAccountName(userName); // 持卡人姓名
        builder.setMobile(mobile); // 预留手机号
        builder.setBankName(bank); // 银行名称
        builder.setSubbankName(subbank); // 支行名称
        builder.setBankCode(bank_code); // 银行编码
        builder.setChannel(channel); // 联行号
        builder.setProvince(province); // 省
        builder.setCity(city); // 市
        builder.setCaptcha(captcha); // 验证码
        Merchantservice.merchant data = builder.build();
        NetworkManager.INSTANCE.post(Apis.merchant_reg, data.toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) {
                hideProgress();
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                if (errorCode == 200) {
                   get_member_info();
                } else {
                    hideProgress();
                    showToast(errorMessage);
                }


            }
        });
    }

    // 获取支持银行
    public void get_support() {
        NetworkManager.INSTANCE.post(Apis.support_list, 10, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Merchantservice.polling datas = Merchantservice.polling.parseFrom(bytes);
                Log.e("ljj获取支持银行", datas.toString());
                banks.addAll(datas.getListList());
            }

            @Override
            public void onError(int code, String errorMessage) {
            }
        });
    }

    //
    private void showBank() {
        if (ValidateUtils.isValidate(banks)) {
            RecycleViewBankDialog recycleViewBankDialog = new RecycleViewBankDialog(new RecycleViewBankDialog.onItemBankClickListener() {
                @Override
                public void setBankText(String code, String bank) {
                    channel = "";
                    sub_name = "";
                    subbankNametv.setText("");
                    bank_name = bank;
                    bank_code = code;
                    jiesuanBankTv.setText(bank_name);
                }
            });
            RecycleViewBankDialog.Builder builder = new RecycleViewBankDialog.Builder(mContext);
            builder.setData(GsonUtils.serializedToJson(banks)).setTitle("支持银行");
            EasyDialog build = builder.build();
            build.show(getSupportFragmentManager());
        } else {
            showToast("暂无支持银行");
        }
    }

    /**
     * 获取验证码
     */
    private void getCode() {
        mPhone = phoneEdt.getText().toString();
        if (TextUtils.isEmpty(mPhone)) {
            showToast("请输入手机号码");
            return;
        }
        if (mPhone.length() != 11) {
            mPhone = null;
            showToast("请输入11位手机号码");
            return;
        }
        Connect.captcha.Builder builder = Connect.captcha.newBuilder();
        builder.setPhone(mPhone);
        builder.setType("5");  //1:注册会员 2：3：重置密码  5：商户入网
        Connect.captcha data = builder.build();
        showProgress();
        NetworkManager.INSTANCE.post(Apis.getMobileCode, data.toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                countDownReSend(getCodeTv, 60);
                hideProgress();
            }

            @Override
            public void onError(int code, String errorMessage) {
                hideProgress();
                showToast(errorMessage);
                if (code == 200) {
                    countDownReSend(getCodeTv, 60);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == 100) {
            province = data.getStringExtra("province");
            city = data.getStringExtra("city");
            county = data.getStringExtra("county");
            jiesuanAreaTv.setText(province + " " + city + " " + county);
        }

        if (resultCode == RESULT_OK && requestCode == 200) {
            sub_name = data.getStringExtra("name");
            channel = data.getStringExtra("code");
            subbankNametv.setText(sub_name);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        showTip();
        return true;
    }

    private void showTip() {
        DialogUntil.showDefaultDialog(this, "", "确定退出到登录页面？", new DialogUntil.DialogImpl() {
            @Override
            public void onOk() {
                Intent intent2 = new Intent(mContext, LoginActivity.class);
                intent2.putExtra("phone", UserHelper.getUser().getMember_mobile());
                UserHelper.loginOut();
                startActivity(intent2);
                UserHelper.loginOut();
                finish();
            }
        });
    }

}
