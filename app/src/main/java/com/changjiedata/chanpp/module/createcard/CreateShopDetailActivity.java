package com.changjiedata.chanpp.module.createcard;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.adapter.CreateShopDetailAdapter;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.changjiedata.chanpp.utils.GsonUtils;
import com.changjiedata.chanpp.utils.StatusBarUtil;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateShopDetailActivity extends BaseActivity {

    @BindView(R.id.back_iv)
    ImageView backTv;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private CreateShopDetailAdapter mAdapter;
    private List<Merchantservice.shop> datas = new ArrayList<>();
    private int type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);
        ButterKnife.bind(this);
        StatusBarUtil.setStatusBarLightMode(this, Color.parseColor("#ffffff"), true);
        tvTitle.setText(getIntent().getStringExtra("title"));
        type = getIntent().getIntExtra("type", 0);
        initRecyclerView();
        initListener();

    }

    private void initListener() {
        backTv.setOnClickListener(view -> finish());

    }

    private void initData() {
        showProgress();
        Merchantservice.shop_list.Builder builder = Merchantservice.shop_list.newBuilder();
        builder.setMemberId(UserHelper.getUser().getMember_id());
        NetworkManager.INSTANCE.post(Apis.shop_list, builder.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] response) throws InvalidProtocolBufferException {
                hideProgress();
                Merchantservice.shop_list list = Merchantservice.shop_list.parseFrom(response);
                List<Merchantservice.shop> datas = list.getListList();
                mAdapter.replaceData(datas);
            }
            @Override
            public void onError(int errorCode, String errorMessage) {
                showToast(errorMessage);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(mAdapter = new CreateShopDetailAdapter(mContext, datas));
        mAdapter.bindToRecyclerView(recyclerView);
        mAdapter.setEmptyView(R.layout.recycle_empty);
        // 每个item的点击事件
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            if (type == 0) {
                Intent intent;
//                if (datas.get(position).getStatus().equals("0")) {
//                    intent = new Intent(mContext, CreateSelectCardActivity.class);
//                } else {
//                    intent = new Intent(mContext, CreateCardActivity.class);
//                }
//                intent.putExtra("data", GsonUtils.serializedToJson(datas.get(position)));
//                startActivity(intent);

            } else {
                Intent intent = new Intent(mContext, CreateCheckActivity.class);
                intent.putExtra("data", GsonUtils.serializedToJson(datas.get(position)));
                intent.putExtra("title", getIntent().getStringExtra("title"));
                startActivity(intent);
            }
        });
    }


}
