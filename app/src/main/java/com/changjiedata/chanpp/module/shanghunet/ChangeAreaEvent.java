package com.changjiedata.chanpp.module.shanghunet;

/**
 * Created by AndroidIntexh1 on 2018/11/3.
 */

public class ChangeAreaEvent {
    public String getAreaId() {
        return areaId;
    }

    public String getArea_name() {
        return area_name;
    }

    private String areaId;
   private String area_name;
    public ChangeAreaEvent(String areaId, String area_name) {
        this.areaId = areaId;
        this.area_name = area_name;
    }
}
