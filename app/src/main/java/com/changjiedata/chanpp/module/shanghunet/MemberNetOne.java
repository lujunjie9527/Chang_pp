package com.changjiedata.chanpp.module.shanghunet;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.helper.GlideHelper;
import com.changjiedata.chanpp.helper.ImageHelper;
import com.changjiedata.chanpp.helper.PermissionHelper;
import com.changjiedata.chanpp.module.main.TakeCardPhotoActivity;
import com.changjiedata.chanpp.proto.MemberOuterClass;
import com.google.protobuf.InvalidProtocolBufferException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MemberNetOne extends BaseActivity {
    @BindView(R.id.name_et)
    EditText nameEt;
    @BindView(R.id.id_et)
    EditText idEt;
    @BindView(R.id.id_zheng_iv)
    ImageView idZhengIv;
    @BindView(R.id.id_bei_iv)
    ImageView idBeiIv;
    @BindView(R.id.hand_zheng_iv)
    ImageView handZhengIv;
    @BindView(R.id.hand_zheng2_iv)
    ImageView handZheng2Iv;
    private String name;
    private String qrcode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_hu_net);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.back_iv, R.id.id_zheng_iv, R.id.id_bei_iv,   R.id.hand_zheng2_iv, R.id.next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.id_zheng_iv:
                break;
            case R.id.id_bei_iv:
                break;

            case R.id.hand_zheng2_iv:
                break;
            case R.id.next_btn:
                submit();
                break;
        }
    }
    public void submit() {
       name=nameEt.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            showToast("请输入姓名");
            return;
        }
        qrcode=idEt.getText().toString().trim();
        if (TextUtils.isEmpty(qrcode)) {
            showToast("请输入身份证号码");
            return;
        }
        if (qrcode.length() < 18) {
            showToast("请输入18位身份证号码");
            return;
        }
    }

    public void selectPicture(int type) {
        PermissionHelper.getInstance().getPermission(mContext, new PermissionHelper.PermissionCallBack() {
            @Override
            public void onSuccess() {
                upLoadImage("");
            }

            @Override
            public void onFail() {
                showToast("请先允许权限再使用此功能");
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }


    public void selectQrCard(int type) {
        PermissionHelper.getInstance().getPermission(mContext, new PermissionHelper.PermissionCallBack() {
            @Override
            public void onSuccess() {

                Intent intent = new Intent(mContext, TakeCardPhotoActivity.class);
                intent.putExtra("type", type);
                startActivity(intent);

            }

            @Override
            public void onFail() {
                showToast("请先允许权限再使用此功能");
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }


    // 上传图片
    public void upLoadImage(String path) {
        ImageHelper.upLoadImage(path, new ImageHelper.OnUpLoadImageCallBack() {
            @Override
            public void onSuccess(String images) throws InvalidProtocolBufferException {
                String[] temp = images.split(",");
                byte[] bytes = new byte[temp.length];
                for (int i = 0; i < temp.length; i++) {
                    bytes[i] = (byte) Integer.parseInt(temp[i]);
                }
                GlideHelper.INSTANCE.loadImage(idZhengIv, MemberOuterClass.img_upload.parseFrom(bytes).getImg());
            }

            @Override
            public void onFail(String error) {
                showToast(error);
            }
        });
    }

}



