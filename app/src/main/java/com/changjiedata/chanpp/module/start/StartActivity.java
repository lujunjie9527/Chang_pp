package com.changjiedata.chanpp.module.start;

import android.os.Bundle;
import android.os.Handler;

import com.changjiedata.chanpp.BuildConfig;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.base.MainApplication;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.module.login.LoginActivity;
import com.changjiedata.chanpp.module.main.MainActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.proto.Index;
import com.changjiedata.chanpp.utils.PreferenceSettings;
import com.google.protobuf.InvalidProtocolBufferException;


public class StartActivity extends BaseActivity {

    private boolean isDestory = false;
    private String IS_FIRST = "is_have_welcome";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        boolean isFirst = PreferenceSettings.getBoolean(IS_FIRST, true);
        MainApplication.setServiceHost(BuildConfig.SERVERHEAD);
        NetworkManager.INSTANCE.post(Apis.get_site_domain, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] response) throws InvalidProtocolBufferException {
//                showToast(response);
//                MainApplication.setServiceHost("http://dev.gdzuanqian.net/");
                MainApplication.setServiceHost(Index.get_site_domain.parseFrom(response).getHost());
                new Handler().postDelayed(() -> {
                    if (isFirst) {
                        startActivity(GuideActivity.class);
                    } else {
                        if (UserHelper.isLogin() && UserHelper.getStep().equals("3")) {
                            startActivity(MainActivity.class);
                        } else {
                            UserHelper.loginOut();//清除登录信息
                            startActivity(LoginActivity.class);
                        }
                    }
                    finish();
                }, 1000);
            }

            @Override
            public void onError(int code, String errorMessage) {
                showHintDialog(errorMessage);
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        isDestory = true;
    }
}
