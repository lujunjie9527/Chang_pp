package com.changjiedata.chanpp.module.shop;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SucceedCardActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_card_succeed);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnNext)
    public void onViewClicked() {
        finish();
    }
}
