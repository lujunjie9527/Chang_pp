package com.changjiedata.chanpp.module.mine;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.adapter.MessageAdapter;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.proto.MemberOuterClass;
import com.changjiedata.chanpp.utils.ValidateUtils;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MessageListActivity extends BaseActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private MessageAdapter mAdapter;
    private List<MemberOuterClass.SysteLog.SystemLogList> datas = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);
        ButterKnife.bind(this);
        system_log(10);
        initRecyclerView();
    }

    @OnClick(R.id.back_iv)
    public void onViewClicked() {
        finish();
    }


    // 消息列表
    public void system_log(int page) {
        NetworkManager.INSTANCE.post(Apis.system_log, page, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                MemberOuterClass.SysteLog systeLog = MemberOuterClass.SysteLog.parseFrom(bytes);
                datas.addAll(systeLog.getSystemLogListList());
                Log.e("ljj消息列表",systeLog.getSystemLogListList().toString());
                if (ValidateUtils.isValidate(systeLog.getSystemLogListList())) {
                    //缺省页

                }
            }

            @Override
            public void onError(int code, String errorMessage) {
                showToast(errorMessage);
            }
        });
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(mAdapter = new MessageAdapter(datas));
        mAdapter.bindToRecyclerView(recyclerView);
        if(!ValidateUtils.isValidate(datas)){//
            mAdapter.setEmptyView(R.layout.recycle_empty);
        }
    }

}
