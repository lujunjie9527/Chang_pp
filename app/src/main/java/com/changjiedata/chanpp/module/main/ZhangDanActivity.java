package com.changjiedata.chanpp.module.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.adapter.ZhanDanAdapter;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.bean.ZhanDanBean;
import com.changjiedata.chanpp.dialog.SelectDayDialog;
import com.changjiedata.chanpp.dialog.SelectTypeDialog;
import com.changjiedata.chanpp.dialog.SelectYearMonthDialog;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.proto.Statistics;
import com.changjiedata.chanpp.utils.ValidateUtils;
import com.changjiedata.chanpp.utils.date.DateUtil;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kale.ui.view.dialog.EasyDialog;

public class ZhangDanActivity extends BaseActivity {
    @BindView(R.id.month_sel_tv)
    TextView monthSelTv;
    @BindView(R.id.day_sel_tv)
    TextView timeSelTv;
    @BindView(R.id.sel_type_tv)
    TextView selTypeTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private int mYear;
    private int mMonth;
    private String mFirstDay;
    private String mEndDay ;
    private String mType  ;
    private ZhanDanAdapter mAdapter;
    private List<ZhanDanBean> datas = new ArrayList<>();
    //    private String customTime = "";
    private String id;
    private String time;
    private int page = 1;
    private int pagesize = 10;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zhang_dan);
        ButterKnife.bind(this);
        monthSelTv.setText(DateUtil.getYear() + "年" + String.format("%02d",DateUtil.getMonth() )+ "月");

        mYear = DateUtil.getYear();
        mMonth = DateUtil.getMonth();
        mFirstDay = "01";
        mType = "1";
        mEndDay = DateUtil.getLastDayOfMonth(DateUtil.getYear(), DateUtil.getMonth());
        initRecyclerView();
        loadData();

    }

    @OnClick({R.id.back_iv, R.id.month_sel_tv, R.id.day_sel_tv, R.id.sel_type_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.month_sel_tv:
                SelectYearMonthDialog.Builder builder = new SelectYearMonthDialog.Builder(this);
                builder.setYearAndMonth(mYear, mMonth);
                builder.setOnDialogClickListener(new SelectYearMonthDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(View v, String month) {
                        mYear = Integer.parseInt(month.substring(0, 4));
                        mMonth = Integer.parseInt(month.substring(4, 6));
                        monthSelTv.setText(mYear + "年" + mMonth + "月");
                        mFirstDay = "01";
                        mEndDay = DateUtil.getLastDayOfMonth(Integer.valueOf(mYear), Integer.valueOf(mMonth));
                        timeSelTv.setText(mFirstDay + "日 至 " + mEndDay + "日");
                        datas.clear();
                        page = 1;
                        mAdapter.setEnableLoadMore(true);
                        loadData();
                    }
                });
                EasyDialog build = builder.build();
                build.setCancelable(true);
                build.show(getSupportFragmentManager());
                break;
            case R.id.day_sel_tv:
                SelectDayDialog.Builder builder1 = new SelectDayDialog.Builder(this);
                builder1.setYearAndMonth(Integer.valueOf(mYear), Integer.valueOf(mMonth));
                builder1.setOnDialogClickListener(new SelectDayDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(View v, String first, String end) {
                        mFirstDay = first;
                        mEndDay = end;
                        timeSelTv.setText(mFirstDay + "日 至 " + mEndDay + "日");
                        datas.clear();
                        page = 1;
                        mAdapter.setEnableLoadMore(true);
                        loadData();
                    }
                });
                EasyDialog build1 = builder1.build();
                build1.setCancelable(true);
                build1.show(getSupportFragmentManager());
                break;
            case R.id.sel_type_tv:
                SelectTypeDialog.Builder builder2 = new SelectTypeDialog.Builder(mContext);
                builder2.setOnDialogClickListener((v, type) -> {
                    mType = type;
                    datas.clear();
                    page = 1;
                    if (type.equals("1")) {
                        selTypeTv.setText("全部收款");
                    } else if (type.equals("2")) {
                        selTypeTv.setText("扫码");
                    } else {
                        selTypeTv.setText("碰一碰");
                    }
                    loadData();
                });
                EasyDialog build2 = builder2.build();
                build2.setCancelable(true);
                build2.show(getSupportFragmentManager());
                break;
        }
    }

    // 获取首页交易数据
    public void loadData() {
        Statistics.get_trading_list.Builder builders = Statistics.get_trading_list.newBuilder();
        builders.setType(mType);
        builders.setMonth(mYear + "-"+ String.format("%02d", mMonth));
        builders.setStartDay(mFirstDay);
        builders.setEndDay(mEndDay);
        Statistics.get_trading_list param = builders.build();
        Log.e("ljj账单请求数据", param.toString());
//        showToast("page:"+page+"type:" + typeNumber + "month:" + month + "dayFirst:" + dayFirst + "dayEnd:" + dayEnd);
        NetworkManager.INSTANCE.post(Apis.get_trading_list, param.toByteArray(), page, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Statistics.get_trading_list repsonDatas = Statistics.get_trading_list.parseFrom(bytes);
                List<Statistics.trading> tradings = repsonDatas.getListList();
                String customTime = "";
                if (ValidateUtils.isValidate(datas)) {
                    customTime = DateUtil.getStrTimeFormat(datas.get(datas.size() - 1).getTime(), "yyyy-MM-dd");
                }
                List<ZhanDanBean> beans = new ArrayList<>();
                for (Statistics.trading in : tradings) {
                    String itemTime = DateUtil.getStrTimeFormat(in.getTime(), "yyyy-MM-dd");
                    if (!customTime.equals(itemTime)) {
                        customTime = itemTime;
                        beans.add(new ZhanDanBean(itemTime));
                    }
                    ZhanDanBean beanNext = new ZhanDanBean(in);
                    beans.add(beanNext);
                }
                mAdapter.addData(beans);
                int size = tradings.size();
                if (size < pagesize) {
                   mAdapter.loadMoreEnd(false);
                } else {
                    mAdapter.loadMoreComplete();
                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                hideProgress();
                showToast(errorMessage);
            }
        });
    }


    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(mAdapter = new ZhanDanAdapter(datas));
        mAdapter.bindToRecyclerView(recyclerView);
        mAdapter.setEmptyView(R.layout.layout_none);//recycle_empty
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            ZhanDanBean zhanDanBean = mAdapter.getData().get(position);
            if (zhanDanBean.getMytype() == 2)  // 内容栏
            {
                id = zhanDanBean.getId();
                time = zhanDanBean.getTime();
                Intent intent = new Intent(mContext, ZhangDanDetailActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("time", time);
                startActivity(intent);
            }
        });
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                page = datas.size() / pagesize + 1;
                loadData();
            }
        },recyclerView);
    }
}
