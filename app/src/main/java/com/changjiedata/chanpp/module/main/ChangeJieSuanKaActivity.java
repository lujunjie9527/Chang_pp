package com.changjiedata.chanpp.module.main;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.helper.GlideHelper;
import com.changjiedata.chanpp.helper.ImageHelper;
import com.changjiedata.chanpp.helper.PermissionHelper;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Connect;
import com.changjiedata.chanpp.proto.MemberOuterClass;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.changjiedata.chanpp.utils.CompressUtils;
import com.changjiedata.chanpp.utils.GsonUtils;
import com.changjiedata.chanpp.utils.ValidateUtils;
import com.changjiedata.chanpp.view.RecycleViewBankDialog;
import com.google.protobuf.InvalidProtocolBufferException;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kale.ui.view.dialog.EasyDialog;

//修改结算卡
public class ChangeJieSuanKaActivity extends BaseActivity {

    @BindView(R.id.tv_bank_user)
    TextView tvBankUser;
    @BindView(R.id.et_bank_card_number)
    EditText etBankCardNumber;
    @BindView(R.id.et_bank_name)
    TextView etbank_name;
    @BindView(R.id.tv_bank_local)
    TextView tvBankLocal;
    @BindView(R.id.tv_bank_support)
    TextView tvBankSupport;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.tv_get_code)
    TextView tvGetCode;
    @BindView(R.id.id_zheng_iv)
    ImageView idZhengIv;
    @BindView(R.id.bind_btn)
    Button bindBtn;

    private String address = "";
    private String province = "";
    private String city = "";
    private String county = "";
    private String bank_code = "";
    private List<Merchantservice.basics> banks = new ArrayList<>();
    private String channel;
    private String sub_name;
    private String bank_name = "";



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_jie_suan_ka);
        ButterKnife.bind(this);
        tvBankUser.setText(UserHelper.getUser().getTruename());
        get_support();
    }


    public void submit() {
        String name = tvBankUser.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            showToast("请输入姓名");
            return;
        }
        String card = etBankCardNumber.getText().toString().trim();
        if (TextUtils.isEmpty(card)) {
            showToast("请填写您本人银行卡号");
            return;
        }
        String mobile = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(mobile)) {
            showToast("请输入手机号码");
            return;
        }
        if (mobile.length() != 11) {
            showToast("请输入11位手机号码");
            return;
        }
        String captcha = etCode.getText().toString().trim();
        if (TextUtils.isEmpty(captcha)) {
            showToast("请输入手机验证码");
            return;
        }
        String sub_name = tvBankSupport.getText().toString();
        if (TextUtils.isEmpty(sub_name)) {
            showToast("请先选择支行名称");
            return;
        }
        if (TextUtils.isEmpty(address)) {
            showToast("请上传银行卡正面照片");
            return;
        }


        showProgress();
        Merchantservice.change_bank.Builder builder = Merchantservice.change_bank.newBuilder();
        builder.setBankCard(card).setPhone(mobile).setBankName(bank_name).setBankCode(bank_code).setImg(address)
                .setSubName(sub_name).setChannel(channel).setProvince(province).setCity(city).setCaptcha(captcha);
        NetworkManager.INSTANCE.post(Apis.change_bank, builder.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) {
            }
            @Override
            public void onError(int errorCode, String errorMessage) {
                showToast(errorMessage);
                if (errorCode == 200)
                {
                  finish();
                }

            }
        });


    }

    /**
     * 获取验证码
     */
    private void getCode() {
       String mPhone = etPhone.getText().toString();
        if (TextUtils.isEmpty(mPhone)) {
            showToast("请输入手机号码");
            return;
        }
        if (mPhone.length() != 11) {
            showToast("请输入11位手机号码");
            return;
        }
        Connect.captcha.Builder builder = Connect.captcha.newBuilder();
        builder.setPhone(mPhone);
        builder.setType("12");
        Connect.captcha data = builder.build();
        showProgress();
        NetworkManager.INSTANCE.post(Apis.getMobileCode, data.toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                countDownReSend(tvGetCode, 60);
                hideProgress();
            }

            @Override
            public void onError(int code, String errorMessage) {
                hideProgress();
                showToast(errorMessage);
                if (code == 200) {
                    countDownReSend(tvGetCode, 60);
                }
            }
        });
    }



    public void selectPicture() {   //手持身份证
        PermissionHelper.getInstance().getPermission(mContext, new PermissionHelper.PermissionCallBack() {
            @Override
            public void onSuccess() {
                ImageHelper.showImageChoose(ChangeJieSuanKaActivity.this);
            }

            @Override
            public void onFail() {
                showToast("请先允许权限再使用此功能");
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    // 上传图片
    public void upLoadImage(String path) {
        ImageHelper.upLoadImage(path, new ImageHelper.OnUpLoadImageCallBack() {
            @Override
            public void onSuccess(String images) throws InvalidProtocolBufferException {
                hideProgress();
                String[] temp = images.split(",");
                byte[] bytes = new byte[temp.length];
                for (int i = 0; i < temp.length; i++) {
                    bytes[i] = (byte) Integer.parseInt(temp[i]);
                }
                address = MemberOuterClass.img_upload.parseFrom(bytes).getImg();
                GlideHelper.INSTANCE.loadImage(idZhengIv,path);
            }

            @Override

            public void onFail(String error) {
                showToast(error);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PictureConfig.CHOOSE_REQUEST) {
            List<LocalMedia> localMedias = PictureSelector.obtainMultipleResult(data);
            long length = CompressUtils.getFileByPath(localMedias.get(0).getCompressPath()).length();
            if (length <= 204800L) {// 原图片的压缩小于300kb
                upLoadImage(localMedias.get(0).getCompressPath());
            } else {
                Handler handler = new Handler();
                new Thread(() -> {
                    handler.post(ChangeJieSuanKaActivity.this::showProgress);
                    Bitmap bitmap = CompressUtils.getBitmap(localMedias.get(0).getCompressPath());
                    Bitmap compressBitmap = CompressUtils.compressByQuality(bitmap, 174080L);
                    String savePath = CompressUtils.Bitmap2File(mContext, compressBitmap);
                    handler.post(() -> upLoadImage(savePath));
                }).start();
            }
        }

        if (resultCode == RESULT_OK && requestCode == 100) {
            province = data.getStringExtra("province");
            city = data.getStringExtra("city");
            county = data.getStringExtra("county");
            tvBankLocal.setText(province + " " + city + " " + county);
        }

        if (resultCode == RESULT_OK && requestCode == 200) {
            sub_name = data.getStringExtra("name");
            channel = data.getStringExtra("code");
            tvBankSupport.setText(sub_name);
        }
    }


    @OnClick({R.id.back_iv, R.id.tv_bank_local, R.id.tv_bank_support, R.id.tv_get_code, R.id.id_zheng_iv, R.id.bind_btn,R.id.et_bank_name})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.et_bank_name:   //所属银行
                showBank();
                break;
            case R.id.tv_bank_local:  //所属地区
                selectLocal();
                break;
            case R.id.tv_bank_support:  //所属支行
                selectBank();
                break;
            case R.id.tv_get_code:
                getCode();
                break;
            case R.id.id_zheng_iv:
                selectPicture();
                break;
            case R.id.bind_btn:
                submit();
                break;


        }
    }

    private void selectBank() {
        if (bank_name.isEmpty())
        {
            showToast("请先选择银行名称");
            return;
        }
        if (province.isEmpty())
        {
            showToast("请先选择地区");
            return;
        }
        Intent intent = new Intent(mContext, CreateSelectCardActivity.class);
        intent.putExtra("provice",province);
        intent.putExtra("city",city);
        intent.putExtra("county",county);
        intent.putExtra("bankName",bank_name);
        startActivityForResult(intent, 200); // 获取银行
    }

    private void selectLocal() {
        Intent intent = new Intent(mContext, LocationSelectActivity.class);
        intent.putExtra("type", "1");
        startActivityForResult(intent, 100);  // 获取位置
    }




    // 获取支持银行
    public void get_support() {
        NetworkManager.INSTANCE.post(Apis.support_list, 10, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Merchantservice.polling datas = Merchantservice.polling.parseFrom(bytes);
                Log.e("ljj获取支持银行", datas.toString());
                banks.addAll(datas.getListList());
            }

            @Override
            public void onError(int code, String errorMessage) {
            }
        });
    }



    private void showBank() {
        if (ValidateUtils.isValidate(banks)) {
            RecycleViewBankDialog recycleViewBankDialog = new RecycleViewBankDialog(new RecycleViewBankDialog.onItemBankClickListener() {
                @Override
                public void setBankText(String code, String bank) {
                    channel = "";
                    sub_name = "";
                    tvBankSupport.setText("");
                    bank_name = bank;
                    bank_code = code;
                    etbank_name.setText(bank_name);

                }
            });
            RecycleViewBankDialog.Builder builder = new RecycleViewBankDialog.Builder(mContext);
            builder.setData(GsonUtils.serializedToJson(banks)).setTitle("支持银行");
            EasyDialog build = builder.build();
            build.show(getSupportFragmentManager());
        } else {
            showToast("暂无支持银行");
        }
    }


}


