package com.changjiedata.chanpp.module.createcard;

import android.os.Bundle;
import android.view.View;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.module.shop.MyShopActivity;

public class CreateSucceedActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_succeed);
        findViewById(R.id.btn_succeed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MyShopActivity.class);
            }
        });
    }
}
