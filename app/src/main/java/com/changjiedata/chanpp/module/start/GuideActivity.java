package com.changjiedata.chanpp.module.start;

/**
 * 文件名 : GuideActivity
 * 创建者 : MangoWang
 * 创建日期 : 2019/12/7 14:11
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;


import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.adapter.GuideAdapter;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.module.login.LoginActivity;
import com.changjiedata.chanpp.utils.PreferenceSettings;
import com.changjiedata.chanpp.view.FinalNoScrollViewPager;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GuideActivity extends BaseActivity {

    @BindView(R.id.viewpager)
    FinalNoScrollViewPager viewPager;

    private List<Integer> localImageList = new ArrayList<>();
    private Handler handler = new Handler();
    private Timer timer;
    private TextView tv_ti_yuan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置无标题
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //设置全屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_guide);
        ButterKnife.bind(this);
        initView();
        initListener();
    }

    public void initView() {
        timer = new Timer();
        localImageList.add(R.mipmap.welcome01);
        localImageList.add(R.mipmap.welcome02);
        localImageList.add(R.mipmap.welcome03);
        localImageList.add(R.mipmap.welcome04);
        localImageList.add(R.mipmap.welcome05);
        initBanner(localImageList);
        tv_ti_yuan = findViewById(R.id.tv_ti_yuan);
        tv_ti_yuan.setOnClickListener(view -> handler.post(() -> {
            Intent intent = new Intent(GuideActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            //保存状态
            PreferenceSettings.setBoolean("is_have_welcome", false);
        }));
    }

    private void initListener() {
        viewPager.setCanScroll(true);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 3) {
                    viewPager.setCanScroll(true);
                    tv_ti_yuan.setVisibility(View.VISIBLE);
                } else {
                    tv_ti_yuan.setVisibility(View.GONE);
                    viewPager.setCanScroll(true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initBanner(List<Integer> localImageList) {
        viewPager.setAdapter(new GuideAdapter(this, localImageList));
    }

    @Override
    protected void onDestroy() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        super.onDestroy();
    }
}

