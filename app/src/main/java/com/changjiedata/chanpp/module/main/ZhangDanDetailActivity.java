package com.changjiedata.chanpp.module.main;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.helper.GlideHelper;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.proto.Statistics;
import com.google.protobuf.InvalidProtocolBufferException;

import butterknife.BindView;
import butterknife.OnClick;

public class ZhangDanDetailActivity extends BaseActivity {
    @BindView(R.id.bank_ic_iv)
    ImageView bankIcIv;
    @BindView(R.id.bank_tv)
    TextView bankTv;
    @BindView(R.id.tran_money_tv)
    TextView tranMoneyTv;
    @BindView(R.id.tran_tv)
    TextView tranTv;
    @BindView(R.id.status_tv)
    TextView statusTv;
    @BindView(R.id.tran_shou_xu_fei_tv)
    TextView tranShouXuFeiTv;
    @BindView(R.id.jie_suan_shou_xu__tv)
    TextView jieSuanShouXuTV;
    @BindView(R.id.number_tv)
    TextView numberTv;
    @BindView(R.id.tran_time_tv)
    TextView tranTimeTv;
    @BindView(R.id.dao_zhang_money_tv)
    TextView daoZhangMoneyTv;
    @BindView(R.id.dao_zhang_time_tv)
    TextView daoZhangTimeTv;
    @BindView(R.id.slot_card_tv)
    TextView slotCardTv;
    private String id;
    private String time;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zhangdan_detail);
//        ZhanDanBean zhanDanBean = GsonUtils.jsonToBean(getIntent().getStringExtra("data"), ZhanDanBean.class);
        id = getIntent().getStringExtra("id");
        time = getIntent().getStringExtra("time");
//        id=zhanDanBean.getId();
//        time=zhanDanBean.getTime();
        get_trading_detail(id, time);

    }


    @OnClick(R.id.back_iv)
    public void onViewClicked(View view) {
        finish();
    }


    // 获取账单列表

    public void get_trading_detail(String id, String time) {
        showProgress();
        Statistics.trading.Builder builders = Statistics.trading.newBuilder();
        builders.setId(id).setTime(time);
        NetworkManager.INSTANCE.post(Apis.get_trading_detail, builders.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Statistics.trading rep = Statistics.trading.parseFrom(bytes);
                GlideHelper.INSTANCE.loadAvatar(bankIcIv, rep.getLogo());
                bankTv.setText(rep.getTradeFree());
                tranMoneyTv.setText(rep.getAmount());
                tranShouXuFeiTv.setText(rep.getTradeFree());
                jieSuanShouXuTV.setText(rep.getSettleFree());
                numberTv.setText(rep.getOrderId());
                tranTimeTv.setText(rep.getSettleTime());
                daoZhangMoneyTv.setText(rep.getSettleAmount());
                daoZhangTimeTv.setText(rep.getSettleTime());
                slotCardTv.setText(rep.getTicket());


                switch (rep.getStatus()) {
                    case "交易成功":
                        statusTv.setTextColor(Color.GREEN);
                        break;
                    case "交易失败":
                    case "初始化":
                        statusTv.setTextColor(Color.RED);
                        break;
                    case "已到账":
                        statusTv.setTextColor(Color.BLUE);
                        break;
                }
                hideProgress();
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                showToast(errorMessage);
                hideProgress();
            }
        });
    }

}
