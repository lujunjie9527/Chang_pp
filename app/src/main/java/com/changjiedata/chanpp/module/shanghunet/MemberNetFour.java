package com.changjiedata.chanpp.module.shanghunet;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MemberNetFour extends BaseActivity {
    @BindView(R.id.name_et)
    EditText nameEt;
    @BindView(R.id.id_et)
    EditText idEt;
    @BindView(R.id.phone_edt)
    EditText phoneEdt;
    @BindView(R.id.mobile_code_et)
    EditText mobileCodeEt;
    @BindView(R.id.get_code_tv)
    TextView getCodeTv;
    @BindView(R.id.id_zheng_iv)
    ImageView idZhengIv;
    @BindView(R.id.id_bei_iv)
    ImageView idBeiIv;
    @BindView(R.id.hand_zheng_iv)
    ImageView handZhengIv;
    @BindView(R.id.hand_zheng2_iv)
    ImageView handZheng2Iv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_hu_net_4);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.back_iv, R.id.get_code_tv, R.id.id_zheng_iv, R.id.id_bei_iv, R.id.hand_zheng_iv, R.id.hand_zheng2_iv, R.id.next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                break;
            case R.id.get_code_tv:
                break;
            case R.id.id_zheng_iv:
                break;
            case R.id.id_bei_iv:
                break;
            case R.id.hand_zheng_iv:
                break;
            case R.id.hand_zheng2_iv:
                break;
            case R.id.next_btn:
                break;
        }
    }
}
