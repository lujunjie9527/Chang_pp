package com.changjiedata.chanpp.module.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.proto.Connect;
import com.google.protobuf.InvalidProtocolBufferException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPasswordActivity extends BaseActivity {
    @BindView(R.id.back_iv)
    ImageView backIv;
    @BindView(R.id.phone_edt)
    EditText phoneEdt;
    @BindView(R.id.mobile_code_et)
    EditText mobileCodeEt;
    @BindView(R.id.get_code_tv)
    TextView getCodeTv;
    @BindView(R.id.year_month_et)
    EditText passwordEt;
    @BindView(R.id.login_cb_password)
    CheckBox loginCbPassword;
    @BindView(R.id.safe_code_et)
    EditText confirmPasswordEt;
    @BindView(R.id.login_cb_confirm_password)
    CheckBox loginCbConfirmPassword;

    @BindView(R.id.login_btn)
    Button loginBtn;


    private String mPhone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);

        setPasswordCanSee();
        setPasswordCanSee2();
    }

    @OnClick({R.id.back_iv, R.id.get_code_tv,  R.id.login_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.get_code_tv:
                getCode();
                break;

            case R.id.login_btn:
                submit();
                break;

        }
    }

    /**
     * 点击注册按钮
     */
    private void submit() {

        mPhone = phoneEdt.getText().toString();

        if (TextUtils.isEmpty(mPhone)) {
            showToast("请输入手机号码");
            return;
        }

        if (mPhone.length() != 11) {
            mPhone = null;
            showToast("请输入11位手机号码");
            return;
        }
        String code = mobileCodeEt.getText().toString();
        if (TextUtils.isEmpty(code)) {
            showToast("请输入验证码");
            return;
        }
        String pwd = passwordEt.getText().toString();
        if (TextUtils.isEmpty(pwd)) {
            showToast("请输入密码");
            return;
        }
        if (pwd.length() < 8 || pwd.length() > 16) {
            showToast("请输入6-16位长度的密码");
            return;
        }
        String confirmPwd = confirmPasswordEt.getText().toString();
        if (!pwd.equals(confirmPwd)) {
            showToast("两次输入密码不一致，请重新输入");
            return;
        }
        showProgress();
        Connect.sms_register.Builder builder = Connect.sms_register.newBuilder();
        builder.setPhone(mPhone);
        builder.setPassword(pwd);
        builder.setCaptcha(code);
        builder.setRePassword(confirmPwd);
        Connect.sms_register data = builder.build();
        NetworkManager.INSTANCE.post(Apis.forgetPassword, data.toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) {
                hideProgress();
            }

            @Override
            public void onError(int code, String errorMessage) {
                hideProgress();
                if (code == 200) {
                    Intent intent = new Intent();
                    intent.putExtra("phone", mPhone);
                    setResult(RESULT_OK, intent);
                    showToast("修改成功！");
                    finish();
                } else {
                    showToast(errorMessage);
                }
            }
        });
    }

    /**
     * 获取验证码
     */
    private void getCode() {
        mPhone = phoneEdt.getText().toString();
        if (TextUtils.isEmpty(mPhone)) {
            showToast("请输入手机号码");
            return;
        }
        if (mPhone.length() != 11) {
            mPhone = null;
            showToast("请输入11位手机号码");
            return;
        }

        Connect.captcha.Builder builder = Connect.captcha.newBuilder();
        builder.setPhone(mPhone);
        builder.setType("3");
        Connect.captcha data = builder.build();
        showProgress();
        NetworkManager.INSTANCE.post(Apis.getMobileCode, data.toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                countDownReSend(getCodeTv, 60);
                hideProgress();
            }

            @Override
            public void onError(int code, String errorMessage) {
                hideProgress();
                showToast(errorMessage);
                if (code == 200) {
                    countDownReSend(getCodeTv, 60);
                }
            }
        });
    }

    /**
     * 设置密码是否为明文显示
     */

    private void setPasswordCanSee() {
        loginCbPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    //设置为密文显示
                    passwordEt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    //设置为明文显示
                    passwordEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                passwordEt.setSelection(passwordEt.length());
            }
        });
    }

    private void setPasswordCanSee2() {
        loginCbConfirmPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    //设置为密文显示
                    confirmPasswordEt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    //设置为明文显示
                    confirmPasswordEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                confirmPasswordEt.setSelection(confirmPasswordEt.length());
            }
        });
    }
}
