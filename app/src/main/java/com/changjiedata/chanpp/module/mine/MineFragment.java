package com.changjiedata.chanpp.module.mine;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.changjiedata.chanpp.BuildConfig;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseFragment;
import com.changjiedata.chanpp.base.MainApplication;
import com.changjiedata.chanpp.bean.ServiceBean;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.helper.GlideHelper;
import com.changjiedata.chanpp.helper.UserBean;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.WebApis;
import com.changjiedata.chanpp.proto.Index;
import com.changjiedata.chanpp.proto.MemberOuterClass;
import com.changjiedata.chanpp.utils.ValidateUtils;
import com.changjiedata.chanpp.web.WebViewActivity;
import com.example.havi.helper.ChatHelper;
import com.example.havi.interfaces.LoginCallBack;
import com.example.havi.ui.ChatActivity;
import com.google.protobuf.InvalidProtocolBufferException;
import com.ruffian.library.widget.RImageView;

import butterknife.BindView;
import butterknife.OnClick;

public class MineFragment extends BaseFragment {
    @BindView(R.id.user_ic_iv)
    RImageView userIcIv;
    @BindView(R.id.user_name_tv)
    TextView userNameTv;
    @BindView(R.id.user_number_tv)
    TextView userNumberTv;
    @BindView(R.id.ke_fu_tv)
    TextView keFuTv;
    private UserBean userBean;
    private String memberMobile;
    private String name;
    private String id;
    private String phone;
    private String number;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void initView() {
        get_member_info();
        getKeFu();
    }

    @Override
    protected void init() {

    }

    @OnClick({R.id.setting_tv, R.id.message_llt, R.id.ke_fu_llt, R.id.user_massage_rlt, R.id.lltOnLine,  R.id.question_llt, R.id.feedback_llt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.setting_tv:
                startActivity(SettingActivity.class);
                break;
            case R.id.message_llt:  //消息中心
                startActivity(MessageListActivity.class);
                break;
            case R.id.user_massage_rlt:
                Intent intent = new Intent(mContent, MessageActivity.class);
                intent.putExtra("name", name);
                intent.putExtra("id", id);
                intent.putExtra("phone", phone);
                intent.putExtra("number", number);
                startActivity(intent);
                break;
            case R.id.ke_fu_llt:  //客服
                takePhone(memberMobile);
                break;
            case R.id.lltOnLine:  //在线客服
                startChat();
                break;
            case R.id.question_llt:  //常见问题
                WebViewActivity.startActivity(mContent, BuildConfig.SERVERHEAD + WebApis.commonProblem);
                break;
            case R.id.feedback_llt:  //用户反馈
                startActivity(FeedBackActivity.class);
                break;
        }
    }

    /**
     * 获取个人信息
     */
    public void get_member_info() {
        NetworkManager.INSTANCE.post(Apis.get_member_info, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] response) throws InvalidProtocolBufferException {
                MemberOuterClass.Member member = MemberOuterClass.Member.parseFrom(response);
                Log.e("ljj个人信息打印", member.toString());
                name = member.getMemberName();
                id = member.getPushId();
                phone = member.getMemberMobile();
                number = member.getMemberId();
                UserBean userBean = new UserBean(member);
                userNameTv.setText(member.getMemberName());
                userNumberTv.setText(member.getMemberId());
                GlideHelper.INSTANCE.loadAvatar(userIcIv, member.getMemberAvatar());
                UserHelper.syncCurrentUserInfo(userBean);   //同步个人信息
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                showToast(errorMessage);
            }
        });
    }

    /**
     * 在线客服
     */
    private void startChat() {
        boolean login = ChatHelper.getInstance().isLogin();
        if (login) {
            // 打开客服聊天
            ServiceBean keFuInfo = UserHelper.getKeFuInfo();
            if (ValidateUtils.isValidate(keFuInfo)) {
                ChatActivity.startActivity(mContent, keFuInfo.getMember_id() + "", keFuInfo.getMember_name(), keFuInfo.getMember_avatar(), keFuInfo.getChat_id(), keFuInfo.getMember_mobile());
            } else {
                showToast("暂无客服信息");
            }
        } else {
            showToast("聊天登录中");
            MainApplication.getChatHelper().login(UserHelper.getUser().getChat_id(), UserHelper.getUser().getChat_pwd(), new LoginCallBack() {
                @Override
                public void onSuccess() {
                    //判断是否为客服
                    // 打开客服聊天
                    ServiceBean keFuInfo = UserHelper.getKeFuInfo();
                    if (ValidateUtils.isValidate(keFuInfo)) {
                        mContent.runOnUiThread(() -> showToast("人工客服回复时间：工作日9:00-12:00；13:30-18:00"));
                        ChatActivity.startActivity(mContent, keFuInfo.getMember_id() + "", keFuInfo.getMember_name(), keFuInfo.getMember_avatar(), keFuInfo.getChat_id(), keFuInfo.getMember_mobile());
                    } else {
                        mContent.runOnUiThread(() -> showToast("暂无客服信息"));
                    }
                }

                @Override
                public void onError(int code, String message) {
                    mContent.runOnUiThread(() -> showToast("聊天暂时登录失败"));
                }
            });
        }
    }

    /**
     * 获取客服
     */
    public void getKeFu() {
        NetworkManager.INSTANCE.post(Apis.get_kfu_info, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] response) throws InvalidProtocolBufferException {
                Index.get_kfu_info kefu_message = Index.get_kfu_info.parseFrom(response);
                memberMobile = kefu_message.getMemberMobile();
                keFuTv.setText(kefu_message.getMemberMobile());
                ServiceBean serviceBean = new ServiceBean();
                serviceBean.setMember_id(kefu_message.getMemberId());
                serviceBean.setMember_name(kefu_message.getMemberName());
                serviceBean.setMember_mobile(kefu_message.getMemberMobile());
                serviceBean.setMember_avatar(kefu_message.getMemberAvatar());
                serviceBean.setChat_id(kefu_message.getChatId());
                serviceBean.setChat_pwd(kefu_message.getChatPwd());
                UserHelper.saveKeFuUserInfo(serviceBean);
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                showToast(errorMessage);
            }
        });
    }

    /**
     * 拨打电话（跳转到拨号界面，用户手动点击拨打）
     */
    public void takePhone(String memberMobile) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        Uri data = Uri.parse("tel:" + memberMobile);//"tel:" + mViewModel.phone.get()
        intent.setData(data);
        startActivity(intent);
    }
}
