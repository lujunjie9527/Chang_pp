package com.changjiedata.chanpp.module.shop;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.adapter.SelectMccAdapter;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.changjiedata.chanpp.utils.StatusBarUtil;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectMccActivity extends BaseActivity {
    @BindView(R.id.back_iv)
    ImageView backTv;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.et_search)
    EditText etSearch;
    private SelectMccAdapter mAdapter;
    private List<Merchantservice.mcc> datas = new ArrayList<>();
    private String name = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_bank);
        ButterKnife.bind(this);
        StatusBarUtil.setStatusBarLightMode(this, Color.parseColor("#ffffff"), true);
        initRecyclerView();
        initListener();
        tvTitle.setText("MCC搜索");
        etSearch.setHint("输入关键词进行搜索");
        initData();
    }

    private void initListener() {
        backTv.setOnClickListener(view -> finish());
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    name = " ";
                } else {
                    name = s.toString();
                }
                initData();
            }
        });

    }

    private void initData() {
        Merchantservice.get_mcc.Builder builder = Merchantservice.get_mcc.newBuilder();
        builder.setName(name);
        NetworkManager.INSTANCE.post(Apis.get_mcc, builder.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                List<Merchantservice.mcc> lists = Merchantservice.get_mcc.parseFrom(bytes).getListList();
                mAdapter.replaceData(lists);
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                showToast(errorMessage);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(mAdapter = new SelectMccAdapter(mContext, datas));
        mAdapter.bindToRecyclerView(recyclerView);
        mAdapter.setEmptyView(R.layout.recycle_empty);
        // 每个item的点击事件
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            Intent intent = new Intent();
            intent.putExtra("mcc",datas.get(position).getMcc());
            intent.putExtra("mcc_name",datas.get(position).getMccName());
            intent.putExtra("sub_mcc",datas.get(position).getSubMcc());
            intent.putExtra("sub_name",datas.get(position).getSubName());
            setResult(RESULT_OK,intent);
            finish();
        });
    }

}
