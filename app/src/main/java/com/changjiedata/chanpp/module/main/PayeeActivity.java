package com.changjiedata.chanpp.module.main;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.helper.GlideHelper;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.proto.Bank;
import com.google.protobuf.InvalidProtocolBufferException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PayeeActivity extends BaseActivity {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.bank_tv)
    TextView bankTv;
    @BindView(R.id.bankIcon_iv)
    ImageView bankIconIv;
    @BindView(R.id.monkey_et)
    EditText monkeyEt;
    @BindView(R.id.pay_tip_tv)
    TextView PayTipTv;
    @BindView(R.id.submit_btn)
    Button submitBtn;
    private int max = 4999;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payee);
        ButterKnife.bind(this);
        int type = getIntent().getIntExtra("type", 0);
        if (type == 1) // ViP
        {
            tvTitle.setText("VIP收款");
           max = 9999;
            PayTipTv.setText("单笔交易金额10-"+ max + "元");
        }
        get_bank("1", "2");
        PayTipTv.setText("单笔交易金额10-"+max+"元");
    }

    @OnClick({R.id.back_iv,  R.id.submit_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.submit_btn:
                submit();
                break;
        }
    }

    private void createShuaKa() {
      /*  MPRequestParams  params = new MPRequestParams();
        params. setShowBindTerminalPrompt (true);// 是否首次绑定提示（默认提示）
        params.setAmount(mViewModel.money.get());
        params.setAgentNo(mViewModel.order.getAgentNo());
        params.setUserId(mViewModel.order.getUserId());
        params.setAgentOrderId(mViewModel.order.getOrderId());
        params.setSplitRule(mViewModel.order.getSplitRule());
        params.setDescription(mViewModel.order.getRequest());
        params.setMerchantId(mViewModel.order.getMerchantId());
        MPApi.INSTANCE.startConsume(mActivity, params, new MPIConsumeCallback() {

            @Override
            public void onSuccess(MPPosResultModel model, Object... expandDatas) {
                Toast.makeText(mActivity, "收款成功：", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(MPApiError error, Object... expandDatas) {
                Toast.makeText(mActivity, "收款失败：" + error.getErrorInfo(), Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    // 获取结算卡（只有一张)）
    public void get_bank(String type, String form) {
        showProgress();
        Bank.get_bank.Builder builders = Bank.get_bank.newBuilder();
        builders.setType(type).setForm(form);
        NetworkManager.INSTANCE.post(Apis.get_bank, builders.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                hideProgress();
                Bank.get_bank get_bank = Bank.get_bank.parseFrom(bytes);
                bankTv.setText(get_bank.getListList().get(0).getBankName()+  "(" + get_bank.getListList().get(0).getCard().substring(get_bank.getListList().get(0).getCard().length() - 4) + ")");
                GlideHelper.INSTANCE.loadAvatar(bankIconIv, get_bank.getListList().get(0).getLogo());
//                bank.setValue(get_bank.getListList().get(0));
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                showToast(errorMessage);
                hideProgress();
            }
        });
    }

    public void submit() {
        String monye = monkeyEt.getText().toString().trim();
        if (TextUtils.isEmpty(monye)) {
            showToast("请输入金额");
        } else {
            Float aFloat = Float.valueOf(monye);
            if (aFloat < 10) {
                showToast("刷卡金额最低10元");
                return;
            }
            if (aFloat > max) {
                showToast("刷卡金额最高" + max + "元");
                return;
            }
        }
        createShuaKa();
    }


}
