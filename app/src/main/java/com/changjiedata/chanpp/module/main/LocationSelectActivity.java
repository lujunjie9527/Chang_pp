package com.changjiedata.chanpp.module.main;


import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Area;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.changjiedata.chanpp.utils.StatusBarUtil;
import com.changjiedata.chanpp.utils.ValidateUtils;
import com.changjiedata.chanpp.widget.easyadapter.BaseViewHolder;
import com.changjiedata.chanpp.widget.easyadapter.RecyclerArrayAdapter;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LocationSelectActivity extends BaseActivity {
    @BindView(R.id.title_tv)
    TextView titleTv;
    @BindView(R.id.location_rv)
    RecyclerView locationRv;

    private RecyclerArrayAdapter<Merchantservice.basics> mAdapter;
    private String areaId = "0";  //"0"
    private int page = 0;
    private List<Merchantservice.basics> dataList = new ArrayList<>();  // 请求中数据
    private String province;
    private String city;
    private String county;
    private String provinceCode;
    private String cityCode;
    private String countyCode;
    private String type;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_select);
        StatusBarUtil.setStatusBarLightMode(this, Color.parseColor("#ffffff"), true);
        ButterKnife.bind(this);
        type = getIntent().getStringExtra("type");
        titleTv.setText("地区");
        initRecycle();
        loadData();
    }

    private void loadData() {
        // 加载底部数据
        showProgress();
        Area.area_list.Builder builder = Area.area_list.newBuilder();
        builder.setAreaId(areaId);
        NetworkManager.INSTANCE.post(Apis.area_list, builder.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] response) throws InvalidProtocolBufferException {
                hideProgress();
                if (titleTv == null) return; // 防止请求延迟出现闪退
                page++;
                List<Merchantservice.basics> list = Merchantservice.polling.parseFrom(response).getListList();
                if (!ValidateUtils.isValidate(list)) {
                    showToast("没有更多了");
                    page--;
                    return;
                }
                dataList.clear();
                mAdapter.clear();
                dataList.addAll(list);
                mAdapter.addAll(list);
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                hideProgress();
                Toast.makeText(LocationSelectActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            }

        });

    }

    /**
     * y
     * 实例化底部RecyclerView
     * *
     */
    private void initRecycle() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        locationRv.setLayoutManager(layoutManager);
        locationRv.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        locationRv.setAdapter(mAdapter = new RecyclerArrayAdapter<Merchantservice.basics>(mContext) {
            @Override
            public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_location, parent, false));
            }

            class ViewHolder extends BaseViewHolder<Merchantservice.basics> {
                TextView location;

                public ViewHolder(View itemView) {
                    super(itemView);
                    location = itemView.findViewById(R.id.tv_location);
                }

                @Override
                public void setData(Merchantservice.basics data) {
                    location.setText(data.getName());
                }
            }
        });
        mAdapter.setOnItemClickListener(position -> {
            Merchantservice.basics locationBean = dataList.get(position);
            areaId = locationBean.getCode();  //地区id
            if (page == 1) {
                province = locationBean.getName();
                provinceCode = locationBean.getCode();
            } else if (page == 2) {
                city = locationBean.getName();
                cityCode = locationBean.getCode();
            } else if (page == 3) {
                county = locationBean.getName();
                countyCode = locationBean.getCode();
            }
            if (type.equals("0")) {
                if (page == 2) {
                    Intent intent = new Intent();
                    intent.putExtra("province", province);
                    intent.putExtra("city", city);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    loadData();
                }
            } else if (type.equals("1")) {
                if (page == 3) {
                    Intent intent = new Intent();
                    intent.putExtra("provinceCode", provinceCode);
                    intent.putExtra("cityCode", cityCode);
                    intent.putExtra("countyCode", countyCode);
                    intent.putExtra("province", province);
                    intent.putExtra("city", city);
                    intent.putExtra("county", county);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    loadData();
                }
            }

        });
    }

    @OnClick(R.id.back_iv)
    public void onViewClicked() {
        finish();
    }
}
