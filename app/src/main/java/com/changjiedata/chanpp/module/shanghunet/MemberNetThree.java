package com.changjiedata.chanpp.module.shanghunet;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MemberNetThree extends BaseActivity {
    @BindView(R.id.mcc_et)
    EditText mccEt;
    @BindView(R.id.mcc_number_et)
    EditText mccNumberEt;
    @BindView(R.id.address_et)
    EditText addressEt;
    @BindView(R.id.next_iv)
    ImageView nextIv;
    @BindView(R.id.detail_address_et)
    EditText detailAddressEt;
    @BindView(R.id.next_btn)
    Button nextBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_hu_net_3);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.back_iv, R.id.next_iv, R.id.next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                break;
            case R.id.next_iv:
                break;
            case R.id.next_btn:
                break;
        }
    }
}
