package com.changjiedata.chanpp.module.main;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CardActivity extends BaseActivity {


    @BindView(R.id.card_icon_iv)
    ImageView cardIconIv;
    @BindView(R.id.card_name_tv)
    TextView cardNameTv;
    @BindView(R.id.card_type_tv)
    TextView cardTypeTv;
    @BindView(R.id.card_number_tv)
    TextView cardNumberTv;
    @BindView(R.id.validity_tv)
    TextView validityTv;
    @BindView(R.id.next_btn)
    Button nextBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_card);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.back_iv, R.id.tvSupBank, R.id.next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.tvSupBank:

              /*  if (ValidateUtils.isValidate(mViewModel.banks)) {
                    RecycleViewDialog.Builder builder = new RecycleViewDialog.Builder(mActivity);
                    builder.setData(GsonUtils.serializedToJson(mViewModel.banks)).setTitle("VIP收款支持银行");
                    EasyDialog build = builder.build();
                    build.show(getChildFragmentManager());
                } else {
                    showShortToast("暂无支持银行");
                }*/
                break;
            case R.id.next_btn:
                break;
        }
    }
}
