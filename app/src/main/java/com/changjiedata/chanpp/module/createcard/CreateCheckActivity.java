package com.changjiedata.chanpp.module.createcard;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.changjiedata.chanpp.utils.GsonUtils;
import com.changjiedata.chanpp.utils.StatusBarUtil;
import com.changjiedata.chanpp.utils.date.DateUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateCheckActivity extends AppCompatActivity {

    @BindView(R.id.back_iv)
    ImageView backTv;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_shop_name)
    TextView tvShopName;
    @BindView(R.id.tv_local)
    TextView tvLocal;
    @BindView(R.id.tv_shop_type)
    TextView tvShopType;
    @BindView(R.id.tv_shop_no)
    TextView tvShopNo;
    @BindView(R.id.tv_up_time)
    TextView tvUpTime;
    @BindView(R.id.tv_succeed_time)
    TextView tvSucceedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_check);
        StatusBarUtil.setStatusBarLightMode(this, Color.parseColor("#ffffff"), true);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        backTv.setOnClickListener(v -> finish());
        Merchantservice.shop data = GsonUtils.deSerializedFromJson(getIntent().getStringExtra("data"), Merchantservice.shop.class);
        tvTitle.setText(data.getMccName());
        tvShopName.setText(data.getShopName());
        tvLocal.setText(data.getProvinceName() + " " + data.getCityName() + " " + data.getCountyName());
        tvShopType.setText(data.getMccName() + " " + data.getSubName());
        tvUpTime.setText(DateUtil.getStrTime(data.getCreatedTime()));
        tvSucceedTime.setText(DateUtil.getStrTime(data.getUpdateTime()));
        tvShopNo.setText(data.getSnCode());

    }
}
