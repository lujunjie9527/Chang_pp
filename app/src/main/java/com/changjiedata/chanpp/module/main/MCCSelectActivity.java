package com.changjiedata.chanpp.module.main;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;

public class MCCSelectActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcc_sel);
    }
}
