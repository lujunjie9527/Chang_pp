package com.changjiedata.chanpp.module.count;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.adapter.CountAdapter;
import com.changjiedata.chanpp.base.BaseFragment;
import com.changjiedata.chanpp.bean.ProgressBean;
import com.changjiedata.chanpp.dialog.SelectMonthDialog;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.proto.Statistics;
import com.changjiedata.chanpp.utils.date.DateUtil;
import com.changjiedata.chanpp.view.MyLineChartView;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import kale.ui.view.dialog.EasyDialog;

public class CountFragment extends BaseFragment {
    @BindView(R.id.select_time_tv)
    TextView selectTimeTv;
    @BindView(R.id.monkey_et)
    TextView monkeyEt;
    @BindView(R.id.kou_kuan_tv)
    TextView kouKuanTv;
    @BindView(R.id.lineChartView)
    MyLineChartView lineChartView;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private String searchTime;
    private CountAdapter mAdapter;
    private List<ProgressBean> progressBeanList = new ArrayList<>();
    private List<Statistics.statistics> statistics;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_count;
    }

    @Override
    protected void initView() {
        selectTimeTv.setText(DateUtil.getYear() + "年" + String.format("%02d",DateUtil.getMonth() )+ "月");
        get_statistics(DateUtil.getYear() + "-" + DateUtil.getMonth());  //当前时间
        get_trend(DateUtil.getYear() + "-" + String.format("%02d", DateUtil.getMonth()));
        initRecyclerView();
    }

    @Override
    protected void init() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    //获取年月时间
    @OnClick(R.id.select_time_tv)
    public void onViewClicked() {
        SelectMonthDialog.Builder builder = new SelectMonthDialog.Builder(getContext());
        builder.setOnDialogClickListener(new SelectMonthDialog.OnDialogClickListener() {
            @Override
            public void onClick(View v, String month) {
                selectTimeTv.setText(month.substring(0, 4) + "年" + month.substring(4, 6) + "月");
                searchTime = month.substring(0, 4) + "-" + month.substring(4, 6);
                get_statistics(searchTime);
                get_trend(searchTime);
            }
        });
        EasyDialog build = builder.build();
        build.setCancelable(true);
        build.show(getFragmentManager());
    }

    // 统计刷卡数据与底部百分比
    public void get_statistics(String time) {
        Statistics.get_statistics.Builder builders = Statistics.get_statistics.newBuilder();
        builders.setMonth(time);
        NetworkManager.INSTANCE.post(Apis.get_statistics, builders.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Statistics.get_statistics data = Statistics.get_statistics.parseFrom(bytes);
                Log.e("ljj刷卡数据与底部百分比", data.toString());
                monkeyEt.setText(data.getTotalAmount());
                kouKuanTv.setText("总扣除手续费￥" + data.getFee() + " 实际到账￥" + data.getSettleAmount());
                progressBeanList.clear();
                List<Statistics.statistics> listList = data.getListList();
                double total = Double.valueOf(data.getTotalAmount());
                // 逻辑处理本应写在adapter，但是total在数据最外层 ，无奈之下这样处理
                for (Statistics.statistics bean : listList) {
                    ProgressBean progressBean = new ProgressBean();
                    progressBean.setName(bean.getTime());
                    double amount = Double.valueOf(bean.getAmount());
                    double percent = amount / total * 100;
                    if (0 < percent && percent < 1) {
                        percent = 1;
                    }
                    progressBean.setProgess((int) percent);
                    progressBean.setPercent(String.valueOf((int) percent));
                    progressBeanList.add(progressBean);
                }
                mAdapter.notifyDataSetChanged();
                mAdapter.loadMoreEnd(true);
            }

            @Override
            public void onError(int code, String errorMessage) {

            }
        });
    }


    // 统计线性图表
    public void get_trend(String time) {
        Statistics.get_statistics.Builder builders = Statistics.get_statistics.newBuilder();
        builders.setMonth(time);
        NetworkManager.INSTANCE.post(Apis.get_trend, builders.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Statistics.get_statistics datas = Statistics.get_statistics.parseFrom(bytes);
                Log.e("线性数据返回", datas.toString());
                lineChartView.setLineData(datas.getListList());
            }

            @Override
            public void onError(int code, String errorMessage) {
            }
        });
    }

    private void initRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(mContent, 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter = new CountAdapter(progressBeanList));
        mAdapter.bindToRecyclerView(recyclerView);
//        mAdapter.setEmptyView(R.layout.recycle_empty);

        //传true 可以滑动 false不可以滑动
        recyclerView.setNestedScrollingEnabled(false);

    }

}
