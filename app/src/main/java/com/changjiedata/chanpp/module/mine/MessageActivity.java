package com.changjiedata.chanpp.module.mine;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.module.login.LoginActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.utils.DialogUntil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MessageActivity extends BaseActivity {
    @BindView(R.id.user_number_tv)
    TextView userNumberTv;
    @BindView(R.id.user_id_number_tv)
    TextView userIdNumberTv;
    @BindView(R.id.user_phone_tv)
    TextView userPhoneTv;
    @BindView(R.id.bian_hao_tv)
    TextView bianHaoTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_per_message);
        ButterKnife.bind(this);
        userNumberTv.setText(getIntent().getStringExtra("name"));
        userIdNumberTv.setText(UserHelper.getUser().getIdcard());
        userPhoneTv.setText(getIntent().getStringExtra("phone"));
        bianHaoTv.setText(getIntent().getStringExtra("number"));
    }

    @OnClick({R.id.back_iv, R.id.next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.next_btn:
                DialogUntil.showDefaultDialog(this, "", "确定退出登录？", new DialogUntil.DialogImpl() {
                    @Override
                    public void onOk() {
                        NetworkManager.INSTANCE.post(Apis.logout, new OnRequestCallBack() {
                            @Override
                            public void onOk(byte[] bytes) {
                            }

                            @Override
                            public void onError(int code, String errorMessage) {
                            }
                        });
                        Intent intent2 = new Intent(mContext, LoginActivity.class);
                        intent2.putExtra("phone", UserHelper.getUser().getMember_mobile());
                        UserHelper.loginOut();
                        startActivity(intent2);
                        finish();
                    }
                });
                break;
        }
    }
}
