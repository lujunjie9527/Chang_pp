package com.changjiedata.chanpp.module.main;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.helper.GlideHelper;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Bank;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.changjiedata.chanpp.utils.HideDataUtil;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class JieSuanKaActivity extends BaseActivity {

    @BindView(R.id.ivBankIc)
    ImageView ivBankIc;
    @BindView(R.id.ivCheck)
    ImageView ivCheck;
    @BindView(R.id.tvBankName)
    TextView tvBankName;

    @BindView(R.id.tvCard)
    TextView tvCard;
    private String codeType = "";
    private String bankCard;
    private String bankName;
    private List<Bank.get_bank> banks = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jie_suan_ka);
        ButterKnife.bind(this);
        get_bank("1", "2");
        check_change();

    }

    @Override
    protected void onResume() {
        super.onResume();
        check_change();
    }

    @OnClick({R.id.back_iv, R.id.bind_btn, R.id.rtvRecord})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.rtvRecord:
                startActivity(ApplyRecordActivity.class);
                break;
            case R.id.bind_btn:
                if (codeType.equals("1")) {
                    showToast("您有结算卡待审核中暂无法发起申请");
                } else if (codeType.equals("0")) {
                    startActivity(ChangeJieSuanKaActivity.class);
                }
                break;
        }
    }

    //查看是否已提交结算卡修改
    public void check_change() {
//        showProgress();
        NetworkManager.INSTANCE.post(Apis.check_change, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
//                hideProgress();
                Merchantservice.basics datas = Merchantservice.basics.parseFrom(bytes);
                Log.e("ljj查看是否已提交结算卡修改", datas.toString());
                codeType = datas.getCode();
//                if (codeType.equals("1")) {
////                    ivCheck.setVisibility(View.VISIBLE);
//                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                showToast(errorMessage);
            }
        });
    }

    // 获取结算卡（只有一张)）
    public void get_bank(String type, String form) {
//        showProgress();
        Bank.get_bank.Builder builders = Bank.get_bank.newBuilder();
        builders.setType(type).setForm(form);
        NetworkManager.INSTANCE.post(Apis.get_bank, builders.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Bank.get_bank get_bank = Bank.get_bank.parseFrom(bytes);
                Log.e("ljj获取结算卡", get_bank.getListList().toString());
                GlideHelper.INSTANCE.loadAvatar(ivBankIc, get_bank.getListList().get(0).getLogo());
                bankName=get_bank.getListList().get(0).getBankName();
                bankCard = get_bank.getListList().get(0).getCard().substring(get_bank.getListList().get(0).getCard().length()-4);
                tvBankName.setText(bankName);
                tvCard.setText(HideDataUtil.hideCardNo(get_bank.getListList().get(0).getCard()));
//                hideProgress();
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                showToast(errorMessage);
                hideProgress();
            }
        });
    }
}
