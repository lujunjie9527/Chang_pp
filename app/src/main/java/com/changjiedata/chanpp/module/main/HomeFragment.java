package com.changjiedata.chanpp.module.main;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseFragment;
import com.changjiedata.chanpp.dialog.VipHasDialog;
import com.changjiedata.chanpp.dialog.VipNotDialog;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.helper.GlideHelper;
import com.changjiedata.chanpp.module.mine.MessageListActivity;
import com.changjiedata.chanpp.module.shop.MyShopActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.proto.Bank;
import com.changjiedata.chanpp.proto.Index;
import com.changjiedata.chanpp.proto.Information;
import com.changjiedata.chanpp.utils.GsonUtils;
import com.changjiedata.chanpp.utils.ValidateUtils;
import com.changjiedata.chanpp.view.BannerImageHoderView;
import com.changjiedata.chanpp.view.RecycleViewDialog;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import kale.ui.view.dialog.EasyDialog;

public class HomeFragment extends BaseFragment {
    @BindView(R.id.appName_tv)
    TextView appNameTv;
    @BindView(R.id.home_convenient_banner)
    ConvenientBanner convenientBanner;
    @BindView(R.id.ll_about)
    LinearLayout llAbout;
    /*    @BindView(R.id.home_refreshLayout)
        TwinklingRefreshLayout mRefreshLayout;*/
    private List<Index.get_show_list.List> advDatas = new ArrayList<>(); // banner数据
    private List<Bank.bank> banks = new ArrayList<>();


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initView() {
        loadData();
        information_index();
    }

    @Override
    protected void init() {

    }

    public void information_index() {   //使用帮助
        NetworkManager.INSTANCE.post(Apis.information_index, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Information.InformationList data = Information.InformationList.parseFrom(bytes);
                for (Information.DetailInfo bean : data.getIndexListList()) {
                    View tempView = View.inflate(mContent, R.layout.item_home_about, null);
                    TextView tvTitle = tempView.findViewById(R.id.tv_title);
                    TextView tvContent = tempView.findViewById(R.id.tv_content);
                    ImageView llBg = tempView.findViewById(R.id.iv_bg);
                 /*   llBg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view1) {
                            WebViewActivity.startActivity(mContent, BuildConfig.SERVERHEAD + WebApis.webImage + bean.getArticlePic() + "&title=" + bean.getBigTitle());
                        }
                    });*/
                    tvTitle.setText(bean.getBigTitle());
                    tvContent.setText(bean.getSmallTitle());
                    GlideHelper.INSTANCE.loadFitXYImage(llBg, bean.getSmallPic());
                    llAbout.addView(tempView);
                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
            }
        });
    }

    @OnClick({R.id.message, R.id.pay_one_llt, R.id.pay_two_llt, R.id.home_category_one, R.id.home_category_two,
            R.id.home_category_three, R.id.home_category_four, R.id.home_category_five, R.id.home_convenient_banner})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.message:
                startActivity(MessageListActivity.class);
                break;
            case R.id.pay_one_llt:
                showToast("功能开发中");
                break;
            case R.id.pay_two_llt:
                showToast("功能开发中");
                break;
            case R.id.home_category_one:
                startActivity(ZhangDanActivity.class);
                break;
            case R.id.home_category_two:
                startActivity(JieSuanKaActivity.class);
                break;
            case R.id.home_category_three:
                startActivity(CardBaoActivity.class);
                break;
            case R.id.home_category_four:
                startActivity(MachineActivity.class);
                break;

            case R.id.home_category_five:  //我的店铺
                startActivity(MyShopActivity.class);
                break;
            case R.id.home_convenient_banner:
                break;
        }
    }


    /**
     * 加载数据项
     */
    private void loadData() {
        // 加载首页数据
        NetworkManager.INSTANCE.post(Apis.get_banner, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] response) throws InvalidProtocolBufferException {
                Index.get_show_list data = Index.get_show_list.parseFrom(response);
                advDatas = data.getAdvListList();
                initBanner(advDatas);
                // 接口返回比较慢,以此来结束刷新请求
//                mRefreshLayout.finishRefreshing();
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                showToast(errorMessage);
            }
        });


        // 加载banner的数据
        NetworkManager.INSTANCE.post(Apis.get_banner, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
//                if (mRefreshLayout == null) return;
                Index.get_show_list data = Index.get_show_list.parseFrom(bytes);
                advDatas = data.getAdvListList();
                initBanner(advDatas);
                // 接口返回比较慢,以此来结束刷新请求
//                mRefreshLayout.finishRefreshing();
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
//                if (mRefreshLayout == null) return;
//                mRefreshLayout.finishRefreshing();
                showToast(errorMessage);
            }
        });


    }


    // 获取自己的信用卡
    public void get_bank_list(String type, String form) {  //2  1
        Bank.get_bank.Builder builders = Bank.get_bank.newBuilder();
        builders.setType(type).setForm(form);
        NetworkManager.INSTANCE.post(Apis.get_bank, builders.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Bank.get_bank get_bank = Bank.get_bank.parseFrom(bytes);
                if (ValidateUtils.isValidate(get_bank)) {
                    banks.clear();
                    showVipHasDialog(get_bank.getListList());
                } else {
                    showVipNotDialog();
                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
            }
        });
    }

    // 弹出vip信用卡框
    private void showVipHasDialog(List<Bank.bank> banks) {
        VipHasDialog.Builder builder = new VipHasDialog.Builder(mContent);
        builder.setData(GsonUtils.serializedToJson(banks));
        builder.setOnDialogClickListenerOne(new VipHasDialog.OnDialogClickListener() {
            @Override
            public void onClick(View view) {
                get_support("2", "1");
//                showBank();
            }
        });
        builder.setOnDialogClickListenerTow(v -> {
            Intent intent = new Intent(mContent, PayeeActivity.class);
            intent.putExtra("type", 1);
            startActivity(intent);
        });
        builder.setOnDialogClickListenerThree(new VipHasDialog.OnDialogClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(mContent, BindXinKaActivity.class);
                startActivity(intent2);
            }
        });
        EasyDialog build = builder.build();
        build.setCancelable(false);
        build.show(getChildFragmentManager());
    }


    // 弹出没有vip信用卡框
    private void showVipNotDialog() {
        VipNotDialog.Builder builder = new VipNotDialog.Builder(mContent);
        builder.setOnDialogClickListenerOne(new VipNotDialog.OnDialogClickListener() {
            @Override
            public void onClick(View view) {
                get_support("2", "1");
//               showBank();
            }
        });
        builder.setOnDialogClickListenerTow(new VipNotDialog.OnDialogClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContent, BindXinKaActivity.class);
                startActivity(intent);
            }
        });
        EasyDialog build = builder.build();
        build.setCancelable(false);
        build.show(getChildFragmentManager());
    }

    // 展示支持的信用卡列表dialog
    private void showBank() {
        if (ValidateUtils.isValidate(banks)) {
            RecycleViewDialog.Builder builder = new RecycleViewDialog.Builder(mContent);
            builder.setData(GsonUtils.serializedToJson(banks)).setTitle("VIP收款支持银行");
            EasyDialog build = builder.build();
            build.show(getChildFragmentManager());
        } else {
            showToast("暂无支持银行");
        }
    }

    // 获取支持银行列表
    public void get_support(String type, String form) {
        Bank.get_bank.Builder builder = Bank.get_bank.newBuilder();
        builder.setType(type).setForm(form);
        NetworkManager.INSTANCE.post(Apis.get_support, builder.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Bank.get_bank data = Bank.get_bank.parseFrom(bytes);
                Log.e("ljj支持银行", data.toString());
                banks.addAll(data.getListList());
                showBank();
            }

            @Override
            public void onError(int code, String errorMessage) {
            }
        });
    }


    private void initBanner(List<Index.get_show_list.List> data) {
        convenientBanner.setPages(() -> new BannerImageHoderView(), data)
                .setPageIndicator(new int[]{R.mipmap.indicator_un_selected, R.mipmap.indicator_selected})
                //设置指示器的方向
                .setPageIndicatorAlign(ConvenientBanner.PageIndicatorAlign.CENTER_HORIZONTAL);
        convenientBanner.startTurning(6000);
    }
}
