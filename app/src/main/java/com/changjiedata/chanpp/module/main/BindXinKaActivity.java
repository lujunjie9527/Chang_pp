package com.changjiedata.chanpp.module.main;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.dialog.SelectMonthDialog;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.proto.Bank;
import com.changjiedata.chanpp.proto.Connect;
import com.changjiedata.chanpp.utils.GsonUtils;
import com.changjiedata.chanpp.utils.ValidateUtils;
import com.changjiedata.chanpp.view.RecycleViewDialog;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kale.ui.view.dialog.EasyDialog;

public class BindXinKaActivity extends BaseActivity {
    @BindView(R.id.bankUser_et)
    EditText bankUserEt;
    @BindView(R.id.bank_number_et)
    EditText bankNumberEt;
    @BindView(R.id.year_month_tv)
    TextView YearMonthTv;
    @BindView(R.id.safe_code_et)
    EditText SafeCodeEt;
    @BindView(R.id.bank_phone_et)
    EditText bankPhoneEt;
    @BindView(R.id.bankMobile_code_et)
    EditText bankMobileCodeEt;
    @BindView(R.id.get_code_tv)
    TextView getCodeTv;
    private String mPhone;
    private String mYearMonth;

    private List<Bank.bank> banks=new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_bank);
        ButterKnife.bind(this);
        get_support("2", "1");
        bankUserEt.setText(UserHelper.getUser().getTruename());
    }

    @OnClick({R.id.back_iv, R.id.get_code_tv, R.id.bind_btn, R.id.tvSupBank, R.id.year_month_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.get_code_tv:
                getCode();
                break;
            case R.id.bind_btn:
                submit();
                break;
            case R.id.tvSupBank:
                showBank();
                break;
            case R.id.year_month_tv:
                selectTime();
                break;
        }
    }


    public void submit() {
        String name = bankUserEt.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            showToast("请输入姓名");
            return;
        }
        String card = bankNumberEt.getText().toString().trim();
        if (TextUtils.isEmpty(card)) {
            showToast("请填写您本人的信用卡卡号");
            return;
        }

        String valid_time = YearMonthTv.getText().toString().trim();
        if (TextUtils.isEmpty(valid_time)) {
            showToast("请选择该卡有效期年月");
            return;
        }

        String cvv = SafeCodeEt.getText().toString().trim();
        if (TextUtils.isEmpty(cvv)) {
            showToast("请输入该卡安全码");
            return;
        }
        String mobile = bankPhoneEt.getText().toString().trim();
        if (TextUtils.isEmpty(mobile)) {
            showToast("请输入手机号码");
            return;
        }
        if (mobile.length() != 11) {
            showToast("请输入11位手机号码");
            return;
        }
        String captcha = bankMobileCodeEt.getText().toString().trim();
        if (TextUtils.isEmpty(captcha)) {
            showToast("请输入手机验证码");
            return;
        }
        showProgress();
    }

    public void showBank() {
        if (ValidateUtils.isValidate(banks)) {
            RecycleViewDialog.Builder builder = new RecycleViewDialog.Builder(mContext);
            builder.setData(GsonUtils.serializedToJson(banks)).setTitle("VIP收款支持银行");
            EasyDialog build = builder.build();
            build.show(getSupportFragmentManager());
        } else {
            showToast("暂无支持银行");
        }
    }
    public void selectTime() {
        SelectMonthDialog.Builder builder = new SelectMonthDialog.Builder(mContext);
        builder.setOnDialogClickListener((view, month) -> {
            mYearMonth=month.substring(0,4)+"-"+month.substring(4,6);
            YearMonthTv.setText(mYearMonth);
        });
        EasyDialog build = builder.build();
        build.setCancelable(true);
        build.show(getSupportFragmentManager());
    }

    // 获取支持银行列表
    public void get_support(String type, String form) {
        Bank.get_bank.Builder builder = Bank.get_bank.newBuilder();
        builder.setType(type).setForm(form);
        NetworkManager.INSTANCE.post(Apis.get_support, builder.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Bank.get_bank data = Bank.get_bank.parseFrom(bytes);
                Log.e("ljj支持银行",data.toString());
                banks.addAll(data.getListList());
            }

            @Override
            public void onError(int code, String errorMessage) {
            }
        });
    }
    /**
     * 获取验证码
     */
    private void getCode() {
        mPhone = bankPhoneEt.getText().toString();
        if (TextUtils.isEmpty(mPhone)) {
            showToast("请输入手机号码");
            return;
        }
        if (mPhone.length() != 11) {
            mPhone = null;
            showToast("请输入11位手机号码");
            return;
        }

        Connect.captcha.Builder builder = Connect.captcha.newBuilder();
        builder.setPhone(mPhone);
        builder.setType("3");
        Connect.captcha data = builder.build();
        showProgress();
        NetworkManager.INSTANCE.post(Apis.getMobileCode, data.toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                countDownReSend(getCodeTv, 60);
                hideProgress();
            }

            @Override
            public void onError(int code, String errorMessage) {
                hideProgress();
                showToast(errorMessage);
                if (code == 200) {
                    countDownReSend(getCodeTv, 60);
                }
            }
        });
    }
}
