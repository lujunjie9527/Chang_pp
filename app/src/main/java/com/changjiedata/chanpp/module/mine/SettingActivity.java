package com.changjiedata.chanpp.module.mine;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.base.MainApplication;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.module.login.LoginActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.utils.APKVersionCodeUtils;
import com.changjiedata.chanpp.utils.CacheUtil;
import com.changjiedata.chanpp.utils.DialogUntil;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends BaseActivity {
    @BindView(R.id.user_number_tv)
    TextView userNumberTv;
    @BindView(R.id.vision_tv)
    TextView visionTv;
//    private DialogUntil dialogUntil;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        userNumberTv.setText(UserHelper.getUser().getMember_mobile());
    }

    @OnClick({R.id.back_iv, R.id.user_number_llt, R.id.change_pwd_llt, R.id.clear_cacha_llt, R.id.check_vision_llt, R.id.submit_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.user_number_llt:
                break;
            case R.id.change_pwd_llt:
                startActivity(ChangePwdActivity.class);
                break;
            case R.id.clear_cacha_llt:
                DialogUntil.showDefaultDialog(this, "", "清理缓存", "确定", "取消", new DialogUntil.DialogImpl() {
                    @Override
                    public void onOk() {
                        CacheUtil.cleanAllCache(mContext);
                        showToast("清除完毕");
                    }
                });

                break;
            case R.id.check_vision_llt:
                break;
            case R.id.submit_btn:
                DialogUntil.showDefaultDialog(this, "", "确定退出登录？", new DialogUntil.DialogImpl() {
                    @Override
                    public void onOk() {
                        NetworkManager.INSTANCE.post(Apis.logout, new OnRequestCallBack() {
                            @Override
                            public void onOk(byte[] bytes) {
                            }

                            @Override
                            public void onError(int code, String errorMessage) {
                                showToast(errorMessage);
                            }
                        });
                        Intent intent2 = new Intent(mContext, LoginActivity.class);
                        intent2.putExtra("phone", UserHelper.getUser().getMember_mobile());
                        UserHelper.loginOut();
                        startActivity(intent2);
                        MainApplication.finishAllTempActivity();
                        finish();
                    }
                });
                break;
        }


    }

    private void checkVersion() {
        String versionName = APKVersionCodeUtils.getVerName(mContext);
        logE("version", versionName);
        HashMap<String, String> params = new HashMap<>();
      /*  NetworkManager.INSTANCE.post(Apis.versionCode, params, new OnRequestCallBack() {
            @Override
            public void onOk(String response) {
                VersionBean versionBean = GsonUtils.jsonToBean(response, VersionBean.class);
                float serviceVersion =  Float.valueOf(versionBean.getVersion()) ;
                float locationeVersion =  Float.valueOf(versionName) ;

                if (locationeVersion < serviceVersion) {
                    AllenVersionChecker instance = AllenVersionChecker.getInstance();
                    DownloadBuilder downloadBuilder = instance.downloadOnly(
                            UIData.create().setDownloadUrl(versionBean.getDownload_url()).setTitle(versionBean.getTitle()).setContent(versionBean.getContext()));
                    if (versionBean.isForce())
                    {
                        downloadBuilder.setForceUpdateListener(() -> {
                            MainApplication.finishAllActivity();
                            new Handler().postDelayed(() -> {
                                Process.killProcess(Process.myPid());
                                System.exit(0);
                                System.gc();
                            }, 500);
                        });
                    }
                    downloadBuilder.setForceRedownload(true).executeMission(mContext);
                }


            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                hideProgress();
            }
        });*/
    }
}
