package com.changjiedata.chanpp.module.shop;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.view.PhoneCode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputCardNumberActivity extends BaseActivity {
    @BindView(R.id.phoneCode_1)
    PhoneCode phoneCode1;
    private String Code;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_card_number);
        ButterKnife.bind(this);
        showSoftInputFromWindow(phoneCode1.et_code);
        phoneCode1.setOnInputListener(new PhoneCode.OnInputListener() {
            @Override
            public void onSucess(String code) {
                Code = code;
                makeCard();
            }

            @Override
            public void onInput() {

            }
        });
    }


    public void makeCard() {
        Intent intent = new Intent(this, AssistCardActivity.class);
        intent.putExtra("shopCode", getIntent().getStringExtra("shopCode"));
        intent.putExtra("snCode", Code);//
        startActivity(intent);
        finish();
    }


    @OnClick(R.id.title_tv)
    public void onViewClicked() {
        finish();
    }

    public void showSoftInputFromWindow(EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

}
