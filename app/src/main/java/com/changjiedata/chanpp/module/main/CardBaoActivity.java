package com.changjiedata.chanpp.module.main;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.proto.Bank;
import com.changjiedata.chanpp.utils.GsonUtils;
import com.changjiedata.chanpp.utils.ValidateUtils;
import com.changjiedata.chanpp.view.RecycleViewDialog;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kale.ui.view.dialog.EasyDialog;

public class CardBaoActivity extends BaseActivity {
    @BindView(R.id.tvTips)
    TextView tvTips;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private List<Bank.bank> banks = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_bao);
        ButterKnife.bind(this);
        get_support("2","1");
    }

    @OnClick({R.id.back_iv, R.id.tvSupBank,R.id.next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.tvSupBank:
                showBank();
                break;
                case R.id.next_btn:

                break;
        }
    }

    // 展示支持的信用卡列表dialog
    private void showBank() {
        if (ValidateUtils.isValidate(banks)) {
            RecycleViewDialog.Builder builder = new RecycleViewDialog.Builder(mContext);
            builder.setData(GsonUtils.serializedToJson(banks)).setTitle("VIP收款支持银行");
            EasyDialog build = builder.build();
            build.show(getSupportFragmentManager());
        } else {
            showToast("暂无支持银行");
        }
    }

    // 获取支持银行列表
    public void get_support(String type, String form) {
        Bank.get_bank.Builder builder = Bank.get_bank.newBuilder();
        builder.setType(type).setForm(form);
        NetworkManager.INSTANCE.post(Apis.get_support, builder.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Bank.get_bank data = Bank.get_bank.parseFrom(bytes);
                Log.e("ljj支持银行", data.toString());
                banks.addAll(data.getListList());
            }

            @Override
            public void onError(int code, String errorMessage) {
            }
        });
    }

}
