package com.changjiedata.chanpp.module.mine;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.proto.Memberfeedback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeedBackActivity extends BaseActivity {
    @BindView(R.id.content_et)
    EditText contentEt;
    @BindView(R.id.remain_tv)
    TextView tvCooperationHin;
    private String context="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);


        //显示输入字符数量
        showCharNumber(300);
        //判断etContent.gettext()是否为空
        if (TextUtils.isEmpty(contentEt.getText().toString())) {
            tvCooperationHin.setText("300");
        } else {
            int ed_lenght = contentEt.getText().length();
            int lenght = 300 - ed_lenght;
            tvCooperationHin.setText(String.valueOf(lenght));
        }
    }


    @OnClick({R.id.back_iv, R.id.submit_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.submit_btn:
                feedback_add();
                finish();
                break;
        }
    }


    private void showCharNumber(final int maxNumber) {
        contentEt.addTextChangedListener(new TextWatcher() {
            private CharSequence temp;
            private int selectionStart;
            private int selectionEnd;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                temp = s;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                int number = s.length();
                tvCooperationHin.setText(number + "/" + maxNumber);
                selectionStart = contentEt.getSelectionStart();
                selectionEnd = contentEt.getSelectionEnd();
                if (temp.length() > maxNumber) {
                    s.delete(selectionStart - 1, selectionEnd);
                    int tempSelection = selectionStart;
                    contentEt.setText(s);
                    contentEt.setSelection(tempSelection);
                    showToast("最多300字");
                }
            }
        });
    }


    // 意见反馈
    public void feedback_add() {
        context = contentEt.getText().toString().trim();
        if(context.isEmpty()){
//            showToast("反馈内容不能为空！");
        }else {
        showProgress();
        Memberfeedback.FeedbackAdd.Builder builders = Memberfeedback.FeedbackAdd.newBuilder();
        builders.setFeedback(context);
        NetworkManager.INSTANCE.post(Apis.feedback_add, builders.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) {
                hideProgress();
            }

            @Override
            public void onError(int code, String errorMessage) {
                showToast(errorMessage);
                hideProgress();
            }
        });
    }}
}
