package com.changjiedata.chanpp.module.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.adapter.ApplyRecordAdapter;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

//申请记录
public class ApplyRecordActivity extends BaseActivity {

    @BindView(R.id.back_iv)
    ImageView backIv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    ApplyRecordAdapter mAdapter;
    private List<Merchantservice.change_bank> datas = new ArrayList<>();

    private int page = 1;
    private int pagesize = 10;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        ButterKnife.bind(this);
        loadData();
        initRecyclerView();
    }


    public void loadData() {
        showProgress();
        Merchantservice.change_list.Builder builder = Merchantservice.change_list.newBuilder();
        builder.setMemberId(UserHelper.getUser().getMember_id());
        Merchantservice.change_list param = builder.build();

        NetworkManager.INSTANCE.post(Apis.change_list, param.toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                hideProgress();
                List<Merchantservice.change_bank> datas = Merchantservice.change_list.parseFrom(bytes).getListList();
                Log.e(" ljj修改列表", datas.toString());
                mAdapter.addData(datas);
                int size = datas.size();
                if (size < pagesize) {
                    mAdapter.loadMoreEnd(false);
                } else {
                    mAdapter.loadMoreComplete();
                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                hideProgress();
                showToast(errorMessage);
            }
        });
    }

    @OnClick(R.id.back_iv)
    public void onViewClicked() {
        finish();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(mAdapter = new ApplyRecordAdapter(datas));
        mAdapter.bindToRecyclerView(recyclerView);
        mAdapter.setEmptyView(R.layout.recycle_empty_record);
        //添加Android自带的分割线
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(mContext, ApplyDetailActivity.class);
                intent.putExtra("id", datas.get(position).getId());
                startActivity(intent);
            }
        });

        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                page = datas.size() / pagesize + 1;
                loadData();
            }
        }, recyclerView);
    }

}
