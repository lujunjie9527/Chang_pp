package com.changjiedata.chanpp.module.shanghunet;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.helper.GlideHelper;
import com.changjiedata.chanpp.helper.ImageHelper;
import com.changjiedata.chanpp.helper.PermissionHelper;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.module.login.LoginActivity;
import com.changjiedata.chanpp.module.main.MainActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.MemberOuterClass;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.changjiedata.chanpp.utils.CompressUtils;
import com.changjiedata.chanpp.utils.DialogUntil;
import com.google.protobuf.InvalidProtocolBufferException;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NetTwoActivity extends BaseActivity {
    @BindView(R.id.id_zheng_iv)
    ImageView idZhengIv;
    @BindView(R.id.id_bei_iv)
    ImageView idBeiIv;

    @BindView(R.id.hand_zheng2_iv)
    ImageView handZheng2Iv;
    @BindView(R.id.ivJieSuanKa)
    ImageView ivJieSuanKa;
    private int type;
    private String id_card_before = "";
    private String id_card_after = "";
    private String id_card_catch = "";
    private String bank_card = "";
    private String address = "";
    private String memberId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_hu_net_two);
        ButterKnife.bind(this);
        memberId=getIntent().getStringExtra("memberId");
    }


    @OnClick({R.id.tvExit, R.id.id_zheng_iv, R.id.id_bei_iv, R.id.hand_zheng2_iv, R.id.ivJieSuanKa, R.id.next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvExit:
                showTip();
                break;
            case R.id.id_zheng_iv:
                type = 0;
                selectPicture();
                break;
            case R.id.id_bei_iv:
                type = 1;
                selectPicture();
                break;
            case R.id.hand_zheng2_iv:  //手持身份证
                type = 2;
                selectPicture();
                break;
            case R.id.ivJieSuanKa:
                type = 3;
                selectPicture();
                break;
            case R.id.next_btn:
                upAllImage();
                break;
        }
    }

    public void selectPicture() {   //
//      this.type=type;
        PermissionHelper.getInstance().getPermission(mContext, new PermissionHelper.PermissionCallBack() {
            @Override
            public void onSuccess() {
                ImageHelper.showImageChoose(NetTwoActivity.this);
            }

            @Override
            public void onFail() {
                showToast("请先允许权限再使用此功能");
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    // 上传图片
    public void upLoadImage(String path, int upLoadType) {
        ImageHelper.upLoadImage(path, new ImageHelper.OnUpLoadImageCallBack() {
            @Override
            public void onSuccess(String images) throws InvalidProtocolBufferException {
                hideProgress();
                String[] temp = images.split(",");
                byte[] bytes = new byte[temp.length];
                for (int i = 0; i < temp.length; i++) {
                    bytes[i] = (byte) Integer.parseInt(temp[i]);
                }
                address = MemberOuterClass.img_upload.parseFrom(bytes).getImg();
                switch (upLoadType) {
                    case 0: // 身份证正面
                        id_card_before = address;  //上传图片并得到后台保存图片地址
                        GlideHelper.INSTANCE.loadImage(idZhengIv, path);
                        break;
                    case 1:   // 身份证反面
                        id_card_after = address;
                        GlideHelper.INSTANCE.loadImage(idBeiIv, path);
                        break;
                    case 2: // 手持身份证
                        id_card_catch = address;
                        GlideHelper.INSTANCE.loadImage(handZheng2Iv, path);
                        break;
                    case 3: // 银行卡正面
                        bank_card = address;
                        GlideHelper.INSTANCE.loadImage(ivJieSuanKa, path);
                        break;
                }
            }

            @Override
            public void onFail(String error) {
                showToast(error);
            }
        });
    }


    public void upAllImage() {
        showProgress();
        Merchantservice.open.Builder builder = Merchantservice.open.newBuilder();
        builder.setMemberId(UserHelper.getUser().getMember_id());  //
        builder.setIdCardBefore(id_card_before);    // 身份证正面
        builder.setIdCardAfter(id_card_after);   // 身份证反面
        builder.setIdCardCatch(id_card_catch);     // 手持身份证
        builder.setBankCard(bank_card);  // 银行卡正面
        Merchantservice.open open = builder.build();
        Log.e("上传信息打印", open.toString());
        NetworkManager.INSTANCE.post(Apis.merchant_open, open.toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                hideProgress();
                Merchantservice.open data = Merchantservice.open.parseFrom(bytes);
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                hideProgress();
                if (errorCode == 200) {
                    startActivity(MainActivity.class);
                    finish();
                } else {
                    showToast(errorMessage);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PictureConfig.CHOOSE_REQUEST) {
            List<LocalMedia> localMedias = PictureSelector.obtainMultipleResult(data);
            // 获取图片大小
            long length = CompressUtils.getFileByPath(localMedias.get(0).getCompressPath()).length();  //getCompressPath
            if (length <= 204800L) {// 原图片的压缩小于300kb
                String savePath = localMedias.get(0).getCompressPath();
                upLoadImage(savePath, type);
            } else {
                Handler handler = new Handler();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                NetTwoActivity.this.showProgress();
                            }
                        });
                        Bitmap bitmap = CompressUtils.getBitmap(localMedias.get(0).getCompressPath());
                        Bitmap compressBitmap = CompressUtils.compressByQuality(bitmap, 174080L);
                        String savePath = CompressUtils.Bitmap2File(mContext, compressBitmap);
//                    Log.e("mango", "进一步处理图片的压缩" + CompressUtils.getFileByPath(savePath).length() + "");
//                    Log.e("mango", "进一步处理图片的压缩" + savePath + "");
//                        showToast("进一步处理图片的压缩" + CompressUtils.getFileByPath(savePath).length() + "");
                        handler.post(() -> {
                            NetTwoActivity.this.upLoadImage(savePath, type);
                        });
                    }
                }).start();
            }
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            showTip();
        }
        return true;
    }

    private void showTip() {
        DialogUntil.showDefaultDialog(this, "", "确定退出到登录页面？", new DialogUntil.DialogImpl() {
            @Override
            public void onOk() {
                Intent intent2 = new Intent(mContext, LoginActivity.class);
                intent2.putExtra("phone", UserHelper.getUser().getMember_mobile());
                UserHelper.loginOut();
                startActivity(intent2);
                UserHelper.loginOut();
                finish();
            }
        });
    }
}