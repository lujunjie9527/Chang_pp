package com.changjiedata.chanpp.module.shop;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.module.createcard.CreateSucceedActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.fm.unionpaysdk.UnionpayTag;
import com.google.protobuf.InvalidProtocolBufferException;
import com.ruffian.library.widget.RRelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AssistCardActivity extends BaseActivity {
    @BindView(R.id.tvShopName)
    TextView tvShopName;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvShopBigType)
    TextView tvShopBigType;
    @BindView(R.id.tvShopLittleType)
    TextView tvShopLittleType;
    @BindView(R.id.tvJiHuo)
    TextView tvJiHuo;
    @BindView(R.id.user_massage_rlt)
    RRelativeLayout userMassageRlt;
    @BindView(R.id.tvFirst)
    TextView tvFirst;
    @BindView(R.id.tvSecond)
    TextView tvSecond;
    @BindView(R.id.tvCity)
    TextView tvCity;
    @BindView(R.id.tvCounty)
    TextView tvCounty;
    @BindView(R.id.bind_btn)
    Button btnStart;
    private UnionpayTag mUnionpayTag;
    private String tagID = "";
    private String snCode = "";
    private String shopStatus = "0";  // 默认为0
    private String shopCode;  // 默认为0
    private String shopTag = "";
    private String ShopUUid = "";
    private Merchantservice.shop data;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assist_card);
        ButterKnife.bind(this);
        tvFirst.setText(Html.fromHtml(" <b>第一步：</b>在支持NFC功能的手机上，在系统设置中打开NFC功能。"));
        tvSecond.setText(Html.fromHtml(" <b>第二步：</b>点击下方【开始读取】按钮后，保持碰一碰卡与手机背部贴合，直至卡片信息读取与写入完成即可完成制卡。"));
        shopCode = getIntent().getStringExtra("shopCode");
        snCode = getIntent().getStringExtra("snCode");
        tvJiHuo.setText(snCode);
        initView();
        initListener();
        initNFC();
    }


    private void initNFC() {
        // 初始化
        mUnionpayTag = new UnionpayTag();
        mUnionpayTag.initNFC(this);
    }

    private void initView() {
        Merchantservice.shop.Builder builder = Merchantservice.shop.newBuilder();
        builder.setShopCode(shopCode);
        Merchantservice.shop par = builder.build();
        Log.e("shop上传参数：", par.toString());
        NetworkManager.INSTANCE.post(Apis.shop_detail, builder.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                hideProgress();
                data = Merchantservice.shop.parseFrom(bytes);
                Log.e("shop_detail参数打印：", data.toString());
                shopStatus = data.getStatus();
                shopTag = data.getTag();
                ShopUUid = data.getUuid();
                tvShopName.setText(data.getShopName());
                tvAddress.setText(data.getProvinceName() + " " + data.getCityName() + " " + data.getCountyName());
                tvShopBigType.setText(data.getMccName() + " " + data.getSubName());
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                hideProgress();
                showToast(errorMessage);
            }
        });
    }


    private void initListener() {
        btnStart.setOnClickListener(v -> {
            // 1.读取卡片
            tagID = mUnionpayTag.getTagId();
            Log.e("tagID", "initListener: " + tagID);
            // 2.判断是否读取成功
            if (tagID.length() > 3)  // 超过3位视为读取成功
            {
                // 3.判断商户的状态
                switch (shopStatus) {
                    case "0":
                        applyShop();
                        break;
                    case "1":
                        // 判断卡片与申请是否一致
                        if (tagID.equals(ShopUUid)) {
                            startWriteCard(data.getTag());
                        } else {
                            showToast("读取卡片与申请卡片不一致");
                        }
                        break;
                    case "2":
                        showToast("商铺已经制卡");
                        break;
                    default:
                        showToast("未知错误");
                }
            } else {
                // 判断错误
                checkStatue(tagID);
                tagID = "";
            }
        });
    }

    private void applyShop() {
        showProgress();
        Merchantservice.apply_shop.Builder builder = Merchantservice.apply_shop.newBuilder();
        builder.setShopCode(data.getShopCode()).setUuid(tagID).setSnCode(snCode);
        NetworkManager.INSTANCE.post(Apis.apply_shop, builder.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] response) throws InvalidProtocolBufferException {
                Merchantservice.apply_shop apply_shop = Merchantservice.apply_shop.parseFrom(response);
                Log.e("ljj制卡：",apply_shop.toString());
                if (!apply_shop.getTag().isEmpty()) {
                    shopStatus = "1"; // 变为已经为制卡功能
                    shopTag = apply_shop.getTag();
                    ShopUUid = tagID;
                    startWriteCard(shopTag);
                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                hideProgress();
                showToast(errorMessage);
            }
        });
    }

    private void startWriteCard(String tag) {
        String result = mUnionpayTag.writeDataToTag(tag);
        checkStatue(result);
    }


    private void checkStatue(String value) {
        hideProgress();
        switch (value) {
            case "100":
                showProgress();
                Merchantservice.apply_shop.Builder builder=Merchantservice.apply_shop.newBuilder();
                builder.setShopCode(shopCode);
                builder.setUuid(ShopUUid);
                NetworkManager.INSTANCE.post(Apis.card_write,builder.build().toByteArray() , new OnRequestCallBack() {
                    @Override
                    public void onOk(byte[] bytes) throws InvalidProtocolBufferException {

                    }

                    @Override
                    public void onError(int errorCode, String errorMessage) {
                    }
                });
                Intent intent = new Intent(mContext, CreateSucceedActivity.class);
                startActivity(intent);
                finish();
                break;
            case "101":
                Toast.makeText(this, "卡片只能读取数据", Toast.LENGTH_SHORT).show();
                break;
            case "102":
                Toast.makeText(this, "卡片不是NDEF格式的卡片", Toast.LENGTH_SHORT).show();
                break;
            case "103":
                Toast.makeText(this, "写入卡片数据超过卡片可写入最大值", Toast.LENGTH_SHORT).show();
                break;
            case "104":
                Toast.makeText(this, "手机不支持nfc功能 或手机未打开nfc", Toast.LENGTH_SHORT).show();
                break;
            case "105":
                Toast.makeText(this, "卡片Tag连接失败,请将手机放到卡片上面", Toast.LENGTH_SHORT).show();
                break;
            case "106":
                Toast.makeText(this, "卡片设置只读模式失败", Toast.LENGTH_SHORT).show();
                break;
            case "107":
            default:
                Toast.makeText(this, "未知错误", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @OnClick({R.id.iv_back, R.id.bind_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.bind_btn:
                startActivity(SucceedCardActivity.class);
                break;
        }
    }
}
