package com.changjiedata.chanpp.module.shop;

import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.changjiedata.chanpp.utils.date.DateUtil;
import com.google.protobuf.InvalidProtocolBufferException;
import com.ruffian.library.widget.RRelativeLayout;
import com.ruffian.library.widget.RTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CardStatusActivity extends BaseActivity {
    @BindView(R.id.tvShopName)
    TextView tvShopName;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvCity)
    TextView tvCity;
    @BindView(R.id.tvShopBigType)
    TextView tvShopBigType;
    @BindView(R.id.tvShopLittleType)
    TextView tvShopLittleType;
    @BindView(R.id.tvJiHuo)
    TextView tvJiHuo;
    @BindView(R.id.user_massage_rlt)
    RRelativeLayout userMassageRlt;
    @BindView(R.id.llt)
    LinearLayout llt;
    @BindView(R.id.tvTipOne)
    TextView tvTipOne;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tvTipTwo)
    TextView tvTipTwo;
    @BindView(R.id.tvCounty)
    TextView tvCounty;
    @BindView(R.id.tvSucceedTime)
    TextView tvSucceedTime;
    @BindView(R.id.rtvRadius)
    RTextView  rtvRadius;

    private String shopStatus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);
        ButterKnife.bind(this);
        initView();
    }


    private void initView() {
        Merchantservice.shop.Builder builder = Merchantservice.shop.newBuilder();
        builder.setShopCode(getIntent().getStringExtra("shopCode"));
        Merchantservice.shop par = builder.build();
        Log.e("shop上传参数：", par.toString());
        NetworkManager.INSTANCE.post(Apis.shop_detail, builder.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                hideProgress();
                Merchantservice.shop data = Merchantservice.shop.parseFrom(bytes);
                Log.e("shop_detail参数打印：", par.toString());
                shopStatus = data.getStatus();
                tvShopName.setText(data.getShopName());
                tvAddress.setText(data.getProvinceName() + " " + data.getCityName() + " " + data.getCountyName());
                tvShopBigType.setText(data.getMccName() + " " + data.getSubName());
                tvJiHuo.setText(data.getSnCode());
                if (shopStatus.equals("1")) {
                    tvTipOne.setText("资料提交成功");
                    tvTipTwo.setText("尚未制卡");
                    tvTime.setText(DateUtil.getStrTime(data.getCreatedTime()));
                } else {
                    tvTipOne.setText("资料提交");
                    tvTipTwo.setText("制卡成功");
                    tvTime.setText(DateUtil.getStrTime(data.getCreatedTime()));
                    tvSucceedTime.setText(DateUtil.getStrTime(data.getUpdateTime()));
                }
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                hideProgress();
                showToast(errorMessage);
            }
        });
    }

    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        finish();
    }
}
