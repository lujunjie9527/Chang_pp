package com.changjiedata.chanpp.module.main;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ApplyDetailActivity extends BaseActivity {
    @BindView(R.id.tvDetail)
    TextView tvDetail;
    @BindView(R.id.tvBankName)
    TextView tvBankName;
    @BindView(R.id.tvBankNum)
    TextView tvBankNum;
    @BindView(R.id.tvBankName2)
    TextView tvBankName2;
    @BindView(R.id.tvBankNum2)
    TextView tvBankNum2;
    @BindView(R.id.tvProgress)
    TextView tvProgress;
    @BindView(R.id.llt)
    LinearLayout llt;
    @BindView(R.id.tvTipOne)
    TextView tvTipOne;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tvTipTwo)
    TextView tvTipTwo;
    @BindView(R.id.tvStatusTime)
    TextView tvStatusTime;
    @BindView(R.id.tvTip3)
    TextView tvTip3;
    @BindView(R.id.rltProgress)
    RelativeLayout rltProgress;
    @BindView(R.id.lltBig)
    LinearLayout lltBig;
    @BindView(R.id.rtvRadius)
    TextView rtvRadius;

    private String bankName;
    private String bankCard;
    private String oldBankName;
    private String status;
    private List<Merchantservice.change_bank> data = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_detail);
        ButterKnife.bind(this);
        loadData();
    }

    public void initView() {
        status = data.get(0).getStatus();
        oldBankName = data.get(0).getImg();
        bankName = data.get(0).getBankName();
        bankCard = data.get(0).getBankCard().substring(data.get(0).getBankCard().length()-4);
        tvBankName.setText(oldBankName);
        tvBankName2.setText(bankName);
        tvBankNum2.setText(bankCard);
        tvTime.setText(data.get(0).getCreatedTime());
        if (status.equals("0")) {
            tvTipTwo.setText("等待客服人员审核中");
            tvTipTwo.setTextColor(Color.parseColor("#CA96AB"));
            GradientDrawable gradientDrawable = (GradientDrawable) rtvRadius.getBackground();
            gradientDrawable.setColor(Color.parseColor("#CA96AB"));
            tvStatusTime.setText("我们将会在24小时内完成审核");
            tvStatusTime.setTextSize(17f);
        } else if (status.equals("1")) {
            tvTipTwo.setText("审核通过");
            tvTipTwo.setTextColor(Color.parseColor("#F65449"));
            GradientDrawable gradientDrawable = (GradientDrawable) rtvRadius.getBackground();
            gradientDrawable.setColor(Color.parseColor("#F65449"));
            tvStatusTime.setText(data.get(0).getUpdateTime());
            tvTip3.setVisibility(View.VISIBLE);
        } else if (status.equals("2")) {
            tvTipTwo.setText("审核不通过");
            tvTipTwo.setTextColor(Color.parseColor("#9E9E9E"));
            GradientDrawable gradientDrawable = (GradientDrawable) rtvRadius.getBackground();
            gradientDrawable.setColor(Color.parseColor("#9E9E9E"));
            tvStatusTime.setText(data.get(0).getUpdateTime());
            tvTip3.setVisibility(View.VISIBLE);
            tvTip3.setText("原因：" + data.get(0).getMsg());
        }
    }

    //查询修改明细
    public void loadData() {
        showProgress();
        Merchantservice.change_bank.Builder builder = Merchantservice.change_bank.newBuilder();
        builder.setId(getIntent().getStringExtra("id"));
        Merchantservice.change_bank change_bank = builder.build();
        NetworkManager.INSTANCE.post(Apis.change_detail, change_bank.toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Merchantservice.change_bank datas = Merchantservice.change_bank.parseFrom(bytes);
                Log.e(" ljj修改列表详情", datas.toString());
                data.add(datas);
                hideProgress();
                initView();

            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                hideProgress();
                showToast(errorMessage);
            }
        });
    }

    @OnClick(R.id.back_iv)
    public void onViewClicked() {
        finish();
    }
}
