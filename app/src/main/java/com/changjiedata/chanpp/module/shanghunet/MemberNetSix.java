package com.changjiedata.chanpp.module.shanghunet;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MemberNetSix extends BaseActivity {
    @BindView(R.id.sn_number_et)
    EditText snNumberEt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_hu_net_6);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.back_iv, R.id.next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                break;
            case R.id.next_btn:
                break;
        }
    }
}
