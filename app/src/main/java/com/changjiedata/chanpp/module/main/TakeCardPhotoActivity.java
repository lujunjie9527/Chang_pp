package com.changjiedata.chanpp.module.main;

import android.os.Bundle;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TakeCardPhotoActivity extends BaseActivity {
    @BindView(R.id.texture)
    TextureView texture;
    @BindView(R.id.takecard)
    ImageView takecard;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_card_photo);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.iv_back, R.id.texture, R.id.takecard})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.texture:
                break;
            case R.id.takecard:
                break;
        }
    }
}
