package com.changjiedata.chanpp.module.shop;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.adapter.ShopListAdapter;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.changjiedata.chanpp.utils.ValidateUtils;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyShopActivity extends BaseActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private List<Merchantservice.shop> shopList = new ArrayList<>();
    private ShopListAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_shop);
        ButterKnife.bind(this);
        initRecyclerView();
    }

    public void shop_list() {
        showProgress();
        Merchantservice.shop_list.Builder builder = Merchantservice.shop_list.newBuilder();
        builder.setMemberId(UserHelper.getUser().getMember_id());
        NetworkManager.INSTANCE.post(Apis.shop_list, builder.build().toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                hideProgress();
                Merchantservice.shop_list data = Merchantservice.shop_list.parseFrom(bytes);
                Log.e("ljj商铺列表：", data.toString());
                shopList.addAll(data.getListList());
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                hideProgress();
                showToast(errorMessage);
            }
        });
    }

    public void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(mAdapter = new ShopListAdapter(shopList));
        mAdapter.bindToRecyclerView(recyclerView);
        if (!ValidateUtils.isValidate(shopList)) {//
            mAdapter.setEmptyView(R.layout.recycle_empty_member);
        }
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            Intent intent;
            if (shopList.get(position).getStatus().equals("0")) {
                intent = new Intent(mContext, InputCardNumberActivity.class); //
            } else if (shopList.get(position).getStatus().equals("1")) {
                intent = new Intent(mContext, AssistCardActivity.class);
                intent.putExtra("snCode", shopList.get(position).getSnCode());
            } else {
                intent = new Intent(mContext, CardStatusActivity.class);
            }
            intent.putExtra("shopCode", shopList.get(position).getShopCode());
            startActivity(intent);
        });
    }

    @OnClick({R.id.back_iv, R.id.btnNext})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.btnNext:
                startActivity(AddShopActivity.class);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        shopList.clear();
        shop_list();
    }
}
