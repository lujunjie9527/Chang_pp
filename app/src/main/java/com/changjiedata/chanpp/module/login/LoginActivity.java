package com.changjiedata.chanpp.module.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.base.MainApplication;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.helper.UserBean;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.module.main.MainActivity;
import com.changjiedata.chanpp.module.shanghunet.NetOneActivity;
import com.changjiedata.chanpp.module.shanghunet.NetTwoActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.proto.LoginOuterClass;
import com.changjiedata.chanpp.proto.MemberOuterClass;
import com.google.protobuf.InvalidProtocolBufferException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity {
    @BindView(R.id.back_iv)
    ImageView backIv;
    @BindView(R.id.phone_edt)
    EditText phoneEdt;
    @BindView(R.id.password_edt)
    EditText passwordEdt;
    @BindView(R.id.login_btn)
    Button loginBtn;

    @BindView(R.id.login_cb_password)
    CheckBox loginCbPassword;
    private long mExitTime;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        phoneEdt.setText(UserHelper.getUser().getMember_mobile());
        setPasswordCanSee();
    }

    @OnClick({R.id.back_iv, R.id.login_btn, R.id.forget_pwd_tv, R.id.register_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_btn:
                login();
                break;
            case R.id.forget_pwd_tv:
                startActivity(ForgetPasswordActivity.class);
                break;
            case R.id.register_tv:
                startActivity(RegisterActivity.class);
                break;
        }
    }

    public void login() {

        String account = phoneEdt.getText().toString();
        if (TextUtils.isEmpty(account)) {
            showToast("请输入账号");
            return;
        }
        String pwd = passwordEdt.getText().toString();
        if (TextUtils.isEmpty(pwd)) {
            showToast("请输入密码");
            return;
        }
        showProgress();
        LoginOuterClass.Login.Builder builder = LoginOuterClass.Login.newBuilder();
        builder.setUsername(account);
        builder.setPassword(pwd);
        builder.setClient("android");
        LoginOuterClass.Login data = builder.build();
        Log.e("发送参数打印：", data.toString());
        NetworkManager.INSTANCE.post("index.php/mobile/login/index", data.toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                LoginOuterClass.Login resp = LoginOuterClass.Login.parseFrom(bytes);
                loginSuccess(resp);
                checkIsRenZhen();
            }

            @Override
            public void onError(int code, String errorMessage) {
                hideProgress();
                showHintDialog(errorMessage);
            }
        });
    }


    private void loginSuccess(LoginOuterClass.Login respon) {  //没有member_id
        UserBean userBean = new UserBean();
        userBean.setKey(respon.getKey());
        userBean.setChat_id(respon.getChatId());
        userBean.setChat_pwd(respon.getChatPwd());
        userBean.setPush_id(respon.getPushId());
        UserHelper.login(userBean);  //极光推送
        showToast("登录成功");
    }
    /**
     * 验证是否进行实名认证
     */
    private void checkIsRenZhen() {
        NetworkManager.INSTANCE.post(Apis.memberDataInfo, new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                hideProgress();
                MemberOuterClass.Member member = MemberOuterClass.Member.parseFrom(bytes);
                UserBean userBean = new UserBean();
                userBean.setStep(member.getStep());
                userBean.setMember_id(member.getMemberId());
                UserHelper.syncCurrentUserInfo(userBean);   //同步个人信息
                switch (member.getStep()) {
                    case "1":
                        Intent intent=new Intent(mContext, NetOneActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    case "2":
                        startActivity(NetTwoActivity.class);
                        finish();
                        break;
                    case "3":
                        startActivity(MainActivity.class);
                        finish();
                        break;
                }

            }

        @Override
        public void onError ( int errorCode, String errorMessage){
            hideProgress();
            showToast(errorMessage);

        }
    });
}

    /**
     * 设置密码是否为明文显示
     */

    private void setPasswordCanSee() {
        loginCbPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    //设置为密文显示
                    passwordEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    //设置为明文显示
                    passwordEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                passwordEdt.setSelection(passwordEdt.length());
            }
        });
    }


    //返回
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
                return true;
            }
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                Toast.makeText(mContext, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                mExitTime = System.currentTimeMillis();
            } else {
                MainApplication.finishAllActivity();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


}