package com.changjiedata.chanpp.module.shanghunet;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MemberNetFive extends BaseActivity {
    @BindView(R.id.name_et)
    EditText nameEt;
    @BindView(R.id.bank_number_et)
    EditText bankNumberEt;
    @BindView(R.id.valid_time_et)
    EditText validTimeEt;
    @BindView(R.id.detail_address_et)
    EditText detailAddressEt;
    @BindView(R.id.phone_edt)
    EditText phoneEdt;
    @BindView(R.id.mobile_code_et)
    EditText mobileCodeEt;
    @BindView(R.id.get_code_tv)
    TextView getCodeTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_hu_net_5);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.back_iv, R.id.get_code_tv, R.id.next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                break;
            case R.id.get_code_tv:
                break;
            case R.id.next_btn:
                break;
        }
    }
}
