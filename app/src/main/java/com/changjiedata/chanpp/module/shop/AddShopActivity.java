package com.changjiedata.chanpp.module.shop;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.module.main.LocationSelectActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.google.protobuf.InvalidProtocolBufferException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddShopActivity extends BaseActivity {
    @BindView(R.id.etShopName)
    EditText etShopName;
    @BindView(R.id.remain_tv)
    TextView tvRemain;
    @BindView(R.id.back_iv)
    ImageView backIv;
    @BindView(R.id.tvMcc)
    TextView tvMcc;
    @BindView(R.id.tvShopCity)
    TextView tvShopCity;

    private String province1 = "";
    private String city = "";
    private String county = "";
    private String provinceCode = "";
    private String cityCode = "";
    private String countyCode = "";
    private String shopName = "";
    private String mcc_name = "";
    private String mcc = "";
    private String shopCode = "";
    private String sub_mcc = "";
    private String sub_name = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_shop);
        ButterKnife.bind(this);
        listenerText();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 100) {
            province1 = data.getStringExtra("province");
            provinceCode = data.getStringExtra("provinceCode");
            city = data.getStringExtra("city");
            cityCode = data.getStringExtra("cityCode");
            county = data.getStringExtra("county");
            countyCode = data.getStringExtra("countyCode");
            tvShopCity.setText(province1 + " " + city + " " + county);

        }
        if (resultCode == RESULT_OK && requestCode == 200) {
            mcc = data.getStringExtra("mcc");
            mcc_name = data.getStringExtra("mcc_name");
            sub_mcc = data.getStringExtra("sub_mcc");
            sub_name = data.getStringExtra("sub_name");
            tvMcc.setText(mcc_name + " " + sub_name);
        }

    }

    public void makeCard(int type) {
        shopName = etShopName.getText().toString().trim();
        if (shopName.isEmpty()) {
            showToast("请填写店铺名称");
            return;
        }
        if (mcc_name.isEmpty()) {
            showToast("请选择商户类别");
            return;
        }
        if (tvShopCity.getText().toString().trim().isEmpty()) {
            showToast("请选择商户所在地");
            return;
        }
        showProgress();
        Merchantservice.add_shop.Builder builder = Merchantservice.add_shop.newBuilder();
        builder.setShopName(shopName);
        builder.setMcc(mcc);
        builder.setMccName(mcc_name);
        builder.setSubMcc(sub_mcc);
        builder.setSubName(sub_name);
        builder.setProvinceCode(provinceCode);
        builder.setProvinceName(province1);
        builder.setCityCode(cityCode);
        builder.setCityName(city);
        builder.setCountyCode(countyCode);
        builder.setCountyName(county);
        Merchantservice.add_shop param = builder.build();
        Log.e("ljjadd_shop新增店铺：",param.toString());
        if (type == 0) {
            NetworkManager.INSTANCE.post(Apis.add_shop, param.toByteArray(), new OnRequestCallBack() {
                @Override
                public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                    hideProgress();
                    Merchantservice.add_shop data = Merchantservice.add_shop.parseFrom(bytes);
                    shopCode = data.getShopCode();
                    Intent intent = new Intent(mContext, InputCardNumberActivity.class);// AssistCardActivity
                    intent.putExtra("shopCode", shopCode);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onError(int errorCode, String errorMessage) {
                    hideProgress();
                    showToast(errorMessage);
                }
            });
        } else if (type == 1) {
            NetworkManager.INSTANCE.post(Apis.add_shop_ios, param.toByteArray(), new OnRequestCallBack() {
                @Override
                public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                    hideProgress();
                    Merchantservice.add_shop data = Merchantservice.add_shop.parseFrom(bytes);
                    shopCode = data.getShopCode();
                    Intent intent = new Intent(mContext, MyShopActivity.class);// AssistCardActivity
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onError(int errorCode, String errorMessage) {
                    hideProgress();
                    showToast(errorMessage);
                }
            });
        }

    }


    public void listenerText() {  //监听text
        showCharNumber(8);
        //判断etContent.gettext()是否为空
        if (TextUtils.isEmpty(etShopName.getText().toString())) {
            tvRemain.setText("8");
        } else {
            int ed_lenght = etShopName.getText().length();
            int lenght = 8 - ed_lenght;
            tvRemain.setText(String.valueOf(lenght));
        }
    }

    /**
     * 显示输入字符数量
     *
     * @param maxNumber
     */
    private void showCharNumber(final int maxNumber) {
        etShopName.addTextChangedListener(new TextWatcher() {
            private CharSequence temp;
            private int selectionStart;
            private int selectionEnd;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                temp = s;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                int number = s.length();
                tvRemain.setText(number + "/" + maxNumber);
                selectionStart = etShopName.getSelectionStart();
                selectionEnd = etShopName.getSelectionEnd();
                if (temp.length() > maxNumber) {
                    s.delete(selectionStart - 1, selectionEnd);
                    int tempSelection = selectionStart;
                    etShopName.setText(s);
                    etShopName.setSelection(tempSelection);
                    showToast("最多8字");
                }
            }
        });
    }

    @OnClick({R.id.back_iv, R.id.tvMcc, R.id.lltAddress, R.id.btnAddNew, R.id.btnMakeCard})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                finish();
                break;
            case R.id.tvMcc:
                startActivityForResult(SelectMccActivity.class, 200);
                break;
            case R.id.lltAddress:
                Intent intent = new Intent(mContext, LocationSelectActivity.class);
                intent.putExtra("type", "1");
                startActivityForResult(intent, 100);
                break;
            case R.id.btnAddNew:
                makeCard(0);
                break;
            case R.id.btnMakeCard:  //服务商制卡
                makeCard(1);
                break;
        }
    }


}
