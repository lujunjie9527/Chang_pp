package com.changjiedata.chanpp.module.main;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.adapter.CreateSelectCardAdapter;
import com.changjiedata.chanpp.base.BaseActivity;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.changjiedata.chanpp.utils.StatusBarUtil;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateSelectCardActivity extends BaseActivity {
    @BindView(R.id.back_iv)
    ImageView backTv;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.et_search)
    EditText etSearch;
    private CreateSelectCardAdapter mAdapter;
    private List<Merchantservice.basics> datas = new ArrayList<>();
    private String bankName;
    private String provice;
    private String city;
    private String county;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_bank);
        ButterKnife.bind(this);
        StatusBarUtil.setStatusBarLightMode(this, Color.parseColor("#ffffff"), true);
        initRecyclerView();
        initListener();
        tvTitle.setText("选择结算卡所在支行");
        bankName = getIntent().getStringExtra("bankName");
        provice = getIntent().getStringExtra("provice");
        city = getIntent().getStringExtra("city");
        county = getIntent().getStringExtra("county");
        name = "";
        initData();
    }

    private void initListener() {
        backTv.setOnClickListener(view -> finish());
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    name = "";
                } else {
                    name = s.toString();
                }
                initData();
            }
        });

    }

    private void initData() {
        showProgress();
        Merchantservice.channel_list.Builder builder = Merchantservice.channel_list.newBuilder();
        builder.setBankName(bankName);
        Log.e("ljjbankName参数：", bankName);

        builder.setProvince(provice);
        builder.setCity(city);
        builder.setCounty(county);
        builder.setName(name);
        Merchantservice.channel_list param = builder.build();
        Log.e("ljj支行参数：", param.toString());

        NetworkManager.INSTANCE.post(Apis.channel_list, param.toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                hideProgress();
                List<Merchantservice.basics> lists = Merchantservice.channel_list.parseFrom(bytes).getListList();
                Log.e("ljj支行：", lists.toString());
                mAdapter.replaceData(lists);
            }

            @Override
            public void onError(int errorCode, String errorMessage) {
                hideProgress();
                showToast(errorMessage);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(mAdapter = new CreateSelectCardAdapter(mContext, datas));
        mAdapter.bindToRecyclerView(recyclerView);
        mAdapter.setEmptyView(R.layout.recycle_empty);
        // 每个item的点击事件
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            Intent intent = new Intent();
            intent.putExtra("code", datas.get(position).getCode());
            intent.putExtra("name", datas.get(position).getName());
            setResult(RESULT_OK, intent);
            finish();
        });
    }

}
