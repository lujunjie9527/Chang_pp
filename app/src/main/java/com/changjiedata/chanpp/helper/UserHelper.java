package com.changjiedata.chanpp.helper;

import android.content.Context;
import android.content.Intent;
/*
import com.example.havi.bean.HyChatUser;
import com.example.havi.helper.ChatHelper;*/
import com.changjiedata.chanpp.bean.ServiceBean;
import com.changjiedata.chanpp.module.login.LoginActivity;
import com.changjiedata.chanpp.push.JPushHelper;
import com.changjiedata.chanpp.utils.GsonUtils;
import com.changjiedata.chanpp.utils.LogUtil;
import com.changjiedata.chanpp.utils.PreferenceSettings;
import com.changjiedata.chanpp.utils.ValidateUtils;

public class
UserHelper {
    private static final String CURRENT_USER_INFO = "current_user_info";
    private static final String CURRENT_KeFuInfo_INFO = "current_ke_fu_info";

    public static boolean checkGoLogin(Context context) {
        if (!UserHelper.isLogin()) {
            Intent intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);
            return true;
        } else {
            return false;
        }
    }

    /**
     * 登录执行 发送登录成功消息
     *
     * @param userBean
     */
    public static void login(UserBean userBean) {
        saveCurrentUserInfo(userBean);
        //设置别名
        JPushHelper.setAlias(UserHelper.getUser().getPush_id());

    }

    /**
     * 退出登录执行 发送退出消息
     */
    public static void loginOut() {
        clearCurrentUserInfo();
        // 取消别名
        JPushHelper.setAlias("");
    }


    /**
     * 是否登录
     *
     * @return true yes
     */
    public static boolean isLogin() {
        return ValidateUtils.isValidate((getCurrentToken()));
    }

    /**
     * 缓存登录用户信息 这里只在登录调用
     *
     * @param bean
     */
    public static void saveCurrentUserInfo(UserBean bean) {
        LogUtil.e("Wang", "保存的登录信息:  " + GsonUtils.serializedToJson(bean));
        PreferenceSettings.setString(CURRENT_USER_INFO, GsonUtils.serializedToJson(bean));
    }

    /**
     * 同步个人信息
     *
     * @param bean
     */
    public static void syncCurrentUserInfo(UserBean bean) {
        LogUtil.e("Wang", "同步个人信息:  " + GsonUtils.serializedToJson(bean));
        UserBean user = getUser();  //同步缓存用户信息
        if (user == null) {
            return;
        }
        //这里 一般个人信息接口不返回key 的标识性字段 ，同步时处理下几个关键字段 其他用个人接口覆盖原来信息
        bean.setKey(user.getKey());
        bean.setPush_id(user.getPush_id());
        bean.setChat_id(user.getChat_id());
        bean.setChat_pwd(user.getChat_pwd());
        PreferenceSettings.setString(CURRENT_USER_INFO, GsonUtils.serializedToJson(bean));
/*
        // 同步缓存聊天信息
        HyChatUser chatUser = new HyChatUser();
        chatUser.setAvatar(bean.getMember_avatar());
        chatUser.setChatPassword(bean.getChat_pwd());
        chatUser.setChatUserName(bean.getTruename());
        chatUser.setNickName(bean.getTruename());
        chatUser.setChatId(bean.getChat_id());
        chatUser.setUserId(bean.getMember_id()+"");
        ChatHelper.getInstance().syncChatUserInfo(chatUser);*/

    }

    public static void saveAvatar(String avatar) {
        UserBean user = getUser();
        user.setMember_avatar(avatar);
        PreferenceSettings.setString(CURRENT_USER_INFO, GsonUtils.serializedToJson(user));
    }

    public static void saveUserName(String userName) {
        UserBean user = getUser();
        user.setMember_name(userName);
        PreferenceSettings.setString(CURRENT_USER_INFO, GsonUtils.serializedToJson(user));
    }

    public static void saveUserMobile(String phone) {
        UserBean user = getUser();
        user.setMember_mobile(phone);
        PreferenceSettings.setString(CURRENT_USER_INFO, GsonUtils.serializedToJson(user));
    }

    /**
     * 获取缓存登录用户信息
     */
    public static UserBean getUser() {
        UserBean userBean = GsonUtils.jsonToBean(PreferenceSettings.getString(CURRENT_USER_INFO, ""), UserBean.class);
        return userBean == null ? new UserBean() : userBean;
    }

    public static String getCurrentToken() {
        UserBean user = getUser();
        return user == null ? "" : user.getKey();
    }

    public static String getStep() {
        UserBean user = getUser();
        return user == null ? "" : user.getStep();
    }

    /**
     * 清除缓存登录用户信息
     */
    public static void clearCurrentUserInfo() {
        PreferenceSettings.setString(CURRENT_USER_INFO, "");
    }

    /**
     * 缓存客服信息
     *
     * @param bean
     */
    public static void saveKeFuUserInfo(ServiceBean bean) {
        LogUtil.e("frank", "保存的登录信息:  " + GsonUtils.serializedToJson(bean));
        PreferenceSettings.setString(CURRENT_KeFuInfo_INFO, GsonUtils.serializedToJson(bean));
    }

    /**
     * 获取缓存登录用户信息
     */
    public static ServiceBean getKeFuInfo() {
        ServiceBean userBean = GsonUtils.jsonToBean(PreferenceSettings.getString(CURRENT_KeFuInfo_INFO, ""), ServiceBean.class);
        return userBean == null ? new ServiceBean() : userBean;
    }
}
