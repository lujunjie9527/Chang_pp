package com.changjiedata.chanpp.helper;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.changjiedata.chanpp.R;


/**
 * Created by FrankZhang on 2017/11/16.
 */

public class DialogUtils {

    public static void showDefaultDialog(Context context, String title, String content, final DialogImpl listener) {
        showDefaultDialog(context, title, content, "确定", "取消", listener);
    }

    public static void showDefaultDialog(Context context, String title, String content, String ok, String cancel, final DialogImpl listener) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(content);
        builder.setNegativeButton(cancel, (dialog, which) -> listener.onCancel());
        builder.setPositiveButton(ok, (dialog, which) -> listener.onOk());
        builder.show();
    }

    public static Dialog showHitDialog(Context context, String title, String content) {
        return showHitDialog(context, title, content, null);
    }

    public static Dialog showHitDialog(Context context, String title, String content, DialogImpl impl) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(content);
//        builder.setNegativeButton("取消", null);
        builder.setPositiveButton("确定", (dialog, which) -> {
            if (impl != null) {
                impl.onOk();
            }
        });
        return builder.show();
    }

    public static class DialogImpl {
        public void onOk() {

        }

        public void onCancel() {

        }
    }


    /**
     * 底部菜单选择
     *
     * @param context
     * @param menu1_str
     * @param menu2_str
     * @param impl
     */
    public static void showBottomMenuDialog(Context context, String title, String menu1_str, String menu2_str, BottomMenuDialogImpl impl) {
        final Dialog mBottomSheetDialog = new Dialog(context, R.style.MaterialDialogSheet);
        View view = View.inflate(context, R.layout.dialog_bottom_tow_menu_sheet, null);
        TextView title_tv = view.findViewById(R.id.title_tv);
        title_tv.setText(title);
        TextView menu1 = view.findViewById(R.id.menu1);
        TextView menu2 = view.findViewById(R.id.menu2);
        menu1.setText(menu1_str);
        menu2.setText(menu2_str);
        TextView cancel = view.findViewById(R.id.cancel);

        menu1.setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            impl.onMenu1();
        });
        menu2.setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            impl.onMenu2();
        });

        cancel.setOnClickListener(v -> mBottomSheetDialog.dismiss());

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }

    /**
     * 底部菜单选择
     *
     * @param context
     * @param menu1_str
     * @param menu2_str
     * @param impl
     */
    public static void showBottomMenuDialogAndHit(Context context, String title, String menu1_str, String menu2_str, BottomMenuDialogImpl impl) {
        final Dialog mBottomSheetDialog = new Dialog(context, R.style.MaterialDialogSheet);
        View view = View.inflate(context, R.layout.dialog_bottom_tow_menu_sheet, null);
        TextView title_tv = view.findViewById(R.id.title_tv);
        title_tv.setText(title);
        TextView menu1 = view.findViewById(R.id.menu1);
        TextView menu2 = view.findViewById(R.id.menu2);
        menu2.setVisibility(View.GONE);
        title_tv.setVisibility(View.GONE);
        menu1.setText(menu1_str);
        menu2.setText(menu2_str);
        TextView cancel = view.findViewById(R.id.cancel);

        menu1.setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            impl.onMenu1();
        });
        menu2.setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            impl.onMenu2();
        });
        cancel.setOnClickListener(v -> mBottomSheetDialog.dismiss());
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }

    public interface BottomMenuDialogImpl {
        void onMenu1();

        void onMenu2();
    }

}
