package com.changjiedata.chanpp.helper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.changjiedata.chanpp.base.MainApplication;


/**
 * 调用方式

 Intent intent = new Intent(MainApplication.getAppContext(), MessageActivity.class);
 intent.addCategory(Intent.CATEGORY_LAUNCHER);
 intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
 intent.putExtra("type", 1);
 PendingIntent pendingIntent = PendingIntent.getActivity(MainApplication.getAppContext(), 1, intent,
 PendingIntent.FLAG_UPDATE_CURRENT);
 NotificationHelper.getInstance.sendNotification(1,"今日菜市", "您有新的聊天消息", pendingIntent);

 //消除通知
 NotificationManager systemService = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
 systemService.cancel(101122);
 */


/**
 * 通知帮助类
 * 兼容android8.0版本 （对于通知提醒方式取决于厂商系统本身，有的是桌面角标 有的是状态栏提示 不纠结）
 * 说明：在 Android 8.0 上，增加了一个 NotificationChannel （通知渠道）的概念，用来允许您为要显示的每种通知类型创建用户可自定义的渠道。
 * 用户界面将通知渠道称之为通知类别。 如果不设置Channel 内部会报错，导致通知不显示
 */
public class NotificationHelper extends ContextWrapper {
    private NotificationManager manager;
    public static final String id = "hyphenateui_id_1"; //channel id
    public static final String name = "hyphenateui_name_1"; //channel name
    private static NotificationHelper instance;

    private NotificationHelper(Context context) {
        super(context);
    }

    public static NotificationHelper getInstance(){
       return instance==null?instance= new NotificationHelper(MainApplication.getAppContext()):instance;
    }

    private NotificationManager getManager() {
        if (manager == null) {
            manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
        return manager;
    }

    private NotificationCompat.Builder getNotification_25(String title, String content, PendingIntent intent) {
        return new NotificationCompat.Builder(getApplicationContext())
                .setContentTitle(title)
                .setContentText(content)
                .setContentIntent(intent)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setSmallIcon(android.R.drawable.stat_notify_more)
                .setAutoCancel(true);
    }

    /**
     *
     * @param id        id 固定每次收到只显示更新一条，不固定就显示多条
     * @param title     标题 一般填写app名称
     * @param content   显示内容
     * @param intent    点击跳转意图
     */
    public void sendNotification(int id, String title, String content, PendingIntent intent) {
        if (Build.VERSION.SDK_INT >= 26) {
            createNotificationChannel();
            Notification notification = getChannelNotification(title, content, intent).build();
            getManager().notify(id, notification);
        } else {
            Notification notification = getNotification_25(title, content, intent).build();
            getManager().notify(id, notification);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createNotificationChannel() {
        //创建 NotificationChannel 可配置 铃声震动 闪光灯等
        NotificationChannel channel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_HIGH);
        getManager().createNotificationChannel(channel);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Notification.Builder getChannelNotification(String title, String content, PendingIntent intent) {
        return new Notification.Builder(getApplicationContext(), id)
                .setContentTitle(title)
                .setContentText(content)
                .setContentIntent(intent)
                .setSmallIcon(android.R.drawable.stat_notify_more)
                .setAutoCancel(true);
    }

    public void cancelNotification(int id){
        NotificationManager systemService = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        systemService.cancel(id);
    }

}