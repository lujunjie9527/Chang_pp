package com.changjiedata.chanpp.helper;

import com.changjiedata.chanpp.proto.MemberOuterClass;

public class UserBean {


    private String key;
    private String member_id;
    private String member_name;
    private String member_mobile;
    private String member_avatar;
    private String member_points;
    private String push_id;
    private String chat_id;
    private String chat_pwd;
    private String truename;
    private String idcard;
    private String step;

    public UserBean() { }

    public UserBean(MemberOuterClass.Member in) {
        setMember_id(in.getMemberId());
        setMember_name(in.getMemberId());
        setMember_mobile(in.getMemberMobile());
        setMember_avatar(in.getMemberAvatar());
        setMember_points(in.getMemberPoints());
        setPush_id(in.getPushId());
        setChat_id(in.getChatId());
        setChat_pwd(in.getChatPwd());
        setTruename(in.getTruename());
        setIdcard(in.getIdcard());
        setStep(in.getStep());
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getMember_name() {
        return member_name;
    }

    public void setMember_name(String member_name) {
        this.member_name = member_name;
    }

    public String getMember_mobile() {
        return member_mobile;
    }

    public void setMember_mobile(String member_mobile) {
        this.member_mobile = member_mobile;
    }

    public String getMember_avatar() {
        return member_avatar;
    }

    public void setMember_avatar(String member_avatar) {
        this.member_avatar = member_avatar;
    }

    public String getMember_points() {
        return member_points;
    }

    public void setMember_points(String member_points) {
        this.member_points = member_points;
    }

    public String getPush_id() {
        return push_id;
    }

    public void setPush_id(String push_id) {
        this.push_id = push_id;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getChat_pwd() {
        return chat_pwd;
    }

    public void setChat_pwd(String chat_pwd) {
        this.chat_pwd = chat_pwd;
    }

    public String getTruename() {
        return truename;
    }

    public void setTruename(String truename) {
        this.truename = truename;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }


}


