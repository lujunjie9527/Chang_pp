package com.changjiedata.chanpp.helper;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.changjiedata.chanpp.base.MainApplication;
import com.changjiedata.chanpp.module.main.MainActivity;
import com.example.havi.interfaces.HyphenateLibraryCallBack;
import com.hyphenate.chat.EMMessage;

/**
 * 文件名 : HyphenateLibraryCallBackHandle
 * 创建者 : MangoWang
 * 创建日期 : 2019/12/8 14:24
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */
public class HyphenateLibraryCallBackHandle implements HyphenateLibraryCallBack {


    @Override
    public void goUserHome(Context context, String userId) {

    }

    @Override
    public void onNewMsg(EMMessage message) {
        if (MainApplication.isAppBackground()) {  //是否是后台运行 显示提醒通知
            Intent intent = new Intent(MainApplication.getAppContext(), MainActivity.class);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(MainApplication.getAppContext(), 1, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationHelper.getInstance().sendNotification(NotificationConstants.HYPHENATE_NOTIFCATION_ID, "环信库", "您有新的聊天消息!", pendingIntent);
        }
    }
}
