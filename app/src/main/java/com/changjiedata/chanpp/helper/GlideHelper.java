package com.changjiedata.chanpp.helper;

import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.utils.Utils;


import java.io.File;


import me.jessyan.autosize.utils.AutoSizeUtils;

/**
 * Created by Frank on 2017/3/31.
 * Glide 4.0 正式版
 */

public enum GlideHelper {
    INSTANCE;


    //备用 后台头像地址不改变（只做本人头像显示）
    public void loadAvatar(ImageView imageView, String url) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.oval_default_image)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .circleCrop();   //4.0版本圆形
        Glide.with(Utils.getApp())
                .load(url)
                .apply(options)
                .into(imageView);
    }

    /*-------------------------- url地址图片 ---------------------------------*/
    /**
     * 加载url图片
     *
     * @param imageView
     * @param url
     */
    public void loadImage(ImageView imageView, String url) {
        Glide.with(Utils.getApp())
                .load(url)
                .apply(new RequestOptions().error(R.mipmap.mine_default_head))
                .transition(DrawableTransitionOptions.withCrossFade(1500)) // 4.0 淡入淡出效果
                .into(imageView);
    }


    public void loadFileImage(ImageView imageView, String url) {
        Glide.with(Utils.getApp())
                .load(Uri.fromFile(new File(url)))
                .apply(new RequestOptions().error(R.mipmap.mine_default_head))
                .transition(DrawableTransitionOptions.withCrossFade(1500)) // 4.0 淡入淡出效果
                .into(imageView);
    }


    /**
     * 加载url图片
     *
     * @param imageView
     * @param url
     */
    public void loadFitXYImage(ImageView imageView, String url) {
        Glide.with(Utils.getApp())
                .load(url)
                .apply(new RequestOptions().error(R.mipmap.mine_default_head).fitCenter())
                .transition(DrawableTransitionOptions.withCrossFade(1500)) // 4.0 淡入淡出效果
                .into(imageView);
    }


    /**
     * 根据设置尺寸加载url
     * @param imageView
     * @param url
     * @param width
     * @param height
     */
    public void loadImage(ImageView imageView, String url, int width, int height) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.rectangle_default_image)
                .centerCrop()
                .override(width,height);
        Glide.with(Utils.getApp())
                .load(url)
                .apply(options)
                .into(imageView);
    }

    /**
     * 带尺寸切角
     * @param imageView
     * @param url
     * @param width
     * @param height
     * @param dp
     */
    public void loadImage(ImageView imageView, String url, int width, int height,int dp) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.rectangle_default_image)
                .centerCrop()
                .transform(new RoundedCorners(AutoSizeUtils.dp2px(Utils.getApp(),dp)))
                .override(width,height);
        Glide.with(Utils.getApp())
                .load(url)
                .apply(options)
                .into(imageView);
    }

    /**
     * 不带尺寸切角
     * @param imageView
     * @param url
     * @param dp
     */
    public void loadImage(ImageView imageView, String url,int dp) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.rectangle_default_image)
                .transform(new MultiTransformation<>(new CenterCrop(),
                        new RoundedCorners(AutoSizeUtils.dp2px(Utils.getApp(),dp))));
        Glide.with(Utils.getApp())
                .load(url)
                .apply(options)
                .into(imageView);
    }

    /**
     * 加载圆形图片
     *
     * @param imageView
     * @param url
     */
    public void loadCircleImage(ImageView imageView, String url) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.oval_default_image)
                .centerCrop()
                .circleCrop();   //4.0版本圆形
        Glide.with(Utils.getApp())
                .load(url)
                .apply(options)
                .into(imageView);
    }
    /**
     * 带尺寸加载圆形图片
     *
     * @param imageView
     * @param url
     */
    public void loadCircleImage(ImageView imageView, String url,int width,int height) {
        RequestOptions options= new RequestOptions()
                .placeholder(R.mipmap.oval_default_image)
                .centerCrop()
                .circleCrop()
                .override(width,height);
        Glide.with(Utils.getApp())
                .load(url)
                .apply(options)
                .into(imageView);
    }


/*-------------------------- 资源图片 ---------------------------------*/
    /**
     * 加载url图片
     *
     * @param imageView
     * @param resIcon
     */
    public void loadImage(ImageView imageView, int resIcon) {
        Glide.with(Utils.getApp())
                .load(resIcon)
                .apply(new RequestOptions().centerCrop())
                .into(imageView);
    }

    /**
     * 根据设置尺寸加载url
     * @param imageView
     * @param resIcon
     * @param width
     * @param height
     */
    public void loadImage(ImageView imageView, int resIcon, int width, int height) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .override(width,height);
        Glide.with(Utils.getApp())
                .load(resIcon)
                .apply(options)
                .into(imageView);
    }

    /**
     * 带尺寸切角
     * @param imageView
     * @param resIcon
     * @param width
     * @param height
     * @param dp
     */
    public void loadImage(ImageView imageView, int resIcon, int width, int height,int dp) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .transform(new RoundedCorners(AutoSizeUtils.dp2px(Utils.getApp(),dp)))
                .override(width,height);
        Glide.with(Utils.getApp())
                .load(resIcon)
                .apply(options)
                .into(imageView);
    }

    /**
     * 不带尺寸切角
     * @param imageView
     * @param resIcon
     * @param dp
     */
    public void loadImage(ImageView imageView, int resIcon,int dp) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .transform(new RoundedCorners(AutoSizeUtils.dp2px(Utils.getApp(),dp)));
        Glide.with(Utils.getApp())
                .load(resIcon)
                .apply(options)
                .into(imageView);
    }

    /**
     * 加载圆形图片
     *
     * @param imageView
     * @param resIcon
     */
    public void loadCircleImage(ImageView imageView, int resIcon) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .circleCrop();   //4.0版本圆形
        Glide.with(Utils.getApp())
                .load(resIcon)
                .apply(options)
                .into(imageView);
    }
    /**
     * 带尺寸加载圆形图片
     *
     * @param imageView
     * @param resIcon
     */
    public void loadCircleImage(ImageView imageView, int resIcon,int width,int height) {
        RequestOptions options
                = new RequestOptions()
                .centerCrop()
                .circleCrop()
                .override(width,height);
        Glide.with(Utils.getApp())
                .load(resIcon)
                .apply(options)
                .into(imageView);
    }






    //验证图片地址
   /* public GlideUrl getGlideUrl(String url) {
        //Authorization 请求头信息
        LazyHeaders headers = new LazyHeaders.Builder()
                .addHeader("Authorization", "http://cdn.dotasell.com")
                .build();
        return new GlideUrl(url, headers);
    }*/


}
