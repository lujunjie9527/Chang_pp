package com.changjiedata.chanpp.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;


import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.utils.UIUtil;
import com.changjiedata.chanpp.utils.date.DateUtil;
import com.ruffian.library.widget.RTextView;
import com.ycuwq.datepicker.date.MonthPicker;
import com.ycuwq.datepicker.date.YearPicker;

import kale.ui.view.dialog.BaseCustomDialog;
import kale.ui.view.dialog.BaseEasyDialog;

public class SelectYearMonthDialog extends BaseCustomDialog {
    public OnDialogClickListener dialogListener;
    public MonthPicker monthPicker;
    public YearPicker yearPicker;
    private RTextView tvSure;
    private RTextView tvCancle;

    public static final String KEY_MONTH = "KEY_MONTH";
    public static final String KEY_YEAR = "KEY_YEAR";
    public static final String KEY_OVER = "KEY_OVER";  //设置是否可以选择大于现在的日期

    /**
     * 自定义builder来增加一些参数，记得要继承自BaseEasyDialog.Builder
     */
    public static class Builder extends BaseEasyDialog.Builder<Builder> {
        private Bundle bundle = new Bundle();
        private OnDialogClickListener listener;

        public Builder(@NonNull Context context) {
            super(context);
        }


        public Builder setOnDialogClickListener(OnDialogClickListener lis) {
            listener = lis;
            return this;
        }


        public Builder setYearAndMonth(int year, int month) {
            bundle.putInt(KEY_MONTH, month);
            bundle.putInt(KEY_YEAR, year);
            return this;
        }

        @NonNull
        @Override
        protected SelectYearMonthDialog createDialog() {
            SelectYearMonthDialog dialog = new SelectYearMonthDialog();
            dialog.setArguments(bundle);
            dialog.dialogListener = (SelectYearMonthDialog.OnDialogClickListener) listener;
            return dialog;
        }
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_select_month;
    }

    @Override
    protected void bindViews(View view) {
        tvSure = findView(R.id.tv_sure);
        tvCancle = findView(R.id.tv_cancel);
        monthPicker = findView(R.id.month_picker);
        yearPicker = findView(R.id.year_picker);
        setListener();
        setYearAndMonthData();

    }

    private void setYearAndMonthData() {
        yearPicker.setSelectedYear(getArguments().getInt(KEY_YEAR, DateUtil.getYear()));
        monthPicker.setSelectedMonth(getArguments().getInt(KEY_MONTH, DateUtil.getMonth()));
    }

    private void setListener() {
        tvSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int year = yearPicker.getSelectedYear();
                int month = monthPicker.getSelectedMonth();
                int todayYear = DateUtil.getYear();
                int todayMonth = DateUtil.getMonth();
                if (SelectYearMonthDialog.this.getArguments().getBoolean(KEY_OVER, true)) {
                    if (year > todayYear || (year == todayYear && month > todayMonth)) {
                        Toast.makeText(SelectYearMonthDialog.this.getContext(), "选择月份大于当前月份", Toast.LENGTH_SHORT).show();
                    } else {
                        dialogListener.onClick(view, (year) + "" + String.format("%02d", month));
                        SelectYearMonthDialog.this.dismiss();
                    }
                } else {
                    dialogListener.onClick(view, (year) + "" + String.format("%02d", month));
                    SelectYearMonthDialog.this.dismiss();
                }


            }
        });
        tvCancle.setOnClickListener(view -> dismiss());
    }

    @Override
    protected void setViews() {
        setBackground();
        setLayout();
    }

    /**
     * 强制取消背景，保持有透明
     */
    private void setBackground() {
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.dialog_shop_jion_bg); // 设置背景
    }


    /**
     * 也可通过setLayout来设置：
     * getDialog().getWindow().setLayout(dm.widthPixels, getDialog().getWindow().getAttributes().height);
     */
    private void setLayout() {
        Window window = getDialog().getWindow();
        final WindowManager.LayoutParams lp = window.getAttributes();

        // 强制宽高
        int padding = UIUtil.dp2px(getContext(),0);
        lp.width = getScreenWidth(getActivity()) - (padding * 2);
        lp.gravity = Gravity.BOTTOM; // 设置展示的位置
        window.setAttributes(lp);
    }

    public static int getScreenWidth(Activity activity) {
        final DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * 监听输入框内容
     */
    public interface OnDialogClickListener {
        /**
         * Called when menu_text_color view has been clicked.
         *
         * @param v The view that was clicked.
         */
        void onClick(View v, String month);
    }


}
