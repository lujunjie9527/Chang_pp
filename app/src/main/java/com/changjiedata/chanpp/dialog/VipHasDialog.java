package com.changjiedata.chanpp.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.adapter.VipCardDialogAdapter;
import com.changjiedata.chanpp.proto.Bank;
import com.changjiedata.chanpp.utils.GsonUtils;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import kale.ui.view.dialog.BaseCustomDialog;
import kale.ui.view.dialog.BaseEasyDialog;
import kale.ui.view.dialog.EasyDialog;

/**
 * Created by AndroidIntexh1 on 2018/9/25.
 */

public class VipHasDialog extends BaseCustomDialog {

    public static final String KEY_DATA = "KEY_DATA";
    private RecyclerView rvBank;
    private TextView tv_vip_bank;
    private Button btn_next;
    private VipCardDialogAdapter mAdapter;
    private List<Bank.bank> datas = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;

    public VipHasDialog.OnDialogClickListener dialogListenerOne;
    public VipHasDialog.OnDialogClickListener dialogListenerTwo;
    public VipHasDialog.OnDialogClickListener dialogListenerThree;
    private TextView tv_close;


    /**
     * 继承自{@link EasyDialog.Builder}以扩展builder
     */
    public static class Builder extends BaseEasyDialog.Builder<Builder> {

        private VipHasDialog.OnDialogClickListener listenerOne;
        private VipHasDialog.OnDialogClickListener listenerTwo;
        private VipHasDialog.OnDialogClickListener listenerThree;
        private Bundle bundle = new Bundle();

        public Builder(@NonNull Context context) {
            super(context);
        }




        public Builder setData(String data) {
            bundle.putString(KEY_DATA, data);
            return this;
        }

        public Builder setOnDialogClickListenerOne(VipHasDialog.OnDialogClickListener lis) {
            listenerOne = lis;
            return this;
        }

        public Builder setOnDialogClickListenerTow(VipHasDialog.OnDialogClickListener lis) {
            listenerTwo = lis;
            return this;
        }

        public Builder setOnDialogClickListenerThree(VipHasDialog.OnDialogClickListener lis) {
            listenerThree = lis;
            return this;
        }

        @NonNull
        @Override
        protected EasyDialog createDialog() {
            VipHasDialog dialog = new VipHasDialog();
            dialog.dialogListenerOne = listenerOne;
            dialog.dialogListenerTwo = listenerTwo;
            dialog.dialogListenerThree = listenerThree;
            dialog.setArguments(bundle); // 增加自己的bundle
            return dialog;
        }
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_recycle_vip;
    }

    @Override
    protected void bindViews(View view) {
        rvBank = findView(R.id.rv_bank);
        tv_vip_bank = findView(R.id.tv_vip_bank);
        btn_next = findView(R.id.btn_bind);
        tv_close = findView(R.id.tv_close);
        initRecyclerView();
    }

    @Override
    protected void setViews() {
        tv_close.setOnClickListener(view -> dismiss());
        tv_vip_bank.setOnClickListener(v -> dialogListenerOne.onClick(v));


        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            dialogListenerTwo.onClick(view);
            dismiss();
        });
        btn_next.setOnClickListener(v -> {
            dialogListenerThree.onClick(v);
            dismiss();
        });
        String string = getArguments().getString(KEY_DATA);
        List<Bank.bank> list = GsonUtils.jsonToBeanList(string, new TypeToken<List<Bank.bank>>() {
        }.getType());
        mAdapter.notifyDataSetChanged();//!
        mAdapter.addData(list);
        setBackground();
        setLayout();
    }

    /**
     * 强制取消背景，保持有透明
     */
    private void setBackground() {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable()); // 去除dialog的背景，即透明
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0xffffffff)); // 设置白色背景
//        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.dialog_shop_jion_bg); // 设置背景
    }


    /**
     * 也可通过setLayout来设置：
     * getDialog().getWindow().setLayout(dm.widthPixels, getDialog().getWindow().getAttributes().height);
     */
    private void setLayout() {
        Window window = getDialog().getWindow();
        final WindowManager.LayoutParams lp = window.getAttributes();

        // 强制宽高
//        int padding = getResources().getDimensionPixelOffset(R.dimen.dialog_padding);
        lp.width = getScreenWidth(getActivity());
//        lp.height = getResources().getDimensionPixelOffset(R.dimen.dialog_height);

        lp.gravity = Gravity.BOTTOM; // 设置展示的位置
        window.setAttributes(lp);
    }

    @Override
    protected void modifyAlertDialogBuilder(AlertDialog.Builder builder) {
        super.modifyAlertDialogBuilder(builder);
    }

    public static int getScreenWidth(Activity activity) {
        final DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }


    /**
     * 监听输入框内容
     */
    public interface OnDialogClickListener {
        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        void onClick(View v);
    }

    private void initRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(getContext());
        rvBank.setLayoutManager(linearLayoutManager);
        rvBank.setAdapter(mAdapter = new VipCardDialogAdapter(datas));
    }

}
