package com.changjiedata.chanpp.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.utils.UIUtil;
import com.changjiedata.chanpp.utils.date.DateUtil;
import com.ruffian.library.widget.RTextView;
import com.ycuwq.datepicker.date.DayPicker;

import kale.ui.view.dialog.BaseCustomDialog;
import kale.ui.view.dialog.BaseEasyDialog;

/**
 * 文件名 : SelectMonthDialog
 * 创建者 : MangoWang
 * 创建日期 : 2019/12/24 12:01
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */
public class SelectDayDialog extends BaseCustomDialog {
    public OnDialogClickListener dialogListener;
    public DayPicker dayEndPicker;
    public DayPicker dayFirstPicker;
    private RTextView tvSure;
    private RTextView tvCancle;

    public static final String KEY_DAY_END = "KEY_DAY_END";
    public static final String KEY_DAY_FIRST = "KEY_DAY_FIRST";
    public static final String KEY_MONTH = "KEY_MONTH";
    public static final String KEY_YEAR = "KEY_YEAR";

    /**
     * 自定义builder来增加一些参数，记得要继承自BaseEasyDialog.Builder
     */
    public static class Builder extends BaseEasyDialog.Builder<Builder> {
        private Bundle bundle = new Bundle();
        private OnDialogClickListener listener;

        public Builder(@NonNull Context context) {
            super(context);
        }


        public Builder setOnDialogClickListener(OnDialogClickListener lis) {
            listener = lis;
            return this;
        }


        public Builder setYearAndMonth(int year, int month) {
            bundle.putInt(KEY_MONTH, month);
            bundle.putInt(KEY_YEAR, year);
            return this;
        }

        public Builder setSelectDay(int dayFirst, int dayEnd) {
            bundle.putInt(KEY_DAY_FIRST, dayFirst);
            bundle.putInt(KEY_DAY_END, dayEnd);
            return this;
        }

        @NonNull
        @Override
        protected SelectDayDialog createDialog() {
            SelectDayDialog dialog = new SelectDayDialog();
            dialog.setArguments(bundle);
            dialog.dialogListener = listener;
            return dialog;
        }
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_select_day;
    }

    @Override
    protected void bindViews(View view) {
        tvSure = findView(R.id.tv_sure);
        tvCancle = findView(R.id.tv_cancel);
        dayEndPicker = findView(R.id.day_end_picker);
        dayFirstPicker = findView(R.id.day_first_picker);
        setListener();
        setYearAndMonthData();

    }

    private void setYearAndMonthData() {
        dayFirstPicker.setMonth(getArguments().getInt(KEY_YEAR, DateUtil.getYear()), getArguments().getInt(KEY_MONTH, DateUtil.getMonth()));
        dayEndPicker.setMonth(getArguments().getInt(KEY_YEAR, DateUtil.getYear()), getArguments().getInt(KEY_MONTH, DateUtil.getMonth()));
        dayFirstPicker.setSelectedDay(getArguments().getInt(KEY_DAY_FIRST, 1));
        dayEndPicker.setSelectedDay(getArguments().getInt(KEY_DAY_END, 28));
    }

    private void setListener() {
        tvSure.setOnClickListener(view -> {
            int fistDay = dayFirstPicker.getSelectedDay();
            int endDay = dayEndPicker.getSelectedDay();
            if (fistDay > endDay) {
                dialogListener.onClick(view, String.format("%02d", endDay),String.format("%02d",fistDay));
            } else {
                dialogListener.onClick(view,String.format("%02d",fistDay),String.format("%02d",endDay));
            }
            dismiss();


        });
        tvCancle.setOnClickListener(view -> dismiss());
    }

    @Override
    protected void setViews() {
        setBackground();
        setLayout();
    }

    /**
     * 强制取消背景，保持有透明
     */
    private void setBackground() {
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable()); // 去除dialog的背景，即透明
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0xffffffff)); // 设置白色背景
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.dialog_shop_jion_bg); // 设置背景
    }


    /**
     * 也可通过setLayout来设置：
     * getDialog().getWindow().setLayout(dm.widthPixels, getDialog().getWindow().getAttributes().height);
     */
    private void setLayout() {
        Window window = getDialog().getWindow();
        final WindowManager.LayoutParams lp = window.getAttributes();

        // 强制宽高
        int padding = UIUtil.dp2px(getContext(), 0);
        lp.width = getScreenWidth(getActivity()) - (padding * 2);
//        lp.height = getResources().getDimensionPixelOffset(R.dimen.dialog_height);

        lp.gravity = Gravity.BOTTOM; // 设置展示的位置
//        lp.y = getResources().getDimensionPixelOffset(R.dimen.join_shop_dialog_margin);
        window.setAttributes(lp);
    }

    public static int getScreenWidth(Activity activity) {
        final DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * 监听输入框内容
     */
    public interface OnDialogClickListener {
        /**
         * Called when menu_text_color view has been clicked.
         *
         * @param v The view that was clicked.
         */
        void onClick(View v, String first, String end);
    }


}
