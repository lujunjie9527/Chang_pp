package com.changjiedata.chanpp.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;


import com.changjiedata.chanpp.R;

import kale.ui.view.dialog.BaseCustomDialog;
import kale.ui.view.dialog.BaseEasyDialog;

/**
 * 文件名 : SelectMonthDialog
 * 创建者 : MangoWang
 * 创建日期 : 2019/12/24 12:01
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */
public class VipNotDialog extends BaseCustomDialog {
    public OnDialogClickListener dialogListenerOne;
    public OnDialogClickListener dialogListenerTwo;
    private TextView typeOne;
    private TextView typeTwo;
    private Button typeThree;

    /**
     * 自定义builder来增加一些参数，记得要继承自BaseEasyDialog.Builder
     */
    public static class Builder extends BaseEasyDialog.Builder<VipNotDialog.Builder> {
        private Bundle bundle = new Bundle();
        private VipNotDialog.OnDialogClickListener listenerOne;
        private VipNotDialog.OnDialogClickListener listenerTwo;

        public Builder(@NonNull Context context) {
            super(context);
        }


        public VipNotDialog.Builder setOnDialogClickListenerOne(VipNotDialog.OnDialogClickListener lis) {
            listenerOne = lis;
            return this;
        }

        public VipNotDialog.Builder setOnDialogClickListenerTow(VipNotDialog.OnDialogClickListener lis) {
            listenerTwo = lis;
            return this;
        }


        @NonNull
        @Override
        protected VipNotDialog createDialog() {
            VipNotDialog dialog = new VipNotDialog();
            dialog.setArguments(bundle);
            dialog.dialogListenerOne = listenerOne;
            dialog.dialogListenerTwo = listenerTwo;
            return dialog;
        }
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_vip_not;
    }

    @Override
    protected void bindViews(View view) {
        typeOne = findView(R.id.tv_close);
        typeTwo = findView(R.id.tv_vip_bank);
        typeThree = findView(R.id.btn_bind);
        setListener();

    }


    private void setListener() {
        typeOne.setOnClickListener(view -> {
            dismiss();
        });
        typeTwo.setOnClickListener(view -> {
            dialogListenerOne.onClick(view);

        });
        typeThree.setOnClickListener(view -> {
            dialogListenerTwo.onClick(view);
            dismiss();
        });

    }

    @Override
    protected void setViews() {
        setBackground();
        setLayout();
    }

    /**
     * 强制取消背景，保持有透明
     */
    private void setBackground() {
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable()); // 去除dialog的背景，即透明
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0xffffffff)); // 设置白色背景
//        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.white_bg); // 设置背景
    }


    /**
     * 也可通过setLayout来设置：
     * getDialog().getWindow().setLayout(dm.widthPixels, getDialog().getWindow().getAttributes().height);
     */
    private void setLayout() {
        Window window = getDialog().getWindow();
        final WindowManager.LayoutParams lp = window.getAttributes();

        // 强制宽高
//        int padding = DisplayUtils.dp2px(40);
        lp.width = getScreenWidth(getActivity());
//        lp.height = getResources().getDimensionPixelOffset(R.dimen.dialog_height);

        lp.gravity = Gravity.BOTTOM; // 设置展示的位置
//        lp.y = getResources().getDimensionPixelOffset(R.dimen.join_shop_dialog_margin);
        window.setAttributes(lp);
    }

    public static int getScreenWidth(Activity activity) {
        final DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * 监听输入框内容
     */
    public interface OnDialogClickListener {
        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        void onClick(View v);
    }


}
