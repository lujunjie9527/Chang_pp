package com.changjiedata.chanpp.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.changjiedata.chanpp.R;
import com.ruffian.library.widget.RTextView;
import com.ycuwq.datepicker.date.MonthPicker;
import com.ycuwq.datepicker.date.YearPicker;

import kale.ui.view.dialog.BaseCustomDialog;
import kale.ui.view.dialog.BaseEasyDialog;

/**
 * Created by AndroidIntexh1 on 2018/12/26.
 */

public class SelectTypeDialog extends BaseCustomDialog {

    public OnDialogClickListener dialogListener;
    public MonthPicker monthPicker;
    public YearPicker yearPicker;
    private RTextView tvShuaKa;
    private RTextView tvVip;
    private RTextView rtvAllType;

    /**
     * 自定义builder来增加一些参数，记得要继承自BaseEasyDialog.Builder
     */
    public static class Builder extends BaseEasyDialog.Builder<SelectTypeDialog.Builder> {
        private Bundle bundle = new Bundle();
        private SelectTypeDialog.OnDialogClickListener listener;

        public Builder(@NonNull Context context) {
            super(context);
        }


        public SelectTypeDialog.Builder setOnDialogClickListener(SelectTypeDialog.OnDialogClickListener lis) {
            listener = lis;
            return this;
        }

        @NonNull
        @Override
        protected SelectTypeDialog createDialog() {
            SelectTypeDialog dialog = new SelectTypeDialog();
            dialog.setArguments(bundle);
            dialog.dialogListener = listener;
            return dialog;
        }
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_select_type;
    }

    @Override
    protected void bindViews(View view) {
        tvVip = findView(R.id.tvVip);
        tvShuaKa = findView(R.id.tvShuaKa);
        rtvAllType = findView(R.id.rtvAllType);
        setListener();

    }

    private void setListener() {
        tvVip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogListener.onClick(view,"3");
                SelectTypeDialog.this.dismiss();
            }
        });
        tvShuaKa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogListener.onClick(view,"2");
                SelectTypeDialog.this.dismiss();
            }
        });
        rtvAllType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogListener.onClick(view,"1");
                SelectTypeDialog.this.dismiss();
            }
        });
    }

    @Override
    protected void setViews() {
        setBackground();
        setLayout();
    }

    /**
     * 强制取消背景，保持有透明
     */
    private void setBackground() {
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.dialog_shop_jion_bg); // 设置背景
    }


    /**
     * 也可通过setLayout来设置：
     * getDialog().getWindow().setLayout(dm.widthPixels, getDialog().getWindow().getAttributes().height);
     */
    private void setLayout() {
        Window window = getDialog().getWindow();
        final WindowManager.LayoutParams lp = window.getAttributes();

        // 强制宽高
        int padding = getResources().getDimensionPixelOffset(R.dimen.dialog_padding);
        lp.width = getScreenWidth(getActivity()) - (padding * 2);
        lp.gravity = Gravity.BOTTOM; // 设置展示的位置
        window.setAttributes(lp);
    }

    public static int getScreenWidth(Activity activity) {
        final DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * 监听输入框内容
     */
    public interface OnDialogClickListener {
        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        void onClick(View v, String type);
    }


}
