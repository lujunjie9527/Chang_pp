package com.changjiedata.chanpp.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.proto.Merchantservice;

import java.util.List;


public class SupportAreaAdapter extends BaseQuickAdapter<Merchantservice.basics, BaseViewHolder> {
    public SupportAreaAdapter(List<Merchantservice.basics> dataList) {
        super(R.layout.item_location,dataList);
    }

    @Override
    protected void convert(BaseViewHolder helper, Merchantservice.basics item) {
        helper.setText(R.id.tv_location,item.getName());
    }
}