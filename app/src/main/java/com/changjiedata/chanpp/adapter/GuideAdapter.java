package com.changjiedata.chanpp.adapter;

/**
 * 文件名 : GuideAdapter
 * 创建者 : MangoWang
 * 创建日期 : 2019/12/7 14:19
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;


import com.changjiedata.chanpp.R;

import java.util.List;

/**
 * 创建者     gao hua
 * 创建时间   6/20 0020 19:37
 * 描述	      ${TODO}
 * <p>
 * 更新者     $Author$
 * 更新时间   $Date$
 * 更新描述   ${TODO}
 */
public class GuideAdapter extends PagerAdapter {

    private Context context;
    private List<Integer> imageList;

    public GuideAdapter(Context context, List<Integer> imageList) {
        this.context = context;
        this.imageList = imageList;
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_guide, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.item_guide_img);
        imageView.setImageResource(imageList.get(position));
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
