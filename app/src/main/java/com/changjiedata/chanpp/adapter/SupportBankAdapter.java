package com.changjiedata.chanpp.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.proto.Merchantservice;

import java.util.List;



public class SupportBankAdapter extends BaseQuickAdapter<Merchantservice.basics, BaseViewHolder> {
    public SupportBankAdapter(List<Merchantservice.basics> dataList) {
        super(R.layout.item_bank_list,dataList);
    }

    @Override
    protected void convert(BaseViewHolder helper, Merchantservice.basics item) {
        helper.setText(R.id.tvBankName,item.getName());
        helper.setVisible(R.id.rivBankIc,true);
    }
}