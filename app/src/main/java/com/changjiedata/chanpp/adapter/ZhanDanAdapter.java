package com.changjiedata.chanpp.adapter;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.bean.ZhanDanBean;
import com.changjiedata.chanpp.helper.GlideHelper;
import com.changjiedata.chanpp.utils.date.DateUtil;


import java.util.List;

/**
 * 文件名 : ZhanDanAdapter
 * 创建者 : MangoWang
 * 创建日期 : 2019/12/26 15:44
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */
public class ZhanDanAdapter extends BaseMultiItemQuickAdapter<ZhanDanBean, BaseViewHolder> {

    public ZhanDanAdapter(List<ZhanDanBean> data) {
        super(data);
        addItemType(1, R.layout.item_zhan_dan_title);
        addItemType(2, R.layout.item_zhang_dan);
    }

    @Override
    protected void convert(BaseViewHolder helper, ZhanDanBean item) {
        switch (helper.getItemViewType()) {
            case 1:
                helper.setText(R.id.tv_zhang_dan_time, item.getTime());
                break;
            case 2:
                helper.setText(R.id.tran_money_tv, item.getAmount())
                        .setText(R.id.id_dao_zhang_tv, item.getStatus())
                        .setText(R.id.tran_time_tv, DateUtil.getStrTime(item.getTime()))
                        .setText(R.id.tran_type_tv, item.getType());
                GlideHelper.INSTANCE.loadImage(helper.getView(R.id.bank_ic_iv), item.getLogo());
                break;
        }
    }
}
