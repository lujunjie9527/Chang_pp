package com.changjiedata.chanpp.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.proto.MemberOuterClass;


import java.util.List;

/**
 * 文件名 : ZhanDanAdapter
 * 创建者 : MangoWang
 * 创建日期 : 2019/12/26 15:44
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */
public class MessageAdapter extends BaseQuickAdapter<MemberOuterClass.SysteLog.SystemLogList, BaseViewHolder> {
    public MessageAdapter(List<MemberOuterClass.SysteLog.SystemLogList> data) {
        super(R.layout.item_message, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MemberOuterClass.SysteLog.SystemLogList item) {
        helper.setText(R.id.tv_title, item.getPushTitle())
                .setText(R.id.tv_time, item.getPushTime())
                .setText(R.id.message_text_tv,  item.getPushMessage());
    }


}
