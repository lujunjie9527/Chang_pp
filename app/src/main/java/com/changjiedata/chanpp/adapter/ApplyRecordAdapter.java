package com.changjiedata.chanpp.adapter;

import android.graphics.Color;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.proto.Merchantservice;

import java.util.List;

/**
 * 文件名 : ProgressAdapter
 * 创建者 : MangoWang
 * 创建日期 : 2019/11/25 11:50
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */

public class ApplyRecordAdapter extends BaseQuickAdapter<Merchantservice.change_bank, BaseViewHolder> {
    public ApplyRecordAdapter(List<Merchantservice.change_bank> dataList) {
        super(R.layout.item_details, dataList);
    }

    @Override
    protected void convert(BaseViewHolder helper, Merchantservice.change_bank item) {
        helper.setText(R.id.tvBankName2, item.getBankName())
                .setText(R.id.tvBankNum2, "尾号" + item.getBankCard().substring(item.getBankCard().length() - 4))
                .setText(R.id.tvTime, item.getCreatedTime());
        if (item.getStatus().equals("0")) {  //待审核
            helper.setText(R.id.rtvStatus, "待审核");
            helper.setBackgroundColor(R.id.rtvStatus, Color.parseColor("#C99F7D"));
        } else if (item.getStatus().equals("1")) {
            helper.setText(R.id.rtvStatus, "审核通过")
                    .setBackgroundColor(R.id.rtvStatus, Color.parseColor("#E68352"));
        } else if (item.getStatus().equals("2")) {
            helper.setText(R.id.rtvStatus, "审核不通过")
                    .setBackgroundColor(R.id.rtvStatus, Color.parseColor("#BCACA4"));
        }
    }
}