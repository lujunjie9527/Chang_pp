package com.changjiedata.chanpp.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.proto.Merchantservice;

import java.util.List;

public class ShopListAdapter extends BaseQuickAdapter<Merchantservice.shop, BaseViewHolder> {
public ShopListAdapter(List<Merchantservice.shop> dataList){
    super(R.layout.item_my_shop,dataList);
}

protected void convert(BaseViewHolder helper,Merchantservice.shop item){
    helper.setText(R.id.tvShopName,item.getShopName());
    helper.setText(R.id.tvAddress,item.getProvinceName());  // 省份名称
    helper.setText(R.id.tvCity,item.getCityName());//市
    helper.setText(R.id.tvCounty,item.getCountyName()); // 县
    helper.setText(R.id.tvShopBigType,item.getMccName());    // 商户大类名称
    helper.setText(R.id.tvShopLittleType,item.getSubName()); // 商户小类名称
    if(item.getStatus().equals("0")){
        helper.setText(R.id.tvJiHuo, "尚未制卡"); // 待制卡
    }else if(item.getStatus().equals("1")){
        helper.setText(R.id.tvJiHuo, "尚未制卡"); //未激活
    }else if(item.getStatus().equals("2")){
        helper.setText(R.id.tvJiHuo,item.getSnCode() ); //"已完成"
    }
    }

}
