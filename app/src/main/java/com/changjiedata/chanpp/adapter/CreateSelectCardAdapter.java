package com.changjiedata.chanpp.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.proto.Merchantservice;

import java.util.List;

/**
 * 文件名 : ProgressAdapter
 * 创建者 : MangoWang
 * 创建日期 : 2019/11/25 11:50
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */

public class CreateSelectCardAdapter extends BaseQuickAdapter<Merchantservice.basics, BaseViewHolder> {
    public CreateSelectCardAdapter(Context context, List<Merchantservice.basics> dataList) {
        super(R.layout.item_select_card, dataList);
    }

    @Override
    protected void convert(BaseViewHolder helper, Merchantservice.basics item) {
        helper.setText(R.id.tv_sn_no,item.getName());
    }



}