package com.changjiedata.chanpp.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.proto.Merchantservice;

import java.util.List;

/**
 * 文件名 : ProgressAdapter
 * 创建者 : MangoWang
 * 创建日期 : 2019/11/25 11:50
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */

public class CreateShopDetailAdapter extends BaseQuickAdapter<Merchantservice.shop, BaseViewHolder> {

    public CreateShopDetailAdapter(Context context, List<Merchantservice.shop> dataList) {
        super(R.layout.item_create_shop_detail, dataList);
    }

    @Override
    protected void convert(BaseViewHolder helper, Merchantservice.shop item) {
        helper.setText(R.id.tv_shop_name, item.getShopName())
                .setText(R.id.tv_local, item.getProvinceName() + " " + item.getCityName() + " " + item.getCountyName())
                .setText(R.id.tv_type, item.getMccName() + " " + item.getSubName());
        switch (item.getStatus())
        {
            case "0":
                helper.setText(R.id.tv_shop_no,"待制卡");
                break;
            case "1":
                helper.setText(R.id.tv_shop_no,"已申请标签");
                break;
            case "2":
                helper.setText(R.id.tv_shop_no,item.getSnCode());
                break;
        }

    }
}