package com.changjiedata.chanpp.adapter;

import android.graphics.Color;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.bean.ProgressBean;
import com.changjiedata.chanpp.view.CustomCircleProgressBar;


import java.util.List;

/**
 * 文件名 : ZhanDanAdapter
 * 创建者 : MangoWang
 * 创建日期 : 2019/12/26 15:44
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */
public class CountAdapter extends BaseQuickAdapter<ProgressBean, BaseViewHolder> {
    public CountAdapter(List<ProgressBean> data) {
        super(R.layout.item_count, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ProgressBean item) {
        CustomCircleProgressBar amProgressbar = helper.getView(R.id.am_progressbar);
        int color = Color.parseColor("#1888ff");
        int position = helper.getLayoutPosition();
        switch( position % 6) {
            case 0:
                color = Color.parseColor("#1888ff");
                break;
            case 1:
                color = Color.parseColor("#ff904e");
                break;
            case 2:
                color = Color.parseColor("#79c263");
                break;
            case 3:
                color = Color.parseColor("#ff3426");
                break;
            case 4:
                color = Color.parseColor("#64c1b3");
                break;
            case 5:
                color = Color.parseColor("#896fd5");
                break;
        }
        amProgressbar.setOutsideColor(color);
        amProgressbar.setTypeText(item.getName());
        amProgressbar.setProgress(item.getProgess());
    }


}
