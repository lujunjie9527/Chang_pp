package com.changjiedata.chanpp.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.helper.GlideHelper;
import com.changjiedata.chanpp.proto.Bank;


import java.util.List;

/**
 * 文件名 : ProgressAdapter
 * 创建者 : MangoWang
 * 创建日期 : 2019/11/25 11:50
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */

public class VipCardDialogAdapter extends BaseQuickAdapter<Bank.bank, BaseViewHolder> {
    public VipCardDialogAdapter(List<Bank.bank> dataList) {
        super(R.layout.item_vip_card,dataList);
    }

    @Override
    protected void convert(BaseViewHolder helper, Bank.bank item) {
        helper.setText(R.id.tv_name,item.getBankName())
        .setText(R.id.tv_card,"*** *** *** *** "+item.getCard().substring(item.getCard().length()-4))
        .setText(R.id.tv_time,"有效期  " + item.getValidTime());
        GlideHelper.INSTANCE.loadImage(helper.getView(R.id.iv_logo),item.getLogo());
    }
}