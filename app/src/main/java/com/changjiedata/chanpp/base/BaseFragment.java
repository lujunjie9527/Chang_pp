package com.changjiedata.chanpp.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {
    protected View rootView;
    private String fragmentName;
    public Activity mContent;
    private BaseActivity mActivity;
    public BaseActivity base;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (rootView == null) {
            try {
                rootView = inflater.inflate(getLayoutId(), container, false);
                rootView.setClickable(true);
            } catch (OutOfMemoryError e) {
                System.gc();
                rootView = inflater.inflate(getLayoutId(), container, false);
            }
        }
        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null) {
            parent.removeView(rootView);
        }
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContent = getActivityContext();
        init();
        initView();
        fragmentName = this.getClass().toString();
        fragmentName = fragmentName.substring(fragmentName.lastIndexOf(".") + 1);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
        base = mActivity;
    }

    public View getRootView() {
        return rootView;
    }

    public void showToast(String message) {
        Toast.makeText(getActivityContext(), message, Toast.LENGTH_LONG).show();
    }

    protected Activity getActivityContext() {
        return mActivity;
    }

    protected abstract int getLayoutId();

    protected abstract void initView();

    protected abstract void init();

    /*------------------------页面跳转------------------------*/
    public void startActivity(Class<?> clz) {
        startActivity(new Intent(getActivityContext(), clz));
    }

    public void startActivityForResult(Class<?> clz, int requestCode) {
        startActivityForResult(new Intent(getActivityContext(), clz), requestCode);
    }

    public void startActivity(Class<?> clz, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(getActivityContext(), clz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    public void finishActivity() {
        if (getActivityContext() != null) {
            getActivityContext().finish();
        }
    }



}
