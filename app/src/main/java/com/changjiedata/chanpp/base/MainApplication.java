package com.changjiedata.chanpp.base;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.changjiedata.chanpp.BuildConfig;
import com.changjiedata.chanpp.helper.HyphenateLibraryCallBackHandle;
import com.changjiedata.chanpp.helper.MyActivityLifecycle;
import com.changjiedata.chanpp.helper.UserHelper;
import com.changjiedata.chanpp.push.JPushHelper;
import com.example.havi.helper.ChatHelper;

import java.util.ArrayList;

public class MainApplication extends Application {
    private static ChatHelper chatHelper;
    private static MainApplication instance;
    private final static ArrayList<BaseActivity> activitys = new ArrayList<>();
    private final static ArrayList<Activity> webActivityList = new ArrayList<>();
    private static MyActivityLifecycle myActivityLifecycle;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initJPush();
        initHyphenateIM();
    }

    //全局记录activity
    public static void addActivity(BaseActivity context) {
        activitys.add(context);
    }
    public static String getServiceHost() {
        return serviceHost;
    }

    public static void setServiceHost(String serviceHost) {
        MainApplication.serviceHost = serviceHost;
    }

    private static String serviceHost = BuildConfig.SERVERHEAD;



    public static void removeActivity(BaseActivity context) {
        activitys.remove(context);
    }
    public static MainApplication getInstance() {
        return instance;
    }

    public static Context getAppContext() {
        return instance.getApplicationContext();
    }

    public static void addTempActivity(Activity context) {
        tempActivitys.add(context);
    }

    //网页记录activity
    public static void addWebActivity(Activity context) { //记录打开的activity
        webActivityList.add(context);
    }
    public static void removeWebActivity(Activity key) {
        webActivityList.remove(key);
    }
    public static void finishAllActivity() {
        Activity activity;
        for (int i = activitys.size() - 1; i >= 0; i--) {
            activity = activitys.get(i);
            if (activity != null && !activity.isFinishing()) {
                activity.finish();
            }
        }
    }


    private final static ArrayList<Activity> tempActivitys = new ArrayList<>(); //临时记录activity界面

    public static void finishAllTempActivity() {
        Activity activity;
        for (int i = tempActivitys.size() - 1; i >= 0; i--) {
            activity = tempActivitys.get(i);
            if (activity != null && !activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    public static void finishTopActivity(int number) {
        Activity activity;
        for (int i = activitys.size() - 1; i >= activitys.size() - number; i--) {
            activity = activitys.get(i);
            if (activity != null && !activity.isFinishing()) {
                activity.finish();
            }
        }
    }


    private void initJPush() {
        JPushHelper.init(this);
        if(UserHelper.isLogin()){
            JPushHelper.setAlias(UserHelper.getUser().getPush_id());
        }
    }


    public static ChatHelper getChatHelper(){
        return chatHelper;
    }

    private void initHyphenateIM() {
        //初始化
        chatHelper = ChatHelper.getInstance();
        chatHelper.init(this,new HyphenateLibraryCallBackHandle());
        //同步用户账号与环信账号信息 项目使用中应该判断是否登录
    }

    /**
     * 判断前台后台运行
     * @return  true  处于后台   false  前台
     */
    public static boolean isAppBackground() {
        if (myActivityLifecycle.getStartCount() == 0) {
            return true;
        }
        return false;
    }
}
