package com.changjiedata.chanpp.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.changjiedata.chanpp.utils.LogUtil;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {
    protected Context mContext;
    protected AlertDialog hintDialog;
    protected ProgressDialog loadingDialog;
    public boolean isDestory;
    public  AlertDialog mTokenHintDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.addActivity(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);  //屏幕旋转
        mContext = this;
    }

    ///设置app字体大小不随系统改变。
    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config = new Configuration();
        config.setToDefaults();  //设置默认字体大小
        res.updateConfiguration(config, res.getDisplayMetrics());
        return res;
    }


    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        hideProgress();
        if(hintDialog!=null&&hintDialog.isShowing()){
            hintDialog.dismiss();
        }
        if(mTokenHintDialog!=null&&mTokenHintDialog.isShowing()){
            mTokenHintDialog.dismiss();
        }
        MainApplication.removeActivity(this);
        super.onDestroy();
        System.gc();
    }

    /*=========================================================================================*/

    public void startActivity(Class<?> clz) {
        startActivity(new Intent(getApplicationContext(), clz));
    }

    public void startForSoultActivity(Class<?> clz, int requestCode) {
        startActivityForResult(new Intent(getApplicationContext(), clz), requestCode);
    }

    public void startActivityForResult(Class<?> clz, int requestCode) {
        startActivityForResult(new Intent(getApplicationContext(), clz), requestCode);
    }

    public void startActivity(Class<?> clz, Bundle bundle) {   //带bundle
        Intent intent = new Intent();
        intent.setClass(mContext, clz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    /*=========================================================================================*/
    public void showHintDialog(String message) {  //提示弹窗
        if (isFinishing()) return;
        if (hintDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setMessage(message);
            builder.setPositiveButton("确定", null);
            hintDialog = builder.show();
        } else {
            hintDialog.setMessage(message);
            hintDialog.show();
        }
    }

    /*=========================================================================================*/
    public void showToast(String str) {
        Toast.makeText(mContext, str, Toast.LENGTH_LONG).show();
    }

    /*=========================================================================================*/

    public void showProgress() {
        showProgress("请稍后...", true);
    }

    public void showProgress(String message) {
        showProgress(message, true);
    }

    public void showProgress(Boolean isCancelable) {
        showProgress("请稍后...", isCancelable);
    }

    public void showProgress(String message, Boolean isCancelable) {
        if (isFinishing()) return;
        loadingDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);  //THEME_HOLO_DARK           THEME_DEVICE_DEFAULT_DARK
        loadingDialog.setMessage(message);
        loadingDialog.setCancelable(isCancelable);
        if (getBaseContext() != null && !loadingDialog.isShowing()) {
            loadingDialog.show();
            Point size = new Point();
            loadingDialog.getWindow().getWindowManager().getDefaultDisplay().getSize(size);
            WindowManager.LayoutParams params = loadingDialog.getWindow().getAttributes();
            params.dimAmount = 0f;
            loadingDialog.getWindow().setAttributes(params);
        }
    }

    public void hideProgress() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
        }
    }

    /*==================================验证码倒计时方法=======================================================*/
    public void countDownReSend(final TextView textView, long sec) {
        if (textView == null) return;
        showToast("验证码发送成功，请注意查收");
        textView.setTag(textView.getTextColors()); //得到系统颜色
        textView.setEnabled(false);
        new CountDownTimer(sec * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                if (isDestory) {
                    cancel();
                    return;
                }
                textView.setText((millisUntilFinished / 1000) + "秒倒计时");
            }

            public void onFinish() {
                if (isDestory) return;
                textView.setText("重新获取");
                textView.setEnabled(true);
                textView.setSelected(false);
            }
        }.start();

    }
    /*==================================弹出登录失效弹窗=======================================================*/
    /*------------------------日志方法------------------------*/
    protected void logE(String tag, String s) {
        LogUtil.e(tag, s);
    }

}
