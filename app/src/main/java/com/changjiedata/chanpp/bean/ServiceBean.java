package com.changjiedata.chanpp.bean;

/**
 * 文件名 : ServiceBean
 * 创建者 : MangoWang
 * 创建日期 : 2019/12/31 16:53
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */
public class ServiceBean {
    /**
     * member_id : 10000
     * member_name : 客服
     * member_avatar : https://zuanqian.oss-cn-beijing.aliyuncs.com/home/member/%E5%AE%A2%E6%9C%8D.jpg
     * chat_id : yyyj05224332241541429012
     * chat_pwd : 8f96fa8dde2c58c28b0b1537f3f9c298
     *  "member_mobile":"123154564564",
     */

    private String member_id;
    private String member_name;
    private String member_avatar;
    private String chat_id;
    private String chat_pwd;


    private String member_mobile;

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getMember_name() {
        return member_name;
    }

    public void setMember_name(String member_name) {
        this.member_name = member_name;
    }

    public String getMember_avatar() {
        return member_avatar;
    }

    public void setMember_avatar(String member_avatar) {
        this.member_avatar = member_avatar;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getChat_pwd() {
        return chat_pwd;
    }

    public void setChat_pwd(String chat_pwd) {
        this.chat_pwd = chat_pwd;
    }


    public String getMember_mobile() {
        return member_mobile;
    }

    public void setMember_mobile(String member_mobile) {
        this.member_mobile = member_mobile;
    }
}
