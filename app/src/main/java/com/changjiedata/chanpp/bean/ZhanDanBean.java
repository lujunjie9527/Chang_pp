package com.changjiedata.chanpp.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.changjiedata.chanpp.proto.Statistics;


/**
 * 文件名 : ZhanDanBean
 * 创建者 : MangoWang
 * 创建日期 : 2019/12/26 18:25
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */
public class ZhanDanBean implements MultiItemEntity {


    private String amount;    //金额
    private String type;    //类型
    private String status;    //状态
    private String time;    //时间
    private String logo;    //logo
    private String id;    //id
    private String trade_free;    //交易手续费
    private String settle_free;    //结算手续费
    private String order_id;    //订单号
    private String settle_amount;   //结算金额
    private String settle_time;   //到账时间
    private String ticket;   //小票
    private String timeLong;   // 时间戳


    private int mytype = 1; // 用于识别是标题 还是内容

    public String getTimeLong() {
        return timeLong;
    }

    public void setTimeLong(String timeLong) {
        this.timeLong = timeLong;
    }



    public ZhanDanBean(String time) {
        setMytype(1);
        String[] strings = time.split("-");
        setTime(strings[0] + "年" + strings[1] + "月" + strings[2] + "日");

    }

    public ZhanDanBean(Statistics.trading in) {
        setMytype(2);
        setAmount(in.getAmount());
        setType(in.getType());
        setStatus(in.getStatus());
        setTime(in.getTime());
        setLogo(in.getLogo());
        setId(in.getId());
        setTrade_free(in.getTradeFree());
        setSettle_free(in.getSettleFree());
        setOrder_id(in.getOrderId());
        setSettle_amount(in.getSettleAmount());
        setSettle_time(in.getSettleTime());
        setTicket(in.getTicket());
    }


    public int getMytype() {
        return mytype;
    }

    public void setMytype(int mytype) {
        this.mytype = mytype;
    }


    @Override
    public int getItemType() {
        return mytype;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTrade_free() {
        return trade_free;
    }

    public void setTrade_free(String trade_free) {
        this.trade_free = trade_free;
    }

    public String getSettle_free() {
        return settle_free;
    }

    public void setSettle_free(String settle_free) {
        this.settle_free = settle_free;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getSettle_amount() {
        return settle_amount;
    }

    public void setSettle_amount(String settle_amount) {
        this.settle_amount = settle_amount;
    }

    public String getSettle_time() {
        return settle_time;
    }

    public void setSettle_time(String settle_time) {
        this.settle_time = settle_time;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }


}

