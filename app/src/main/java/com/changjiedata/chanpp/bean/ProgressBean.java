package com.changjiedata.chanpp.bean;

/**
 * 文件名 : ProgressBean
 * 创建者 : MangoWang
 * 创建日期 : 2019/12/30 20:48
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */
public class ProgressBean {
    private String name;
    private String percent;
    private int progess;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public int getProgess() {
        return progess;
    }

    public void setProgess(int progess) {
        this.progess = progess;
    }


}
