package com.changjiedata.chanpp.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.adapter.SupportAreaAdapter;
import com.changjiedata.chanpp.net.Apis;
import com.changjiedata.chanpp.net.NetworkManager;
import com.changjiedata.chanpp.net.interfaces.OnRequestCallBack;
import com.changjiedata.chanpp.proto.Merchantservice;
import com.changjiedata.chanpp.utils.GsonUtils;
import com.google.gson.reflect.TypeToken;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;

import kale.ui.view.dialog.BaseCustomDialog;
import kale.ui.view.dialog.BaseEasyDialog;
import kale.ui.view.dialog.EasyDialog;

/**
 * Created by AndroidIntexh1 on 2018/9/25.
 */

public class RecycleViewThreeAreaDialog extends BaseCustomDialog {

    public static final String KEY_DATA = "KEY_DATA";
    public static final String KEY_TITLE = "KEY_TITLE";
    private MaxHeightRecyclerView recyclerView;
    private TextView tvTitle;
    private SupportAreaAdapter mAdapter;
    private List<Merchantservice.basics> datas = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private static onItemAreaClickListener areaListener;
    private String province = "";
    private String city = "";
    private String county = "";
    private String provinceCode;
    private String cityCode;
    private String countyCode;

    public RecycleViewThreeAreaDialog(final onItemAreaClickListener listener) {
        this.areaListener = listener;
    }

    /**
     * 继承自{@link EasyDialog.Builder}以扩展builder
     */
    public static class Builder extends BaseEasyDialog.Builder<Builder> {
        private RecycleViewThreeAreaDialog.OnDialogClickListener listener;
        private Bundle bundle = new Bundle();

        public Builder(@NonNull Context context) {
            super(context);
        }

        public Builder setData(String data) {
            bundle.putString(KEY_DATA, data);
            return this;
        }

        public Builder setTitle(String title) {
            bundle.putString(KEY_TITLE, title);
            return this;
        }

        @NonNull
        @Override
        protected EasyDialog createDialog() {
            RecycleViewThreeAreaDialog dialog = new RecycleViewThreeAreaDialog(areaListener);
            dialog.setArguments(bundle); // 增加自己的bundle
            return dialog;
        }
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_recycle_view;
    }

    @Override
    protected void bindViews(View view) {
        recyclerView = findView(R.id.max_recyclerView);
        tvTitle = findView(R.id.tv_title);
        TextView btn_cancle = findView(R.id.tv_cancel);
        TextView btn_cancle2 = findView(R.id.tv_cancel2);
        String string = getArguments().getString(KEY_DATA);
        String title = getArguments().getString(KEY_TITLE);
        tvTitle.setText(title);
        List<Merchantservice.basics> list = GsonUtils.jsonToBeanList(string, new TypeToken<List<Merchantservice.basics>>() {
        }.getType());
        initRecyclerView();
        mAdapter.addData(list);
        btn_cancle.setOnClickListener(view1 -> dismiss());
        btn_cancle2.setOnClickListener(view1 -> dismiss());
    }

    @Override
    protected void setViews() {
        setBackground();
        setLayout();
    }

    /**
     * 强制取消背景，保持有透明
     */
    private void setBackground() {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable()); // 去除dialog的背景，即透明
    }

    /**
     * 也可通过setLayout来设置：
     * getDialog().getWindow().setLayout(dm.widthPixels, getDialog().getWindow().getAttributes().height);
     */
    private void setLayout() {
        Window window = getDialog().getWindow();
        final WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = getScreenWidth(getActivity());
        lp.gravity = Gravity.BOTTOM; // 设置展示的位置
        window.setAttributes(lp);
    }

    @Override
    protected void modifyAlertDialogBuilder(AlertDialog.Builder builder) {
        super.modifyAlertDialogBuilder(builder);
    }

    public static int getScreenWidth(Activity activity) {
        final DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public void setOnClickListener(onItemAreaClickListener listener) {
        this.areaListener = listener;
    }

    /**
     * 监听输入框内容
     */
    public interface OnDialogClickListener {
        /**
         * Called when menu_text_color view has been clicked.
         *
         * @param v The view that was clicked.
         */
        void onClick(View v, String sn_code, String sn_count, String sn_up_code);
    }

    private void initRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mAdapter = new SupportAreaAdapter(datas));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));  //横线        //添加Android自带的分割线
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (province.isEmpty()) {
                    province = datas.get(position).getName();
                    provinceCode = datas.get(position).getCode();
                    area_list(datas.get(position).getCode());
                } else if (city.isEmpty()) {
                    city = datas.get(position).getName();
                    cityCode = datas.get(position).getCode();
                    area_list(datas.get(position).getCode());
                } else {
                    county = datas.get(position).getName();
                    countyCode = datas.get(position).getCode();
                    areaListener.setAreaText(provinceCode,cityCode,countyCode, province, city, county);
                    dismiss();
                }

            }
        });
    }

    // 获取地区列表
    public void area_list(String code) {
        Merchantservice.basics.Builder builder1 = Merchantservice.basics.newBuilder();
        builder1.setCode(code);
        Merchantservice.basics data = builder1.build();
        NetworkManager.INSTANCE.post(Apis.area_list, data.toByteArray(), new OnRequestCallBack() {
            @Override
            public void onOk(byte[] bytes) throws InvalidProtocolBufferException {
                Merchantservice.polling datas = Merchantservice.polling.parseFrom(bytes);
                Log.e("ljj获取地区列表", datas.toString());
                mAdapter.replaceData(datas.getListList());
            }

            @Override
            public void onError(int code, String errorMessage) {

            }
        });
    }

    public interface onItemAreaClickListener {
        void setAreaText(String provinceCode, String cityCode, String countyCode, String province, String city, String county);
    }


}
