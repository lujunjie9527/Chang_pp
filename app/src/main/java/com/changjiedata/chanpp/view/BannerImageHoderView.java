package com.changjiedata.chanpp.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.bigkoo.convenientbanner.holder.Holder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.changjiedata.chanpp.R;
import com.changjiedata.chanpp.base.MainApplication;
import com.changjiedata.chanpp.net.WebApis;
import com.changjiedata.chanpp.proto.Index;
import com.changjiedata.chanpp.web.WebViewActivity;

/**
 * Created by AndroidIntexh1 on 2018/9/11.
 */

public class BannerImageHoderView implements Holder<Index.get_show_list.List> {
    private ImageView imageView;

    @Override
    public View createView(Context context) {
        //你可以通过layout文件来创建，也可以像我一样用代码创建，不一定是Image，任何控件都可以进行翻页
        imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        return imageView;
    }

    @Override
    public void UpdateUI(Context context, int position, Index.get_show_list.List data) {
        imageView.setImageResource(R.mipmap.rectangle_default_image);
        Glide.with(MainApplication.getAppContext())
                .load(data.getAdvCode())
                .apply(new RequestOptions().placeholder(R.mipmap.rectangle_default_image))
                .into(imageView);
        imageView.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(data.getAdvCode())) {  //  关联类型：0无跳转，1链接跳转，2视频详情页跳转
                if (data.getType().equals("1")) {
                    WebViewActivity.startActivity(context, WebApis.news+data.getAdvId());  // 不带视频
                } else if (data.getType().equals("2")){
                    WebViewActivity.startActivity(context, WebApis.news+data.getAdvId()+"&type=1");  // 带视频跳转
                }
            }
        });
    }
}
