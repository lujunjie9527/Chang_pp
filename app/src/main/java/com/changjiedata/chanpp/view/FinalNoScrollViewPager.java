package com.changjiedata.chanpp.view;

/**
 * 文件名 : FinalNoScrollViewPager
 * 创建者 : MangoWang
 * 创建日期 : 2019/12/7 14:15
 * 微信 : MangoWaWang
 * 邮箱 : 763482205@qq.com
 * 描述 : TODO
 */

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

/**
 * 创建者     gao hua
 * 创建时间   11/24 0024 9:48
 * 描述	      ${TODO}
 * <p>
 * 更新者     $Author$
 * 更新时间   $Date$
 * 更新描述   ${TODO}
 */

public class FinalNoScrollViewPager extends ViewPager {
    private boolean isCanScroll ;

    public FinalNoScrollViewPager(Context context) {
        super(context);
    }

    public FinalNoScrollViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setCanScroll(boolean isCanScroll) {
        this.isCanScroll = isCanScroll ;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if(isCanScroll) {
            return super.onInterceptTouchEvent(ev);
        }else {
            return  false ;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if(isCanScroll) {
            return super.onTouchEvent(ev);
        }else {
            return  false ;
        }
    }
}
