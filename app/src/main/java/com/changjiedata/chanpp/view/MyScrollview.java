package com.changjiedata.chanpp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;
/**
 * 屏蔽横向与纵向滚动事件
 */
public class MyScrollview extends ScrollView {
    private OnScrollChanged mOnScrollChanged;

    public MyScrollview(Context context) {
        super(context);
    }

    public MyScrollview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyScrollview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mOnScrollChanged != null) {
            mOnScrollChanged.onScroll(l, t, oldl, oldt);
        }
    }

    public void setOnScrollChanged(OnScrollChanged onScrollChanged) {
        this.mOnScrollChanged = onScrollChanged;
    }

    public interface OnScrollChanged {
        void onScroll(int scrolX, int scrollY, int oldScrollX, int oldScrollY);
    }
}
